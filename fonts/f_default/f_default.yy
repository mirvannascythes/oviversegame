{
    "id": "55c5000f-227c-426a-b277-9b1c8b16f65b",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "f_default",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "8f459967-7f82-4a2d-91ba-4b57cba988ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ef6e4d3e-3643-47d0-adb4-2e7412adb189",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 77,
                "y": 122
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "2cf6e9d5-df7a-4fcf-9d77-133d600a1469",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 82,
                "y": 122
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "d1467962-00c8-4a7a-819f-4c6648a860f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 90,
                "y": 122
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "c8f92199-c7eb-45c7-a39a-ccef26c6af14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 103,
                "y": 122
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "fbdbe58c-80a2-480c-828e-205b072f0e55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 22,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 115,
                "y": 122
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "517f01e6-2018-41e0-a89a-c12d0d81a575",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 132,
                "y": 122
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "5e1bc7a9-a278-48c2-ae56-4b15070f7be6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 147,
                "y": 122
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "dc9f8dbb-b636-4e9a-a3f7-ecc74be65e8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 152,
                "y": 122
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f94baeca-22ce-469b-b6ed-6effc61b9042",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 159,
                "y": 122
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "8f996672-4136-4c10-be9e-adf7a6d85af3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 166,
                "y": 122
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "e449b878-6798-4a0d-8018-8de38a6c2de5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 175,
                "y": 122
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "04e13c0b-0f7f-4aef-a2b8-f8666b6179ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 187,
                "y": 122
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "b8ea61a9-851f-4a84-88dc-721f64499364",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 192,
                "y": 122
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "3c8451fd-6160-4f66-a298-f578744ba765",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 200,
                "y": 122
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "f953398e-6c48-4697-95e4-a376577e0988",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 205,
                "y": 122
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "04e82184-2852-4d5d-8f35-3b4b70913f48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 213,
                "y": 122
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "92e64464-a555-4ffe-a22f-a86f63c4c239",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 22,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 225,
                "y": 122
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "42209052-8708-45f3-928c-80d659ddfd29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 65,
                "y": 122
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a5ab5868-9d20-4c8a-a830-55c6cd01eed2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 53,
                "y": 122
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "f1e267e4-9df2-4dd2-83b7-335090aaa37e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 41,
                "y": 122
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "817235c3-d900-40c5-a296-89f247b65692",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 150,
                "y": 98
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "0063e472-5d20-4688-b713-bf7b1c64afa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 80,
                "y": 98
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "6931a79d-91ee-4bd3-87cf-6bf203a0c2c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 92,
                "y": 98
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "f75f7de8-9559-4bac-8762-a60e871b4b70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 104,
                "y": 98
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "7031b10d-8a8d-462e-ad2d-593114007e76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 116,
                "y": 98
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "162298cc-3b45-46f9-a7c4-17bf445f18f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 128,
                "y": 98
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "fa52b069-422b-4090-be09-190a5d366307",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 133,
                "y": 98
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "6d8773c0-122f-4eac-bc37-140eed8f2ce7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 138,
                "y": 98
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "1ffca176-20e9-4304-ae41-0d68495a02ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 162,
                "y": 98
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "2da3e831-c189-4bca-88f7-8d4e6e859952",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 29,
                "y": 122
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "44a21691-a0cb-44c0-925e-271a9fa836ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 174,
                "y": 98
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "b7b1bd03-53fc-4a01-a703-0b4e8d5d5721",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 22,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 186,
                "y": 98
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "2de95d44-37c8-47a5-be58-4e588847049d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 22,
                "offset": -1,
                "shift": 13,
                "w": 14,
                "x": 206,
                "y": 98
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "75d5c96d-3745-4b66-ba34-ec20ac401aa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 222,
                "y": 98
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "01551089-fb0a-4607-95b0-654f5376254e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 235,
                "y": 98
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "19390bcb-1b15-4692-bb3c-1a61a7e6a60d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "9009233f-bfc0-4fe0-9f01-0a303cb26a3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 16,
                "y": 122
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "f1fd78f0-69e8-48ef-a960-883a622b0936",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 233,
                "y": 122
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "10bc6653-faf7-4b60-9784-dcfe5fbcf624",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 14,
                "y": 146
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "1e3c7b30-8436-4f6b-b05b-0aec4696c6a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 225,
                "y": 170
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "24d82784-ed1c-4579-a634-38f8cd89a828",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 29,
                "y": 146
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "7857e7b4-d382-4b09-9179-cde3178044c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 229,
                "y": 146
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "905072b1-e6bf-48c9-b56c-72275b62c7eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 240,
                "y": 146
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "ff6a706e-d197-4ca2-895f-9561927ceb09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 170
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "807db044-34b3-41d9-83a7-7eed703eda3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 13,
                "y": 170
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "87ac6c96-a6fc-49bb-9b09-cae10f651290",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 29,
                "y": 170
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "0d92644a-e234-4d9a-9f98-7ab119af67a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 22,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 43,
                "y": 170
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "33e299b3-6744-47ca-a78c-acae48860143",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 59,
                "y": 170
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d1190851-6af3-465b-b548-20c6c472cc06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 72,
                "y": 170
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "2da14aef-5b8b-4d2c-b16d-5a999e215597",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 89,
                "y": 170
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "f37ba27f-8bef-43ba-8b28-f92623b17b71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 104,
                "y": 170
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "cc08f1d9-1d3a-40b7-948d-c0b539189f53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 118,
                "y": 170
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "f58be5a3-e4fc-497c-81c4-68786b6edc21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 132,
                "y": 170
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "eb87ad1c-5c72-474c-b981-4e46f470d57b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 146,
                "y": 170
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "4a5a844f-5ac9-4d90-9218-df54b2332ae8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 22,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 161,
                "y": 170
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "f83d48c2-05f3-435b-9827-42a3439dda44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 181,
                "y": 170
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "f1dc9ef9-cfcd-442d-907c-29e64edda1c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 196,
                "y": 170
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "6d2264fc-de6e-4129-b9bf-3c9dce105f68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 211,
                "y": 170
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "68808fc9-1059-4031-9baf-571fe5654082",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 223,
                "y": 146
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "20b82724-5be4-4013-8021-67501602aec7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 215,
                "y": 146
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ec20deb3-0c6f-4bbe-9051-95d772464813",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 208,
                "y": 146
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "6d1efc39-a7f8-4618-bd7e-95b589f232e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 114,
                "y": 146
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "9a0da8fe-e7e8-4b34-8e95-b1a2231d9c51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 34,
                "y": 146
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "60fb0c3f-6fb8-4024-b449-927e4b3b3cd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 48,
                "y": 146
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "c1a3fa5d-a2c2-4de7-b145-5c350c509ab5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 55,
                "y": 146
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "ce882471-39f7-42b4-a905-b15506cbf856",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 67,
                "y": 146
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "3aca2189-d37c-4821-8cd0-8a3028bd3efb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 78,
                "y": 146
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "08abfed0-2cc5-409d-97bc-170a2047353c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 90,
                "y": 146
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "5fc671ff-0c50-4636-9bad-98fb53c5abe2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 102,
                "y": 146
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "b3ae5041-b658-4f0c-9019-333f9f0043b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 125,
                "y": 146
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "efe6e8eb-0a95-4f22-88e4-9c363dae7891",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 196,
                "y": 146
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "6918bc89-6428-4c72-ad16-46beb266c4fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 133,
                "y": 146
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "5b9fb5a5-0b83-4611-872b-a556b9f836bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 144,
                "y": 146
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "57953434-0c9d-4b4b-a193-35c4a2a40816",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 148,
                "y": 146
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "03c168f0-b142-4712-a3ee-21b0bc427d87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 154,
                "y": 146
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "9f93b0b7-a2c1-46f1-a243-32e58adeb823",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 165,
                "y": 146
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d67d8309-284f-4b50-b611-abfa85ccddc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 169,
                "y": 146
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "c02d7c4b-c031-43a4-a0cc-9d3b98201071",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 185,
                "y": 146
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "084ed900-7641-40c4-93e8-7306ce62f9d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 68,
                "y": 98
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "a6b0e76c-f2c5-4524-ae19-0fe27a169d4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 57,
                "y": 98
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "4010c7cf-953c-4594-9ff7-6be764376996",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 45,
                "y": 98
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "bc15eb68-7b5b-45bb-a94c-d15d8f0406f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 37,
                "y": 98
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d76f338b-64c7-4227-b235-948323600fa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 54,
                "y": 26
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "3b55c585-b11f-4600-9cf8-7d95ea87facc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 65,
                "y": 26
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "356fdcfc-44de-41f5-b0e0-5bafcec5cf23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 73,
                "y": 26
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "7f3f4e9a-aeb6-454a-bc3f-2a632bd4bccb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 84,
                "y": 26
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "3577d54a-1445-4ded-912a-7210bc24136a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 96,
                "y": 26
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "df105f2b-6d11-4760-b36e-4a4f31065772",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 112,
                "y": 26
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "b3c81c4b-b8eb-4054-81f2-035b6c434d97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 124,
                "y": 26
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "c165f555-966c-4e23-a1fe-55ad54588207",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 149,
                "y": 26
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "c0eb9f1f-dec4-457a-9b8d-21b19af83cc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 241,
                "y": 26
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "0b8cd078-acaa-48f5-a678-07f28a4c0927",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 161,
                "y": 26
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "ae45637b-563d-4b1c-abb2-59cabe3428b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 166,
                "y": 26
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "45d1d5d2-3457-46b3-a4fd-6bf2037ba49d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 174,
                "y": 26
            }
        },
        {
            "Key": 1040,
            "Value": {
                "id": "f046c16c-a609-4126-bccd-6365fe58e627",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1040,
                "h": 22,
                "offset": -1,
                "shift": 13,
                "w": 14,
                "x": 187,
                "y": 26
            }
        },
        {
            "Key": 1041,
            "Value": {
                "id": "b9b3d81a-ab82-48a7-b9a0-52e7d4f59659",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1041,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 203,
                "y": 26
            }
        },
        {
            "Key": 1042,
            "Value": {
                "id": "9f5c1ae3-0b48-4adb-8e6a-520c31bd0bc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1042,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 216,
                "y": 26
            }
        },
        {
            "Key": 1043,
            "Value": {
                "id": "98c7d634-7d99-4095-9254-bac94a79a7fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1043,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 229,
                "y": 26
            }
        },
        {
            "Key": 1044,
            "Value": {
                "id": "aaa3242e-5d25-4d2b-a524-64ec773559a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1044,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 39,
                "y": 26
            }
        },
        {
            "Key": 1045,
            "Value": {
                "id": "28d5b865-ca73-46ef-aed7-ad0ab81f5c9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1045,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 136,
                "y": 26
            }
        },
        {
            "Key": 1046,
            "Value": {
                "id": "6882d832-c95d-4af5-8590-523ee9f333e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1046,
                "h": 22,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 19,
                "y": 26
            }
        },
        {
            "Key": 1047,
            "Value": {
                "id": "4024b17a-7eee-448f-8d54-b9bb7bc089f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1047,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 1048,
            "Value": {
                "id": "1ece2413-42f2-4808-8f0b-164ca1a0001c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1048,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 1049,
            "Value": {
                "id": "2a4ffa03-25cb-4a4d-a247-91898912fab9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1049,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 1050,
            "Value": {
                "id": "a6f76b15-cf6e-4df5-a17e-d81ffa75a968",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1050,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 1051,
            "Value": {
                "id": "fe878c44-4526-4eb1-8cec-bf92cab1fbfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1051,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 1052,
            "Value": {
                "id": "9822f19d-169f-49b1-872d-adaae767be15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1052,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 1053,
            "Value": {
                "id": "77fdea8c-44b0-4e3b-859e-b911256fa2bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1053,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 1054,
            "Value": {
                "id": "55522b42-a4c4-4628-9077-81ff06cf3d89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1054,
                "h": 22,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 1055,
            "Value": {
                "id": "b0e0c11a-9a32-4ed9-924d-477dd4019e55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1055,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 1056,
            "Value": {
                "id": "cf58223a-7bc3-429a-8077-23eca96d2fb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1056,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 1057,
            "Value": {
                "id": "befd0d23-05db-46a4-b075-64cc12c9c63b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1057,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 1058,
            "Value": {
                "id": "22b8202e-7142-4670-b388-d234bfa74807",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1058,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 1059,
            "Value": {
                "id": "5685344d-4960-45de-93ed-5c79b6818100",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1059,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 1060,
            "Value": {
                "id": "e4c12e46-8f96-42c8-b2d9-d1a291cb6ad8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1060,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 179,
                "y": 2
            }
        },
        {
            "Key": 1061,
            "Value": {
                "id": "d7df0747-4a08-4ec6-af0e-a69e09c5c43e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1061,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 195,
                "y": 2
            }
        },
        {
            "Key": 1062,
            "Value": {
                "id": "4aef50df-597d-4800-8b03-f2929542615c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1062,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 1063,
            "Value": {
                "id": "ec5cdfb9-1842-4bdf-81ac-5ba94d90de88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1063,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 225,
                "y": 2
            }
        },
        {
            "Key": 1064,
            "Value": {
                "id": "5dedffd2-63af-44d1-8a02-2234bdd548b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1064,
                "h": 22,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 1065,
            "Value": {
                "id": "41113780-bb25-45a1-9d0a-86a2d9e4a44d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1065,
                "h": 22,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 1066,
            "Value": {
                "id": "b9130a4b-93e8-4378-8e7c-cea97534e2fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1066,
                "h": 22,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 21,
                "y": 50
            }
        },
        {
            "Key": 1067,
            "Value": {
                "id": "83dd0264-c996-4938-83c6-2443bcb5cf29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1067,
                "h": 22,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 38,
                "y": 50
            }
        },
        {
            "Key": 1068,
            "Value": {
                "id": "3463f34a-62dd-4bf5-88ba-0cdd8808b4a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1068,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 65,
                "y": 74
            }
        },
        {
            "Key": 1069,
            "Value": {
                "id": "7cd82882-a78b-4209-b2b3-ac8ab0c1ab85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1069,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 78,
                "y": 74
            }
        },
        {
            "Key": 1070,
            "Value": {
                "id": "1ce47171-2d9c-4e97-a1c5-56b943d8863a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1070,
                "h": 22,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 93,
                "y": 74
            }
        },
        {
            "Key": 1071,
            "Value": {
                "id": "6dddfe98-20c6-4a7b-8799-f027f2e0a75c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1071,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 113,
                "y": 74
            }
        },
        {
            "Key": 1072,
            "Value": {
                "id": "2c2bcda9-33fc-4839-a8b4-41a98923511a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1072,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 128,
                "y": 74
            }
        },
        {
            "Key": 1073,
            "Value": {
                "id": "12fb0528-2184-4957-aa81-03327e392cb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1073,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 140,
                "y": 74
            }
        },
        {
            "Key": 1074,
            "Value": {
                "id": "c3fe0942-13b0-4206-9bf8-6afc4e5680ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1074,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 153,
                "y": 74
            }
        },
        {
            "Key": 1075,
            "Value": {
                "id": "dd0f6cd4-acd7-4cdb-9fb9-7fb2867de66c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1075,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 164,
                "y": 74
            }
        },
        {
            "Key": 1076,
            "Value": {
                "id": "cb0a0fc8-0496-4035-994d-e7a75b41a7fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1076,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 172,
                "y": 74
            }
        },
        {
            "Key": 1077,
            "Value": {
                "id": "f731416e-894e-4b57-ae69-be33236fe701",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1077,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 185,
                "y": 74
            }
        },
        {
            "Key": 1078,
            "Value": {
                "id": "ba6d32c9-cb52-47a5-aa32-c7fabd6904d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1078,
                "h": 22,
                "offset": -1,
                "shift": 13,
                "w": 14,
                "x": 197,
                "y": 74
            }
        },
        {
            "Key": 1079,
            "Value": {
                "id": "d2b4e202-c3bd-4eb4-9046-927b33389da2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1079,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 213,
                "y": 74
            }
        },
        {
            "Key": 1080,
            "Value": {
                "id": "3f0f4212-a26c-4046-9dc2-e0b1303dd556",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1080,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 224,
                "y": 74
            }
        },
        {
            "Key": 1081,
            "Value": {
                "id": "d73f1dc6-1b29-4c3c-af05-a10b482f879b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1081,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 235,
                "y": 74
            }
        },
        {
            "Key": 1082,
            "Value": {
                "id": "77c4427b-bc13-44b3-94f4-3f4bb056a522",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1082,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 1083,
            "Value": {
                "id": "92898e0f-e2fd-4418-8ae8-7b4912d846cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1083,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 12,
                "y": 98
            }
        },
        {
            "Key": 1084,
            "Value": {
                "id": "c4aec3d0-f24e-49dc-ae4f-8159f1e428b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1084,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 24,
                "y": 98
            }
        },
        {
            "Key": 1085,
            "Value": {
                "id": "5a248c57-23da-4eb5-855a-de5f96f1aa28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1085,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 54,
                "y": 74
            }
        },
        {
            "Key": 1086,
            "Value": {
                "id": "65e17c94-aed4-4571-8714-c1b6a159be13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1086,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 42,
                "y": 74
            }
        },
        {
            "Key": 1087,
            "Value": {
                "id": "3aa1d724-ec87-4495-9211-4a239d1ff84b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1087,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 31,
                "y": 74
            }
        },
        {
            "Key": 1088,
            "Value": {
                "id": "6fc27bed-2505-471d-9cd4-13f63f488775",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1088,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 142,
                "y": 50
            }
        },
        {
            "Key": 1089,
            "Value": {
                "id": "a76ab117-1cb8-40ef-93f3-03ebd62e0bce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1089,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 55,
                "y": 50
            }
        },
        {
            "Key": 1090,
            "Value": {
                "id": "badfb37b-644e-4787-9b42-2cadaa6c30f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1090,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 67,
                "y": 50
            }
        },
        {
            "Key": 1091,
            "Value": {
                "id": "e9c1b14d-e10d-4e61-9bf7-862f2ec9b5a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1091,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 78,
                "y": 50
            }
        },
        {
            "Key": 1092,
            "Value": {
                "id": "311426d7-3765-4154-9d53-c5d5433a0c24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1092,
                "h": 22,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 90,
                "y": 50
            }
        },
        {
            "Key": 1093,
            "Value": {
                "id": "e9550567-7711-41cf-9718-ed1ad888b6c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1093,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 107,
                "y": 50
            }
        },
        {
            "Key": 1094,
            "Value": {
                "id": "483084c2-7425-497e-82d8-e6dbfaac1050",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1094,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 119,
                "y": 50
            }
        },
        {
            "Key": 1095,
            "Value": {
                "id": "f31612eb-7443-4bec-8674-d8fae7a5d379",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1095,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 131,
                "y": 50
            }
        },
        {
            "Key": 1096,
            "Value": {
                "id": "20965575-ab0c-4120-b862-7a2d1d1701d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1096,
                "h": 22,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 153,
                "y": 50
            }
        },
        {
            "Key": 1097,
            "Value": {
                "id": "2c007139-7f18-4ddb-acae-78ccc34f1e71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1097,
                "h": 22,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 14,
                "y": 74
            }
        },
        {
            "Key": 1098,
            "Value": {
                "id": "4948f214-ead2-4513-86e5-616b49bc7b83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1098,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 168,
                "y": 50
            }
        },
        {
            "Key": 1099,
            "Value": {
                "id": "e231be40-1ce0-45c3-9ac3-77daa621eb01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1099,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 182,
                "y": 50
            }
        },
        {
            "Key": 1100,
            "Value": {
                "id": "e7043181-6b73-4d2a-b2bb-32379ca2858f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1100,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 196,
                "y": 50
            }
        },
        {
            "Key": 1101,
            "Value": {
                "id": "88dc1f9d-eb7b-4e46-8cab-ab7da319e305",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1101,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 207,
                "y": 50
            }
        },
        {
            "Key": 1102,
            "Value": {
                "id": "658835c5-3ff2-41db-8631-af63d939be77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1102,
                "h": 22,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 218,
                "y": 50
            }
        },
        {
            "Key": 1103,
            "Value": {
                "id": "67321b75-75dd-4f9b-b6d0-efe537e1a2d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1103,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 233,
                "y": 50
            }
        },
        {
            "Key": 1104,
            "Value": {
                "id": "13a0c8ae-9bdd-40c4-8fe4-b4e9a3d6c43c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1104,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 1105,
            "Value": {
                "id": "6a45ff2e-356a-4c6d-915d-b6ab0a0f0a0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1105,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 146
            }
        },
        {
            "Key": 1106,
            "Value": {
                "id": "33dc39e3-39b7-4315-ae1a-a60e644d03fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1106,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 239,
                "y": 170
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "53725ee6-89f2-4312-b673-78b98cf9f03c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "785570da-5b08-48b1-8dc9-c5b875e2780b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "674744be-abd8-41c8-9c89-9883861589d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "9477495e-7c2e-4ed5-8fee-80ae80f45c1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "90067d5a-4d5b-4448-9f3e-56c9ae74c1c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "052e1f47-fd99-405f-ac73-bb20b4319a82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "64bd6c5f-9de0-4204-9422-06dcc4001cd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "8480bfc2-bee5-4420-9f27-bea86d3c6808",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "e1d9e9af-6aaa-4a1e-a3a1-b4515fdc847d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "b0c97f47-f400-4e86-8ccf-77cb64a06f3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "2d9a2172-e4ea-477b-ba67-9f95f997b169",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "0ae4c154-2fb7-4ff2-8bac-8df23ddaa5aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "b019b207-6887-4674-989d-c8bd6e24eb6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "ebbdc871-3d24-4288-a682-0eb977e163e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "beb6c43b-0386-47ee-b6a2-279713d97ce0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "d2ae0f75-7c5e-495e-8458-3de8e8d85697",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "0337b7cf-5045-4975-84a9-ed18a4572134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "86f928a2-906a-4814-8821-c28e96184ffd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "904ecf72-67c7-448d-bbbd-03fb0364295a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "b26156b6-6a1e-44da-8936-2a3b8db021c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "39bfebfb-9d16-42d5-ab08-360c2c71ba1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "3bc0f9bb-2141-4b4f-9b1c-4f1b4bdf2f31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "67255331-c3bf-4c5e-8a2f-1c61e1061153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "d934eb9d-4aee-42b6-a54a-f009ba6675fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "94d036d7-3794-44d1-8833-ef5d7a6ab566",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "4c3f6a53-9285-4968-81fc-f99f0a4eb0fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "5fbcdf0d-eb6a-4266-a59d-608be4a6b001",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "01eb77f6-e35a-4205-ab25-08dd2daef0ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "11876348-9838-4dfc-a28e-c15d74b50177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "839915cb-781a-4a06-a237-6f84f06ecc4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "d021d9da-a1d8-4188-990b-b255f8d5b6d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 58
        },
        {
            "id": "6ed94327-eec3-441f-a75a-1018f0fe4b9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 59
        },
        {
            "id": "376cf2fa-59ff-45b0-b49b-7902547028a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "13099f1a-0cd3-4ea4-a979-558f2ed580ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "b4ec466c-ab5c-41f3-be03-d21aef316ac3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "3ea8f00f-939b-40ee-a1d9-0f574c8fd1a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "a54a9d11-3094-400d-9cb7-87d87b8ce776",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "a4f47039-563e-4133-a027-e4e6db899c24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "ec4d22fd-74d6-40cc-ac0c-3a9b74754d98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "a92a3cfe-3a0e-49f8-93d6-672c225081a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "48f928ea-418c-456b-af17-38ab47a6a331",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "7258be7d-4b6f-4cf9-a500-4cceb6d9bcef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "c94007ab-885c-46bc-81ef-e74edb4bfca9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "8ec0212b-553e-475b-bfea-a7d7b52dc32b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "f0c59543-b57b-4a9e-a630-f654e328e486",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 894
        },
        {
            "id": "08ae632d-5317-4542-a0ca-3908fac0badc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "ed2261dd-9990-4236-87a2-0edc370067e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "2b393b13-48d2-4831-9181-08675d83c580",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "f7f2c18c-1e75-4809-98b6-7ff1395a6e9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "e296beae-d2f4-4399-a0a9-38ebaa37387f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "bbd12374-82fd-48b0-8b84-3a2481ba900e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "95dfab09-a27e-40f4-86b1-9b0c9163d230",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "64009ed7-538e-49d3-9cd6-7ddf3db0c8bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "0dbcde6e-a44d-449f-afdf-87e1f047464d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "d0e991dd-4db4-4189-9304-c66ad7fd9e4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "f18ca28d-fa0e-4797-9756-de673e6be39b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "9cfae7e7-998d-4722-b82d-5d0c40b6d42c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "651ef9dd-f3cc-4448-ba74-f645d250e426",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "2e24acb6-a792-4b14-8e10-012fbb8a12db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "728cb51f-5fb0-4e2b-a939-d43b2daa93d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "9f176c1b-9b00-4e65-a747-d27f4fc8efc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "75e11a42-7f47-4c84-8167-df4c67664777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "4c7e4eac-2b0b-40be-97e4-f8362aa53f28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "520ded6a-f963-413a-83b8-bd1aba867c6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "9a30d8b1-be8c-4b25-b9aa-411904d54a40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "b13fd424-8c08-48bf-8fc8-f1fa68dbd49c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "21aa07b1-efc0-4b37-b510-d027c1d36c0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "be197625-36ee-4e9c-8d56-b5c0158d67d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "0f2fe8cd-a9b9-47be-a7ad-982cd1eda0fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "17ea8df3-08d8-4568-94d8-299e38b32d82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "4b609b34-7493-4568-b46b-205ad46f7768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "1c7ddf37-6b3a-4d90-b3d6-3c6cae3a9f91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "c855d59e-11d1-479e-ade2-3d10e69e47fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "cca06bd6-8a1e-418b-9ee2-006e416d6cb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "ce371146-5d73-4047-b96f-85fa6290acd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "7e9ba920-24da-4e5d-a899-ebf07ed67577",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "e451da68-31af-44e9-beaf-2a87e76c2888",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "2e8a4fbf-6511-44d6-9e34-6510d81bde68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "7f8f8aa7-1a1a-4e2b-871c-aa50ea811d12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "4664637f-b618-4931-81f9-b1efb57d0ff7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "62f65cf9-f481-467b-b0d1-030172bdb196",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "a23c8cdc-9f50-46be-86ca-f87e045efaae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "7ab0f37d-097e-4219-a10e-38597012f8b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "e73698fd-e175-4d31-a02b-f802c1be2544",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "2a43220c-5ae4-4e30-8e64-bf6e8b025627",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "0bca691c-0c8d-45bc-8c9b-0fe3c83f1099",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "70a093ab-385a-4447-a8cb-91129794a802",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "9a714386-7a9b-46d9-92a0-3c0f454b9943",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        },
        {
            "id": "ce0f4d7e-e3b1-45e7-a6ba-900e0b90f67e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1040,
            "second": 1058
        },
        {
            "id": "9b3599b7-7707-4dd0-b98f-68d03beb44bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1040,
            "second": 1059
        },
        {
            "id": "df65e44e-2075-4cf7-9807-b48b7ebdec8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1040,
            "second": 1063
        },
        {
            "id": "703353ec-c04f-4ddf-a772-48c7657d7ea8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1040,
            "second": 8217
        },
        {
            "id": "a668e01c-68ef-4f3c-8a77-c480bf4dd1bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1041,
            "second": 1058
        },
        {
            "id": "dcd3d8b2-eb85-41aa-9fd0-a342decd13f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1041,
            "second": 1063
        },
        {
            "id": "e71200e8-6502-4ebc-bad3-c4590df639f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1042,
            "second": 1058
        },
        {
            "id": "9896fd7a-cede-4d3d-9fb0-34fa309dca82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1042,
            "second": 1061
        },
        {
            "id": "9acb5c29-d78f-4f31-adcd-09540c2ca087",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1042,
            "second": 1063
        },
        {
            "id": "96684f02-4aa0-4a65-a497-241e779b6401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1042,
            "second": 1066
        },
        {
            "id": "af7356c2-c228-4be8-9a0d-6e718e3b504d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1043,
            "second": 44
        },
        {
            "id": "4e5b5b07-6d1f-49e5-b00e-b1ea47f0eb4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1043,
            "second": 46
        },
        {
            "id": "d9cf7387-4399-47d5-bbac-c0abdf0e9a91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 171
        },
        {
            "id": "6425cfdc-d8f3-4fa7-990a-ede2f3f2f796",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 187
        },
        {
            "id": "4cebd732-c9f3-4b05-aae5-c07a78a79b61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 1040
        },
        {
            "id": "d155d975-7e40-44f9-91e2-915337857f82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 1044
        },
        {
            "id": "70b7b723-7a53-41f2-a458-5c7243d3113b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 1051
        },
        {
            "id": "38915729-80e9-4af4-8689-02fa6bf3d633",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 1054
        },
        {
            "id": "f10800d5-1a4e-49fb-ad67-e4123e523efe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 1057
        },
        {
            "id": "7b3d6b1f-874a-44de-9e36-264e9cb6c14c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 1072
        },
        {
            "id": "6f8bdc95-66cf-4b73-b7c6-4a4129fd0ac7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 1074
        },
        {
            "id": "ea8a492c-5b98-462b-851c-9dfdbd173550",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 1076
        },
        {
            "id": "3fd72a25-ca39-4e75-8c4c-916c621f36f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 1077
        },
        {
            "id": "792fbd59-8256-4d49-97b3-1f51535f3a71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 1080
        },
        {
            "id": "bcf77395-da4c-4ba5-8483-04cb02b40f3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 1083
        },
        {
            "id": "8e46cf35-5ad1-4610-9152-c08164a69e31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 1084
        },
        {
            "id": "5ee8cf10-ade1-4387-899a-dea61c05c2ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 1085
        },
        {
            "id": "81c21119-a68f-4599-8810-8cef9caaa47b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 1086
        },
        {
            "id": "99ab1192-dc25-47b5-9797-0dcddcaa9064",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 1088
        },
        {
            "id": "b3b80554-6dd7-4f14-acfc-457b7eb93260",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 1091
        },
        {
            "id": "022ae9a4-b58a-4967-8eac-a9f79b88ff48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 1099
        },
        {
            "id": "88a21809-76bc-4812-aa3b-caa8a2045ae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 1100
        },
        {
            "id": "10a6bd24-bcc0-447f-bff5-370154dacfd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 1102
        },
        {
            "id": "1ffc4561-0c05-4c18-a285-aaef66c9b525",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 1103
        },
        {
            "id": "25c0843e-3e20-41fc-b81a-3ac916edee58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1054,
            "second": 1061
        },
        {
            "id": "bed14799-1376-48ec-a8a3-9acfff174450",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 1056,
            "second": 44
        },
        {
            "id": "4f975ee1-979a-4c84-93d7-288f615e8e72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 1056,
            "second": 46
        },
        {
            "id": "085f5fc2-7906-46d0-85a6-5551d5716c91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 187
        },
        {
            "id": "2fd87746-71e7-4470-8dd0-0f1deb59d53a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 1040
        },
        {
            "id": "5826d874-4b5b-442c-95c5-ff3e7ce6ff22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 1044
        },
        {
            "id": "b35dff02-f13b-447b-b9d3-db90a06df611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 1051
        },
        {
            "id": "40d0caf7-9e7e-4e26-a50d-e8232adcbe68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 1058
        },
        {
            "id": "05524c88-5b1d-4245-81c2-2f385f40479a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 1061
        },
        {
            "id": "e941654d-9641-4cf8-85c4-7049586d5a16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 1076
        },
        {
            "id": "d5eec5b0-93f6-4daf-a44c-07cbc118947e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 1077
        },
        {
            "id": "91e78514-30e5-465e-88e4-7a9d3baebdce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 1086
        },
        {
            "id": "dd90ec93-4365-4948-8643-e2c1b3512ff5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1057,
            "second": 1061
        },
        {
            "id": "cfa6e105-31d0-4d87-bb55-04a0051dd396",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1058,
            "second": 44
        },
        {
            "id": "b684bd22-bbf0-4e68-afcb-58e7c05830e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1058,
            "second": 46
        },
        {
            "id": "ccd8bf0b-f895-48c4-8de5-b06e1df2649c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 171
        },
        {
            "id": "d2b5f880-8494-4d8e-bd32-536d51e79a2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 187
        },
        {
            "id": "e94c70b2-b9f7-4ced-b11b-cf1884cb89ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1054
        },
        {
            "id": "1132b360-63a6-400a-baf2-5e3c3a51e16f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1072
        },
        {
            "id": "4ef73b43-b54b-4f5f-9d3d-a2980c173cf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1074
        },
        {
            "id": "a9c2a54a-5e90-4dec-998b-853cb7ad96a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1077
        },
        {
            "id": "1c2f6e56-cad2-43c1-802a-e12ed00d4a28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1080
        },
        {
            "id": "e0e2cbcd-a2da-4e4c-953e-eeb34fda5df4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1082
        },
        {
            "id": "8e758d56-bcd6-4c37-a053-a86de70375cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1083
        },
        {
            "id": "d1003e62-378a-4e77-bcdb-13eb8197ec1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1084
        },
        {
            "id": "503087ac-977b-40b7-99e4-af0ffc8355f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1086
        },
        {
            "id": "3d28bfa1-09b7-4a7e-92f5-c7eac7dc7366",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1087
        },
        {
            "id": "bb6f1f53-f7aa-42ac-851f-3839d3219c6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1088
        },
        {
            "id": "2c0e7773-1b0b-4f77-901e-b2d18a8b74b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1089
        },
        {
            "id": "4ac4fc58-00bb-437d-ad4b-e4e370a97de0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1091
        },
        {
            "id": "cb33dcd2-81ef-4885-94de-826c1a3e5fa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1093
        },
        {
            "id": "288f9cf1-2c73-45c3-b3aa-3d98ba7b5139",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1097
        },
        {
            "id": "11732741-e1df-47c5-b3c0-471309884d7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1099
        },
        {
            "id": "cad3ffa9-4821-4df5-bb07-51a8b3b6dfe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1100
        },
        {
            "id": "23ebf19e-ed86-465c-874d-91bd7dc9cd69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1102
        },
        {
            "id": "f3730ebc-8e6e-40f6-b552-c18c7344a3db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1103
        },
        {
            "id": "a9ef4718-a9c3-43e6-9bd5-2978124b544e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1059,
            "second": 44
        },
        {
            "id": "70344e7c-ffde-4947-9a61-7428cd6e6a77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1059,
            "second": 46
        },
        {
            "id": "a861505f-cd24-407e-ad2c-fd52868aaab1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 171
        },
        {
            "id": "af207695-0870-436c-80bd-6b0a66a25470",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 187
        },
        {
            "id": "f5a4f8e5-9a1a-4f7a-bf3e-d5f5511d6102",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1040
        },
        {
            "id": "af3c6bc4-cd2c-431f-a315-c9b3a19adda6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1044
        },
        {
            "id": "7c88b711-0c54-43e3-adb9-4defbb059fc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1074
        },
        {
            "id": "d33d5112-b651-48db-959e-cc53ea100ab2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1075
        },
        {
            "id": "19a8900c-38be-42e1-ac0f-3f9e6e767747",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1076
        },
        {
            "id": "88f9e9f9-e182-4471-b9dc-932cb1ecc3a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1077
        },
        {
            "id": "f1de6a69-bb66-4ab6-844c-03b47ce64d48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1079
        },
        {
            "id": "3f5e0d53-425d-410f-b2d6-f56ac015ddc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1080
        },
        {
            "id": "821853fd-4eae-498f-816e-4c1ed80c130d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1082
        },
        {
            "id": "2a679700-a454-4e52-b623-c63eba4c7988",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1083
        },
        {
            "id": "14b1f6b6-007b-43aa-8d95-6748953b99fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1084
        },
        {
            "id": "94c75624-610d-4288-bbad-b443ee6381bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1085
        },
        {
            "id": "503f1933-0334-483a-9e78-3612eb6240f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1086
        },
        {
            "id": "06ae5634-c23f-463b-8dcd-fe1022604ab7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1087
        },
        {
            "id": "37577fea-b544-40b3-bda3-4c7ffd3be581",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1088
        },
        {
            "id": "2a2a732b-1487-40f1-a621-228af4a34da4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1089
        },
        {
            "id": "68752e6f-4e1a-4267-b2c0-b676c8dd8ecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1093
        },
        {
            "id": "dfc6cccd-7442-4971-8a61-52b36e4fc820",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1094
        },
        {
            "id": "8d662f66-412f-4857-8b01-c9790b563ebf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1096
        },
        {
            "id": "6ad694a2-51de-4bb6-a0a2-e6f61c13abb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1097
        },
        {
            "id": "01801ac9-54d6-4b99-9509-2f1ceb54cac0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1102
        },
        {
            "id": "34985248-0ce1-4784-b0f9-6074cee3b093",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1103
        },
        {
            "id": "9a160064-b032-4e92-a41f-a5630972fd60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1060,
            "second": 1058
        },
        {
            "id": "ecd7dbef-55be-4c0d-b344-7b580bca00fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1060,
            "second": 1059
        },
        {
            "id": "11c4f62e-0829-46dd-ac3b-a1428857487a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1066,
            "second": 8217
        },
        {
            "id": "81f6f961-fb6a-4277-82db-c82fec0066f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1068,
            "second": 1058
        },
        {
            "id": "48a1862e-e0d8-47f3-905a-97ad6409e736",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1068,
            "second": 1061
        },
        {
            "id": "bcfff50f-6dd5-4b2f-a276-6e484555da80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1068,
            "second": 1063
        },
        {
            "id": "8de2d9ee-f61d-4ff0-bf70-d69d733e93f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1068,
            "second": 1071
        },
        {
            "id": "7631cd9c-b37e-4217-95aa-a0a4f956bcbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1068,
            "second": 8217
        },
        {
            "id": "4379c852-c850-4653-a2cd-cd1e869ddae9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1070,
            "second": 1044
        },
        {
            "id": "65c97c2b-6166-47c7-98cc-fc01d8a1630f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1070,
            "second": 1051
        },
        {
            "id": "16920b90-ceac-427d-9af7-74418da2e3e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1070,
            "second": 1058
        },
        {
            "id": "a4b3187e-2ab4-443a-807e-cf187fe036da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1070,
            "second": 1061
        },
        {
            "id": "cc4f41d0-332d-43f3-8699-97f707b45667",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1070,
            "second": 1076
        },
        {
            "id": "03add974-8e1c-43b8-a1aa-472775488c69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1070,
            "second": 1083
        },
        {
            "id": "481498e1-08cd-4d7d-bb16-7b07350ae6ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1073,
            "second": 1076
        },
        {
            "id": "6c1fa76c-4dbc-4759-aa05-9d913d5f6880",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1073,
            "second": 1083
        },
        {
            "id": "556db2df-70bb-424c-8547-a4eb9c9a86eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1074,
            "second": 1095
        },
        {
            "id": "9b2f17ff-99b2-4352-8ddf-cc35ecfda2a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1075,
            "second": 44
        },
        {
            "id": "ce334be3-b7f0-44a2-b8a5-7a334cb94906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1075,
            "second": 46
        },
        {
            "id": "d499c666-0f59-45b9-a6a6-6cdd55251a6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1075,
            "second": 1076
        },
        {
            "id": "206d9b41-9fdf-488f-b9a8-784685b24465",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1090,
            "second": 44
        },
        {
            "id": "fd6822aa-7cd5-40c0-ae68-eb13aa1eb2a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1090,
            "second": 46
        },
        {
            "id": "3147cf4e-7ee9-4b8e-aa04-dff390266a26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1091,
            "second": 44
        },
        {
            "id": "26e7abd7-47bf-4d4b-b10c-90259dfab21a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1091,
            "second": 46
        },
        {
            "id": "e608e010-e33b-4a57-a8f1-43f0eef898e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1100,
            "second": 1090
        },
        {
            "id": "e397790f-101e-4a3f-ab60-b26b32ed0f9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1100,
            "second": 1095
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 1040,
            "y": 1106
        },
        {
            "x": 65334,
            "y": 65334
        },
        {
            "x": 65345,
            "y": 65345
        },
        {
            "x": 65352,
            "y": 65353
        },
        {
            "x": 65358,
            "y": 65358
        },
        {
            "x": 65363,
            "y": 65363
        },
        {
            "x": 65369,
            "y": 65369
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}