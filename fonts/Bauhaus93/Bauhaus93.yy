{
    "id": "b947ff48-32aa-4cb5-970d-b0fc8c2ebaac",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "Bauhaus93",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bauhaus 93",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "edcfda12-964b-4f2d-8f51-4c2fd4c639e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 47,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "a040bb34-99ee-4763-8ef8-85cf84ff27a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 47,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 118,
                "y": 100
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b0bd1911-e8e8-4bcc-b31d-407c49c735a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 47,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 104,
                "y": 100
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "5179945c-b94a-4f34-a0ec-4cf5ff4e66e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 47,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 83,
                "y": 100
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "3146f8eb-ff65-4c4b-8075-143ea6ff7cef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 47,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 69,
                "y": 100
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "09f411e7-0ca7-4696-8389-9c6759be018e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 47,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 41,
                "y": 100
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "71fa3e0c-efdb-4060-b151-913610f53b8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 47,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 21,
                "y": 100
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "3f7f51bf-98cc-4cec-a1c4-5013d2eaa82b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 47,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 13,
                "y": 100
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "fc6b8b1f-d105-4457-bbdf-4890939f71f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 47,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 2,
                "y": 100
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "e01e92db-49c5-49f0-9067-235febf9ca1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 495,
                "y": 51
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "73d43427-72c9-4684-8a9c-ab38d4c373af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 47,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 128,
                "y": 100
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "e98fa54c-bb74-4fb2-a05d-da3e2f691dfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 47,
                "offset": 3,
                "shift": 24,
                "w": 17,
                "x": 476,
                "y": 51
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "6003ab12-2932-4888-b999-bec402d953f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 47,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 450,
                "y": 51
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "e3c99b0c-4d9f-4e39-a47f-459309e690de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 47,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 440,
                "y": 51
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "61e5907f-35b7-4dbe-a374-9de930381691",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 47,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 430,
                "y": 51
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "35380dee-8a13-4b08-ac01-378e91ff9ad7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 47,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 418,
                "y": 51
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "af5a2a9d-a21e-4335-9b44-c7e0f6485c9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 400,
                "y": 51
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "d6ac8072-603e-451c-b014-307217124b7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 47,
                "offset": 6,
                "shift": 18,
                "w": 7,
                "x": 391,
                "y": 51
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "96b958d6-1690-4e70-b0f8-57bc73fb363e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 373,
                "y": 51
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "1e49deeb-f268-4320-964e-5e7279b58877",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 47,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 359,
                "y": 51
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "305630b0-e60a-4e67-82e3-de875797966c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 341,
                "y": 51
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "8aad5d39-4db8-4fe6-97fc-2f7e9113f8f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 47,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 460,
                "y": 51
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a651c3cd-18f1-4eb5-a1a4-a5b07ee7d21b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 142,
                "y": 100
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "abf71ae0-c458-4116-9370-dd8f421a9120",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 162,
                "y": 100
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "d90f3e49-be58-4ed3-9b77-20b8519d6d35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 180,
                "y": 100
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "9bdc2f6f-7ffa-4d97-87f9-32d3f499802d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 57,
                "y": 149
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "502aa96f-52e0-4e46-aebb-7cfb04621f5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 47,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 47,
                "y": 149
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "5612fb79-ff28-4822-9438-0cba814102ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 47,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 37,
                "y": 149
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "72c2f2ae-df7f-4997-8a91-c063521d1a1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 47,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 21,
                "y": 149
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "052a876a-e3d0-462b-b09b-510efeedaa8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 47,
                "offset": 3,
                "shift": 24,
                "w": 17,
                "x": 2,
                "y": 149
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "3ff1a800-93fc-47a3-acc8-ffa21be3b22f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 47,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 478,
                "y": 100
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "c94ad404-84df-4fc0-a8b8-1faa6d815b19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 461,
                "y": 100
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "8ecc0007-bde4-4c7d-857b-a93930f263f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 47,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 437,
                "y": 100
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "527e7e74-641b-41b9-8419-710d70f78692",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 418,
                "y": 100
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "baf82925-2891-4b92-8691-90610f13a76f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 47,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 400,
                "y": 100
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "dbf0f705-369b-447d-8868-96842bd325be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 47,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 374,
                "y": 100
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "33d0cfb0-e3bb-4ab8-ba3d-28802eaeedd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 47,
                "offset": 2,
                "shift": 23,
                "w": 21,
                "x": 351,
                "y": 100
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "ff2ce15f-8694-4005-8dfb-8d93a50b3656",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 47,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 336,
                "y": 100
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "5eb5e496-10dc-4a4c-baf9-7435ee1c37ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 47,
                "offset": 1,
                "shift": 15,
                "w": 16,
                "x": 318,
                "y": 100
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "d2e1fa4a-abc0-4465-9490-eb4827d79884",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 47,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 296,
                "y": 100
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "9ab99d3a-ac0f-41cc-a3c7-250d0136745a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 47,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 278,
                "y": 100
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "51bb4839-587f-4aa7-97f7-2dfcb5135396",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 47,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 269,
                "y": 100
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "0d639c32-3696-47db-a68b-b1f7d355178c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 47,
                "offset": -1,
                "shift": 10,
                "w": 10,
                "x": 257,
                "y": 100
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "777d120e-66b4-4b52-9220-5eb03ab8f002",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 47,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 236,
                "y": 100
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "542e10a8-48f2-4588-b06a-436709b86489",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 47,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 224,
                "y": 100
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "f217e013-0afe-4cfd-9044-9c85290f0dc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 47,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 198,
                "y": 100
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "e6618aa3-db0e-46d7-98c4-1dde61563a7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 322,
                "y": 51
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "6d02e132-27dc-448b-b876-58348a30ad63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 47,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 296,
                "y": 51
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "6aa631f4-7472-444a-9bca-ed8fc2bed4a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 47,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 277,
                "y": 51
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "f0a6e103-40d1-4b01-bf78-cd9139d761e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 47,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 388,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "5320c89e-45ee-4e99-ba0c-88515f3b9bf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 47,
                "offset": 2,
                "shift": 19,
                "w": 18,
                "x": 356,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "9c471af5-bbe5-46dc-ae87-fdf6a29853df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 47,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 342,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "987a5259-d5e4-4809-9058-e6fa034a2b1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 47,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 323,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "d16961fa-606f-4fd3-a378-1fecf990ea9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 304,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "7c6b8872-4d9d-4650-9700-358c50072961",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 47,
                "offset": -2,
                "shift": 17,
                "w": 20,
                "x": 282,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "e2b406b9-2877-4b2e-bab2-66985b007fad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 47,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 256,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "9c2a5933-19cb-4313-b2cd-60deb06c4afd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 47,
                "offset": -2,
                "shift": 16,
                "w": 20,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "cf3877c0-be3d-4c2b-9204-2b382e45a7f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 47,
                "offset": -2,
                "shift": 16,
                "w": 20,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "3a27240e-5b16-4a2a-90be-ef28e5822aa0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 47,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 193,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "5bab5082-69d9-4bd0-9dac-5b6d319c36e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 376,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "1d04d8a6-2082-4278-9dde-68c883a44b16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 47,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "bd4aeabd-0649-4d3d-953d-c69b6055a41e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "37c04fc7-6cca-4442-845c-4d0813ef21d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 47,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 139,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "95719bfc-7639-4d87-aaae-94d87cc6ab27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 47,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "37c7c6f7-6f44-4892-950d-5825d7a8b8d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 47,
                "offset": 3,
                "shift": 16,
                "w": 8,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "fbece7a3-efed-4b9b-b6e9-50f808c046dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 47,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "9d63f451-d543-45e2-9eaf-f9e3770b75bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ff902880-0cff-4c4b-b7bd-6de37a64c685",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "60b29ac2-9f13-4a54-bc61-174083593891",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 47,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "bfbf1ff2-1683-4a7d-8343-40deae3481ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "254df489-0dcd-44e8-8618-177172a7fae8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 47,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 169,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "23f0f3fb-8ec1-4264-a526-54f91dff0725",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 414,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "bddd6829-8797-4a1f-a634-a89394173950",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 79,
                "y": 51
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "0296a97f-fe0a-4f70-b5e0-eea38a7ea54c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 47,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 433,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "ee0b4e46-7173-4fcd-9b4a-02c214568798",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 47,
                "offset": -3,
                "shift": 11,
                "w": 13,
                "x": 248,
                "y": 51
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "d63839cc-38c1-4b6e-9137-aac5168dc830",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 229,
                "y": 51
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "f0aae162-0886-4692-aef0-78cd7d504d55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 47,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 220,
                "y": 51
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "e14566dc-0bb4-40a7-977a-d8dba6e19731",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 47,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 195,
                "y": 51
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "78023ad7-21a1-43c1-8888-cfe95c46ada0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 178,
                "y": 51
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "ec15a725-4dd7-44a9-9d5a-2ae87bf52ca7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 47,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 158,
                "y": 51
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "e53e660c-5d49-4639-81ee-4c554c560cd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 47,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 139,
                "y": 51
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "90d64d95-4715-4fb3-9cfd-a46d0464238a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 47,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 119,
                "y": 51
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "a9211f11-f0c1-4d3d-8ab9-16ef4bebfb87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 47,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 107,
                "y": 51
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "e912a44a-5aa5-4f0d-bf60-beb50c8327b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 47,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 263,
                "y": 51
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "8b08e6f5-4926-41c1-aced-6ef48c91e44b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 47,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 96,
                "y": 51
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "1b2eb173-c89b-4d40-863a-393782f00fcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 62,
                "y": 51
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "849600d1-2396-4a77-ad6d-83bba14532de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 47,
                "offset": -2,
                "shift": 12,
                "w": 16,
                "x": 44,
                "y": 51
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "fefe5130-6f67-4965-888f-1a509b4b5abf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 47,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 19,
                "y": 51
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "9a7e4c43-f30f-47b4-a1e4-041b8bdb6595",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 51
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "c4cc15c1-fe56-4cbc-91b2-bfb6d38fedf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 47,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 492,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "934c8c9e-5a9a-4b14-a7c5-ae9ef07f487d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 47,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 474,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d632dfe5-e58c-4206-8546-58d3bdb86439",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 462,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "843b7856-f9b5-4e3e-987f-784a668f92f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 47,
                "offset": 5,
                "shift": 16,
                "w": 6,
                "x": 454,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f340bd98-e907-4884-b3e4-c8cb98cacc35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 47,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 442,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "7976308a-0298-4b94-bd46-4a437ed67eba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 47,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 77,
                "y": 149
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "dfe32809-e8e3-4e60-8dd4-8252f3612b60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 47,
                "offset": 6,
                "shift": 31,
                "w": 19,
                "x": 96,
                "y": 149
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}