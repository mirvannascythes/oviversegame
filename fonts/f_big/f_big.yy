{
    "id": "f6db747d-51c4-48d2-80c9-2a54dd1fb256",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "f_big",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "3dc93fff-67f1-4103-9a4f-528696f55593",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 381,
                "y": 197
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "aa9d24ea-4a7b-4b19-ab59-8d490919db74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 37,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 478,
                "y": 197
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "e6f858f2-d010-4b10-87ee-95c1048fbf5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 37,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 348,
                "y": 197
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "2e822014-12cd-4d80-bdfc-efc3261dd674",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 82,
                "y": 119
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "2026c664-c3d8-4a3e-addf-7764a60a4a3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 289,
                "y": 119
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "58e1b79d-475d-46f5-9a1a-feeaa065bda1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 37,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "1ba74e40-d04d-4dbc-8fdc-cbea18078666",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 475,
                "y": 41
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "caf478b6-7476-4055-9a9c-01197895f171",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 37,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 492,
                "y": 197
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "3ef1d463-0f91-45d7-98dc-53533eedefb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 37,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 326,
                "y": 197
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "eb5814c4-3402-4f05-81be-073677ce2f83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 37,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 359,
                "y": 197
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "9acedb7a-b2b8-433f-a251-004e5d46adaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 37,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 242,
                "y": 197
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "29955ad2-f84d-42c0-bcf3-b3e563aed7eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 254,
                "y": 158
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "54b5a6ca-aa06-43ba-b091-e88797c9980d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 37,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 471,
                "y": 197
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "ab00df69-5d26-48dc-8e7a-6c01cad26f7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 37,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 315,
                "y": 197
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "832659d8-062a-478f-8ee1-da98b6b64ae6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 37,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 464,
                "y": 197
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "ef00fac5-fc2d-4b11-8a59-67d9bb340222",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 337,
                "y": 197
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "18ac0533-5847-43b8-8b06-03306650c458",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 110,
                "y": 158
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "d76bb58b-4730-4576-860c-e693d6a280e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 37,
                "offset": 3,
                "shift": 18,
                "w": 9,
                "x": 370,
                "y": 197
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "665cb700-2122-491e-9b26-39448521e812",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 197,
                "y": 119
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "0a52c6b9-badc-4469-a226-991195652097",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 290,
                "y": 158
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "bd7e8b48-9718-4dd7-b95f-00fd87dfaf07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 159,
                "y": 119
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "62fe5d1e-3389-4337-8e5b-dbdd8f92cc46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 92,
                "y": 158
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "afcb25c8-32e6-4521-aa3a-0475c46c032b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 56,
                "y": 158
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "d18303e0-ad85-4607-84da-2532f185df41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 20,
                "y": 158
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "b22436eb-1992-4ccb-b18d-aed5156392e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 2,
                "y": 158
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "09d45673-eb6b-4370-9fc3-7ef388f30f22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 487,
                "y": 119
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "36e9ea30-8b32-4185-8fe6-247f65ad96c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 37,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 450,
                "y": 197
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "dbf502e8-3acd-413a-9d83-47f7e1a076ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 37,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 485,
                "y": 197
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "6d5e787c-49d3-41e4-bee0-2d4e6a38b213",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 415,
                "y": 119
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "5d98a79b-c081-4819-8d61-444090b6b441",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 433,
                "y": 119
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "750133d9-d356-4f6b-91c9-391ab4e23c73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 451,
                "y": 119
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "2d3cf7d3-aa24-4f7a-b385-3bbd86d4f492",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 469,
                "y": 119
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "4528bb4a-e35c-4d52-b588-f51f6dc12de5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 37,
                "offset": 1,
                "shift": 32,
                "w": 31,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "b291e6ee-6e57-4d22-93ca-8a56f3255a89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 37,
                "offset": -1,
                "shift": 21,
                "w": 23,
                "x": 320,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "edce618f-f86c-43db-85b5-89a71a39ba2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 440,
                "y": 80
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "d6347052-4144-4aec-b59d-9525d5d5470f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 37,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 430,
                "y": 41
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "12793d30-ebf2-4d5d-bef2-efc08eed40c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 37,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 453,
                "y": 41
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "7072ee67-c930-4234-a837-e3797e2e277e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 360,
                "y": 80
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "7ef59784-d3ea-4265-9d0e-469d20e432b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 37,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 216,
                "y": 119
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "079b8e81-d388-4a8f-8dc1-311a53e47e3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 37,
                "offset": 1,
                "shift": 25,
                "w": 22,
                "x": 174,
                "y": 41
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "b3b6edad-df23-47fe-96ef-6f0a94b3b6ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 37,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 277,
                "y": 80
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "4d5e9384-e9c9-4afa-97e9-fcc098291ab2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 37,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 457,
                "y": 197
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "92bca045-664e-4943-97c2-3f579b323084",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 115,
                "y": 197
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "a9d67461-e79c-46fb-8d25-5d95f2f2e573",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 20,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "949bd945-19b9-497e-9c81-42e975cfca10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 393,
                "y": 158
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "ebc20c61-ea35-4d7c-b5e0-ad96ff461d2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 37,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 27,
                "y": 41
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "f2df9111-de16-413e-9d57-ba7ffe1ac008",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 37,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 319,
                "y": 80
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "a25f63d3-3e9e-48ab-a9f0-faacdeb8fd4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 37,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 2,
                "y": 41
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "99f9a006-4276-41d1-bd61-1111e1ea76f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 42,
                "y": 119
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "58c177b2-f368-4f4d-843d-94afe6ebbd83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 37,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 470,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "2eb4cd38-4e97-43f2-9919-ae6a88a8e9b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 37,
                "offset": 2,
                "shift": 23,
                "w": 21,
                "x": 361,
                "y": 41
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "47a64c8c-6ca3-4032-a801-e5f77f2b328d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 298,
                "y": 80
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "8ffa5458-a428-4e62-8ac1-4eb4a51bf0f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 37,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 235,
                "y": 80
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "38638589-5e6c-4345-b53a-9f146a73d4e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 37,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 214,
                "y": 80
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "2a5708f2-486a-48d9-88cf-fc37bfcef26a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 37,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 198,
                "y": 41
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "ee074cd1-9cea-43fd-b430-815b0b8ac64e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 37,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "ad6a2f90-f99c-4166-8604-c3d6bececb0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 37,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 150,
                "y": 41
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "adb34870-0908-4521-9b5d-91d337045e29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 37,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 126,
                "y": 41
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "bc5bb7de-8733-46e5-83dd-3ea20453344d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 37,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 172,
                "y": 80
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "52100e16-0e90-421d-aa8b-1ce165dc2e8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 37,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 414,
                "y": 197
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "30b4fddd-9ab0-4cf0-b182-d179f0a0d5fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 392,
                "y": 197
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "74c26daf-cc35-40ac-8d12-7982e6a2f07a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 423,
                "y": 197
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "12242d54-eb04-4225-a06c-3895687e1906",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 444,
                "y": 158
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "dcf8a11d-57d8-48e3-93fb-cdbdc2f186bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 37,
                "offset": -1,
                "shift": 18,
                "w": 20,
                "x": 24,
                "y": 80
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "40e98b67-1439-4831-a601-c560cd38952d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 37,
                "offset": 1,
                "shift": 11,
                "w": 7,
                "x": 432,
                "y": 197
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "c5e3aa6a-b652-4c62-9ecc-1ccb603a8bc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 164,
                "y": 158
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "33e69053-8cea-46b6-8318-62c93ec5a737",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 461,
                "y": 158
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "6dd845d9-d1db-4b61-8792-ea184bd57ac0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 376,
                "y": 158
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "05e86936-e342-41a6-bf55-f64b02bdb9b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 2,
                "y": 197
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "53619be4-7e53-4740-b6aa-687db62e9dfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 218,
                "y": 158
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e69c9d05-58ce-4c11-9709-7b5a99f64a82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 255,
                "y": 197
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "77db9d22-ae34-40d7-ad10-d169da535725",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 478,
                "y": 158
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "d99e0cfd-a1ca-486b-b05c-4fdbb3d5b786",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 147,
                "y": 197
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "bb374ca4-c56f-4308-815a-3038fe923493",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 37,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 504,
                "y": 197
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "6851869d-2347-4016-830b-43d0d155d012",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 37,
                "offset": -2,
                "shift": 7,
                "w": 7,
                "x": 441,
                "y": 197
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "0baefe78-9496-4fd5-9751-02c533e4e271",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 179,
                "y": 197
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "276657fb-f1e9-4a4f-b224-87d1c59ac842",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 37,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 2,
                "y": 236
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "fd170ca7-3452-497e-934a-b5d19ee84221",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 37,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 52,
                "y": 41
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "176ac433-9f57-412f-be0b-61bd6be85d86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 195,
                "y": 197
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "a242f5b5-2f90-4b2e-a0cc-0338ff40fba7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 397,
                "y": 119
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "3d311cd2-47b8-4b1c-bdae-9cc67c8a816c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 359,
                "y": 158
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "de41cf37-73c8-4a79-9270-20bcc1807cca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 427,
                "y": 158
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "fd5354a4-44fb-41a2-9ec7-ba2819ba72d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 37,
                "offset": 2,
                "shift": 11,
                "w": 10,
                "x": 279,
                "y": 197
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "75e0f098-d851-4779-86ce-dcacee18bc19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 410,
                "y": 158
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "97682879-c7f1-48b1-b505-0f6867709ae8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 403,
                "y": 197
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "3ddbd347-cca9-4e60-93e6-a4983898ded9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 163,
                "y": 197
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "279e7fcd-ac26-4ceb-93b0-41c3a2e86222",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 271,
                "y": 119
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "df17c8d1-5b8f-4606-9103-545988ed6a2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 37,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 370,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "f6b70aad-0a44-4014-a5aa-872c64bba7ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 307,
                "y": 119
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "ba843387-2c79-4561-a874-f54398b9bdd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 325,
                "y": 119
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "10bf9a01-ee45-4d73-8c12-6383648f0d5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 361,
                "y": 119
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "33167149-4cc4-4437-8815-ce56d4c7eefe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 37,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 291,
                "y": 197
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "bb330cda-9965-4f68-8d53-ccaf00600e8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 37,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 498,
                "y": 197
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "6f6cf00c-4bbb-46f3-bf53-196159612358",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 37,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 267,
                "y": 197
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "dff4a06d-3f8b-444c-b2de-8933ab2f0619",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 102,
                "y": 119
            }
        },
        {
            "Key": 1040,
            "Value": {
                "id": "41142e8e-de12-48c1-b5b2-6d7fddfa7065",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1040,
                "h": 37,
                "offset": -1,
                "shift": 21,
                "w": 23,
                "x": 420,
                "y": 2
            }
        },
        {
            "Key": 1041,
            "Value": {
                "id": "8931fc0f-aab8-40a9-969f-5966418b0d9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1041,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 22,
                "y": 119
            }
        },
        {
            "Key": 1042,
            "Value": {
                "id": "7bdddee1-3ae1-4dea-9d8a-1972c329ee19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1042,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 2,
                "y": 119
            }
        },
        {
            "Key": 1043,
            "Value": {
                "id": "969b9698-43d5-47ae-97bc-144ca83a4052",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1043,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 16,
                "x": 253,
                "y": 119
            }
        },
        {
            "Key": 1044,
            "Value": {
                "id": "93a68cdb-64b4-43ec-9aee-876146588b6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1044,
                "h": 37,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 384,
                "y": 41
            }
        },
        {
            "Key": 1045,
            "Value": {
                "id": "5789670a-2041-46b6-9ad2-6f509f32d0b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1045,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 400,
                "y": 80
            }
        },
        {
            "Key": 1046,
            "Value": {
                "id": "39d1c6be-c15c-47ae-91ad-dedba285df5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1046,
                "h": 37,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 1047,
            "Value": {
                "id": "2cec1532-c7fa-436d-b0ca-4d81c4769f4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1047,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 340,
                "y": 80
            }
        },
        {
            "Key": 1048,
            "Value": {
                "id": "c841b020-851d-4d82-8812-cfb2eff9f415",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1048,
                "h": 37,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 193,
                "y": 80
            }
        },
        {
            "Key": 1049,
            "Value": {
                "id": "b66f9f73-d376-47b8-a046-dd7e13ad7776",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1049,
                "h": 37,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 130,
                "y": 80
            }
        },
        {
            "Key": 1050,
            "Value": {
                "id": "af727943-3d6d-40a4-b773-74567a811cac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1050,
                "h": 37,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 178,
                "y": 119
            }
        },
        {
            "Key": 1051,
            "Value": {
                "id": "17a2a44c-306f-4085-865d-67d2b024c09e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1051,
                "h": 37,
                "offset": 0,
                "shift": 21,
                "w": 19,
                "x": 109,
                "y": 80
            }
        },
        {
            "Key": 1052,
            "Value": {
                "id": "15be824b-313d-4abf-911c-53c533d8f58e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1052,
                "h": 37,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 445,
                "y": 2
            }
        },
        {
            "Key": 1053,
            "Value": {
                "id": "d108f486-5c87-4f4a-9a0f-b91019b7b529",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1053,
                "h": 37,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 256,
                "y": 80
            }
        },
        {
            "Key": 1054,
            "Value": {
                "id": "a8fe8e6a-a615-4c96-bbd4-e5e2672e8d80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1054,
                "h": 37,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 77,
                "y": 41
            }
        },
        {
            "Key": 1055,
            "Value": {
                "id": "31dd3b22-a6e2-45ab-b0d7-4d663ee783b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1055,
                "h": 37,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 151,
                "y": 80
            }
        },
        {
            "Key": 1056,
            "Value": {
                "id": "374ad5ab-7bc2-4951-a243-2167d01625d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1056,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 62,
                "y": 119
            }
        },
        {
            "Key": 1057,
            "Value": {
                "id": "b9b3da43-d1cf-469a-b3db-f6bd22ff297b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1057,
                "h": 37,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 315,
                "y": 41
            }
        },
        {
            "Key": 1058,
            "Value": {
                "id": "cee68fb5-2448-4dac-946d-93c8d47df75f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1058,
                "h": 37,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 46,
                "y": 80
            }
        },
        {
            "Key": 1059,
            "Value": {
                "id": "c4e19ee7-2e9c-4be6-b3fb-5930f308618f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1059,
                "h": 37,
                "offset": 0,
                "shift": 20,
                "w": 21,
                "x": 269,
                "y": 41
            }
        },
        {
            "Key": 1060,
            "Value": {
                "id": "2f6c4c27-00da-4487-bc90-6bb1bad12e3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1060,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 345,
                "y": 2
            }
        },
        {
            "Key": 1061,
            "Value": {
                "id": "01a8a7c0-2b37-4885-b710-b9892b306d26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1061,
                "h": 37,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 102,
                "y": 41
            }
        },
        {
            "Key": 1062,
            "Value": {
                "id": "5197b892-76e1-4fe2-b6fb-991d344010d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1062,
                "h": 37,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 338,
                "y": 41
            }
        },
        {
            "Key": 1063,
            "Value": {
                "id": "c000f37c-ba6e-474c-ae97-4ee861bc4e9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1063,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 380,
                "y": 80
            }
        },
        {
            "Key": 1064,
            "Value": {
                "id": "7989e96f-8008-46ac-b0ad-e2ee7ebec6fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1064,
                "h": 37,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 241,
                "y": 2
            }
        },
        {
            "Key": 1065,
            "Value": {
                "id": "a75adbfa-4066-4c68-837a-e89878a148f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1065,
                "h": 37,
                "offset": 2,
                "shift": 30,
                "w": 27,
                "x": 130,
                "y": 2
            }
        },
        {
            "Key": 1066,
            "Value": {
                "id": "fe606cda-1612-4200-a8fc-22948a3bc832",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1066,
                "h": 37,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 214,
                "y": 2
            }
        },
        {
            "Key": 1067,
            "Value": {
                "id": "cd76d2f3-b406-458d-968e-ba7af1146cb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1067,
                "h": 37,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 268,
                "y": 2
            }
        },
        {
            "Key": 1068,
            "Value": {
                "id": "cffcd0ed-4cde-4cd3-82b1-1a0dbd64b01f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1068,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 420,
                "y": 80
            }
        },
        {
            "Key": 1069,
            "Value": {
                "id": "4f40e564-625f-4da8-b04a-1d2b0efaa9ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1069,
                "h": 37,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 292,
                "y": 41
            }
        },
        {
            "Key": 1070,
            "Value": {
                "id": "4b13d1f5-49d9-408f-99eb-ceed2f0f80cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1070,
                "h": 37,
                "offset": 2,
                "shift": 32,
                "w": 29,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 1071,
            "Value": {
                "id": "7c148655-a71c-490d-a24e-f06ea11010fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1071,
                "h": 37,
                "offset": 0,
                "shift": 23,
                "w": 21,
                "x": 246,
                "y": 41
            }
        },
        {
            "Key": 1072,
            "Value": {
                "id": "16731edc-7d73-47c1-9f9a-49484ac2938c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1072,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 182,
                "y": 158
            }
        },
        {
            "Key": 1073,
            "Value": {
                "id": "99e22525-21a6-4b2f-9c09-567f809cce02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1073,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 140,
                "y": 119
            }
        },
        {
            "Key": 1074,
            "Value": {
                "id": "d1426d54-e255-4af2-9d0a-5778ef0f874a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1074,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 99,
                "y": 197
            }
        },
        {
            "Key": 1075,
            "Value": {
                "id": "90e5b7c8-ec09-42d5-b35e-a986c99b2cd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1075,
                "h": 37,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 303,
                "y": 197
            }
        },
        {
            "Key": 1076,
            "Value": {
                "id": "1a246239-1d3f-4d18-9f82-7953f5dd9d59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1076,
                "h": 37,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 460,
                "y": 80
            }
        },
        {
            "Key": 1077,
            "Value": {
                "id": "99f50e00-fe31-4061-96f9-819185f81274",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1077,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 146,
                "y": 158
            }
        },
        {
            "Key": 1078,
            "Value": {
                "id": "49fe3f95-1b71-4db1-b010-49c7867b263e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1078,
                "h": 37,
                "offset": -1,
                "shift": 21,
                "w": 23,
                "x": 395,
                "y": 2
            }
        },
        {
            "Key": 1079,
            "Value": {
                "id": "a7713c82-28ae-47cb-9338-8e48c7854285",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1079,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 83,
                "y": 197
            }
        },
        {
            "Key": 1080,
            "Value": {
                "id": "91a56e48-0536-45cc-8b49-2b2a9e160701",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1080,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 67,
                "y": 197
            }
        },
        {
            "Key": 1081,
            "Value": {
                "id": "fba6a93c-d40f-44e2-be66-2d8eb50947ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1081,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 131,
                "y": 197
            }
        },
        {
            "Key": 1082,
            "Value": {
                "id": "19f9fac4-fb34-4ea8-a36e-5adf6efb737b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1082,
                "h": 37,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 227,
                "y": 197
            }
        },
        {
            "Key": 1083,
            "Value": {
                "id": "385d1975-dbfa-4ea5-bcf2-ac00f4d96332",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1083,
                "h": 37,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 121,
                "y": 119
            }
        },
        {
            "Key": 1084,
            "Value": {
                "id": "08414f89-f228-4df4-b4a1-3a8fb53fe687",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1084,
                "h": 37,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 480,
                "y": 80
            }
        },
        {
            "Key": 1085,
            "Value": {
                "id": "230436be-247b-4865-9c73-35a3714c4170",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1085,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 35,
                "y": 197
            }
        },
        {
            "Key": 1086,
            "Value": {
                "id": "d729ec72-1ad5-4188-81d5-2e60bdb88df9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1086,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 38,
                "y": 158
            }
        },
        {
            "Key": 1087,
            "Value": {
                "id": "8bbd5530-0326-4701-98d3-1d8569094f8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1087,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 19,
                "y": 197
            }
        },
        {
            "Key": 1088,
            "Value": {
                "id": "5092eb5a-405e-48da-985a-0fe8bac375af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1088,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 308,
                "y": 158
            }
        },
        {
            "Key": 1089,
            "Value": {
                "id": "b3418ec6-820f-47d8-8fe4-eb2a805e0353",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1089,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 325,
                "y": 158
            }
        },
        {
            "Key": 1090,
            "Value": {
                "id": "fdf56512-e830-473f-85e3-4374e5abec42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1090,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 342,
                "y": 158
            }
        },
        {
            "Key": 1091,
            "Value": {
                "id": "e8f43525-06bc-492c-bda9-b6bdd167d69b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1091,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 200,
                "y": 158
            }
        },
        {
            "Key": 1092,
            "Value": {
                "id": "cee2c65a-d17e-46c8-8a2b-3c81358b40a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1092,
                "h": 37,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 187,
                "y": 2
            }
        },
        {
            "Key": 1093,
            "Value": {
                "id": "8f0bf10e-6fc2-4444-931a-90d212038231",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1093,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 128,
                "y": 158
            }
        },
        {
            "Key": 1094,
            "Value": {
                "id": "2e5a5aa0-8abd-4964-b8dd-993662267cc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1094,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 236,
                "y": 158
            }
        },
        {
            "Key": 1095,
            "Value": {
                "id": "1393e0be-e402-4866-93a4-c8d9d0095c67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1095,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 51,
                "y": 197
            }
        },
        {
            "Key": 1096,
            "Value": {
                "id": "d2c2de54-0d18-41aa-b820-29b0a93ac8c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1096,
                "h": 37,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 222,
                "y": 41
            }
        },
        {
            "Key": 1097,
            "Value": {
                "id": "b92d9b28-0cd3-4cd6-97ed-1ccb4033b7ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1097,
                "h": 37,
                "offset": 2,
                "shift": 26,
                "w": 24,
                "x": 294,
                "y": 2
            }
        },
        {
            "Key": 1098,
            "Value": {
                "id": "70283be7-226c-430a-8891-9d22ff3d3133",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1098,
                "h": 37,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 88,
                "y": 80
            }
        },
        {
            "Key": 1099,
            "Value": {
                "id": "297e6be3-00ee-47e4-b6cd-3b9787a69b0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1099,
                "h": 37,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 67,
                "y": 80
            }
        },
        {
            "Key": 1100,
            "Value": {
                "id": "941652ed-0a5a-4c1a-bf62-a5b0202f196a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1100,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 211,
                "y": 197
            }
        },
        {
            "Key": 1101,
            "Value": {
                "id": "f471138c-29cb-4f7f-8e35-c780af5d2b07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1101,
                "h": 37,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 272,
                "y": 158
            }
        },
        {
            "Key": 1102,
            "Value": {
                "id": "8e70b7b3-9357-4900-9db4-5d94cb2b4dc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1102,
                "h": 37,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 407,
                "y": 41
            }
        },
        {
            "Key": 1103,
            "Value": {
                "id": "3c40c26b-2c07-4a2f-9bde-566235e4d2d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1103,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 343,
                "y": 119
            }
        },
        {
            "Key": 1104,
            "Value": {
                "id": "b77d40fa-f049-4114-b920-f25cdcab85fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1104,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 235,
                "y": 119
            }
        },
        {
            "Key": 1105,
            "Value": {
                "id": "ce3593bc-1067-4fd1-9ebe-c91c15faecd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1105,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 74,
                "y": 158
            }
        },
        {
            "Key": 1106,
            "Value": {
                "id": "efc17123-f171-4fa9-91de-8798c4b5c5fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 1106,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 16,
                "x": 379,
                "y": 119
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "60ff9d74-05fb-49fb-966a-976fb44da046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 65
        },
        {
            "id": "d31e53b9-5a26-4052-8545-7f4c4e03dcc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 84
        },
        {
            "id": "a919183a-295a-4e62-b2ab-b80656f15420",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "1137ee4e-2156-40e0-b523-df1721cfc831",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 902
        },
        {
            "id": "c7cd4f77-4e19-45ac-a4cd-6e0fb8f2fea3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 913
        },
        {
            "id": "dc5ef8b2-ba7b-4feb-986e-fb4e18b0cf50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 916
        },
        {
            "id": "8636d70c-c5cf-4887-a729-ceb91d14adb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 923
        },
        {
            "id": "73c19941-28de-488d-b817-0ba22398ddf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 932
        },
        {
            "id": "465001b7-eccb-4cc2-8803-dbc98536af3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 933
        },
        {
            "id": "8f724b81-f7c3-49f6-b47b-1e4884217d41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 939
        },
        {
            "id": "601058e2-b5b3-4912-9618-6d46d6defb5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 49
        },
        {
            "id": "8749a293-a760-4802-a252-6a82af137b12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 32
        },
        {
            "id": "bfbc45bb-a9b2-4904-ab2b-e1e121759515",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "1ae3aa95-0d7b-46dc-99f7-b21addcb23f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "5da78f86-b425-4164-b2b9-c719ce844b65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "aa1c09c2-4acd-4a10-87aa-6dbece15dc01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "5489256b-bd04-4ea9-bba0-6c72be89b504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "2e523abc-0c2b-42a1-ade8-18d0cf9cf94f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "8bc02546-18ce-4abb-af62-e4ef20cbd1a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "af9f3f11-8fce-4cee-8da4-10b969e3bb49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 160
        },
        {
            "id": "97bd0a4e-1871-48d7-bc57-772cd87cd014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8217
        },
        {
            "id": "6cb15d39-d2fe-49d3-a5a6-892d9d7b9272",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 44
        },
        {
            "id": "7b0631ef-31d1-439e-966e-7aba3c622606",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 46
        },
        {
            "id": "042fb8a2-de88-444c-8138-aae78b3d6d15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 65
        },
        {
            "id": "8cd7ba73-f460-44ed-982f-2d132db2a080",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "c1b6e9d1-be15-4647-8f62-51095eaf5ab6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "aaae2492-1d49-4743-9ff5-f9d559190c81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "19f7c262-636b-4e16-9c09-b1afd44d7b93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "31ccc905-f8e4-4c3f-aa27-8fd17fc94768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "a0432894-f42a-4bb3-907d-5e36102f3fdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "0752ca23-0e89-4a50-a20f-bf7c37348037",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "7f7cd965-8c6d-4f07-980d-c01458e561a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8217
        },
        {
            "id": "dd610444-f408-4805-b119-921558139032",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "2fe51889-d1b8-4c3e-9ee3-29c849859e67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 44
        },
        {
            "id": "1bd97313-8517-4b70-a575-62b3917ea7c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 46
        },
        {
            "id": "48212f64-17f6-4ec0-bda2-b0464d4e3683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "16676683-35aa-4de2-9676-fb98aae4bbb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 160
        },
        {
            "id": "19393702-f10a-4e11-b18c-eb465c2404db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "4b6b6050-d70c-4b1d-bfed-1f5e2c5df893",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "de814b49-842e-4a74-8839-2ad27644b992",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "9e5b9421-2924-4bdc-bdb3-bc3644308952",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "1f5934e7-6341-4a30-bc75-40daf2aa0440",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 32
        },
        {
            "id": "ae2cb4f7-83b9-45b0-9b0b-7a8aece64fdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 44
        },
        {
            "id": "181d389d-7e63-4305-ac86-44ec14e7f983",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 45
        },
        {
            "id": "73318b14-02cd-4e32-a672-707118df349c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 46
        },
        {
            "id": "1651c795-b04b-48b2-a863-af1e492e3eb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 58
        },
        {
            "id": "72adaa58-94bc-43ff-9428-1d16603d67ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 59
        },
        {
            "id": "d0b711d5-c570-4034-ad8e-b2a66b00872d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "420ec4a6-1f82-44ce-8c72-92f67715a598",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "28b9d0ab-939f-4377-a438-ea29190de188",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 97
        },
        {
            "id": "bfe81d67-14df-42f2-92fb-c62ec4c1f766",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 99
        },
        {
            "id": "bc2be774-1c0c-4f51-a1bc-fccc5dcaa401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 101
        },
        {
            "id": "63e9b01a-0505-4697-8903-51452a0e71b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "b8cb910c-1b3a-49f2-9b4f-54fae97dfe25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 111
        },
        {
            "id": "305fd211-d36e-43ad-a891-d324ff399628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "0e7899f6-f54d-48c5-81c4-002e24f08ff4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 115
        },
        {
            "id": "b3b0b9b0-0f1a-41de-a014-6f4ea46e58fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "5a382410-091d-4e3e-9f79-cfd8be184f8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 119
        },
        {
            "id": "fc4d27fa-0691-41e5-9c55-6f2c88ed5068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 121
        },
        {
            "id": "f1f62e6f-d20c-477e-9576-1ab1f62041d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 160
        },
        {
            "id": "4938f76f-9357-42a3-bf02-ccb2f7d78649",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 173
        },
        {
            "id": "8660a341-bd57-40cb-ad46-edafe788bfb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 894
        },
        {
            "id": "61c8783d-e32f-4463-83fc-489159b6d419",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 44
        },
        {
            "id": "d26db444-2564-4751-ad85-079b25939aac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 45
        },
        {
            "id": "a8c30435-07c0-4fd5-bf4a-f85e50e7cc16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 46
        },
        {
            "id": "16a604b8-0228-45fa-8470-0dd29f33c965",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "958c7873-9758-4cdc-acca-ca848772cc11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "38c3b678-c70f-4288-bbfb-6754f2b0e8c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "0d1c586c-9706-4448-937d-f0cec506f159",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "8ffdbf64-5b17-4493-a552-d90c6c5c557f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 101
        },
        {
            "id": "9c61d279-5e6d-49e4-a6c0-903e0c47e8ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "acea822a-563d-4019-bc7e-b9fede8728f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 111
        },
        {
            "id": "6c032be7-9c8c-42d6-b723-f9a51b36f27c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "6f8a6e31-11ad-487e-a57f-03fcf08bc3c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "b044983a-5575-46dc-9ae8-8f2c22269e1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "88bcbfc5-7c22-4f4c-b3a7-45d0369ad187",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 173
        },
        {
            "id": "2bee51a5-680f-4584-999e-ccb810c5654e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "bd2ec333-f8ca-46aa-a785-385883ed2582",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "04ed4757-666a-40e6-ac51-ce9d76fb1a17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "5d67e3e6-6ddd-4de8-8e34-d6ef0c64fe4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "45b5787e-f9af-4f13-8f09-14d443762fcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "6fda9763-aa71-4efd-949d-1f8fc07db196",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "768d2233-3bac-4508-b754-f2d42650590c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "fc79b5c1-758e-4bfe-8785-4afe89b20ef1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "e6e4832c-9b91-49f1-abb6-cb6cf7586499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "a5c02c55-9ad5-4db9-878b-5a3c2fb73155",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "c84c4207-7e2c-4ced-9921-f497bab7324f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "a4f58dad-1378-445e-bce7-22a7229309dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "77ef526c-721f-4f8f-9470-588c21cdcbb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "4137059e-95a5-4e12-8f8d-9d77c2f96c00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "75df8565-72c6-4bf9-b308-00a5aae8997f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 32
        },
        {
            "id": "ef6fc89e-d69b-494f-889e-f684ad762819",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 44
        },
        {
            "id": "6eb6a9ee-67f7-4f8d-80e5-1b056b5dc07c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 45
        },
        {
            "id": "db22986c-37b8-4696-94d2-bb79e6e0f9a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 46
        },
        {
            "id": "f4b79b2e-8a31-40f0-bf04-cebafe28a003",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 58
        },
        {
            "id": "2b180ee5-76ed-4290-8f13-9cd18556b072",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 59
        },
        {
            "id": "384c1f8c-5b34-461e-b727-4e7326deaa79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "182ac24b-496f-48de-83ce-c0a70795976c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "a27bff40-fb65-45bb-8b9c-190ec5b9d353",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 101
        },
        {
            "id": "5ae7d682-f791-4e2d-bce2-2a4e19ff1f81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "5cae4924-8566-4c89-8467-050be6750ac7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 111
        },
        {
            "id": "1a986368-4618-4e2b-8105-e1306e806c27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "4a3c6c68-c6ae-476c-8a10-5a14c2707b34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 113
        },
        {
            "id": "cce54028-2184-4a72-ba17-6ba986b283e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 117
        },
        {
            "id": "7cbb044d-94ed-4579-9b6d-5ddd13b6b09d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 118
        },
        {
            "id": "88d8bdce-8f3d-42af-aa6b-2df362f36596",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 160
        },
        {
            "id": "0017c178-a555-4494-9bf2-d8eb0ebe4f54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 173
        },
        {
            "id": "ba9042e1-6582-4bab-bf5a-d1ab49c48d24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 894
        },
        {
            "id": "30bb86a8-45cf-4a73-8c8f-ab59b15ba0b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 102
        },
        {
            "id": "ac013532-176f-4cab-9834-40df92c488de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "acd7130e-5b4d-43fc-8a42-81e6cece1598",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 44
        },
        {
            "id": "c49e5643-dd30-4d73-9b1f-cc51de62869e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 46
        },
        {
            "id": "573bd8a9-3d92-4736-a4f7-d16076b3ae4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "649ccbf9-5ea3-4a15-9d74-6139ab749ac0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "cfb47c37-f5a8-44e6-86f5-82648c744e6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "227e2b42-b2c6-49fe-9900-100ca216b26c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 44
        },
        {
            "id": "6b034450-31d2-405e-81b9-6bd0f4ffc738",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 46
        },
        {
            "id": "b4594f76-45ab-49ad-98c4-53220dcb3400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 44
        },
        {
            "id": "942acc4c-293d-423d-8674-bf60b31124f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 46
        },
        {
            "id": "609df497-454f-4749-89ee-f5a9c85efbb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 1040,
            "second": 1044
        },
        {
            "id": "da8b92d0-15a0-4bc6-8534-272eda89e563",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 1040,
            "second": 1051
        },
        {
            "id": "3d0a6ef0-2172-47dd-9055-97a8d3b5cfae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1040,
            "second": 1054
        },
        {
            "id": "6064ac26-af2a-47ef-a764-faeb5ca5e7c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1040,
            "second": 1057
        },
        {
            "id": "4eab5042-60d9-4da2-bdd6-bf856ec829c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1040,
            "second": 1058
        },
        {
            "id": "b35fe0e8-c1d3-4f29-9cb4-6aa2a9325cac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1040,
            "second": 1059
        },
        {
            "id": "00cef6f3-5381-4584-b1cc-b01dafc5e328",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1040,
            "second": 1060
        },
        {
            "id": "2492a969-e549-4e2b-b54c-19f6153c201a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1040,
            "second": 1063
        },
        {
            "id": "78d69113-79d4-4e26-9068-08fe924e871c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1040,
            "second": 1069
        },
        {
            "id": "4268b18c-97a5-45db-bd5b-9746a8cb46af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1040,
            "second": 1090
        },
        {
            "id": "fe74ffc1-06b8-463f-9204-f81d961c7882",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 1040,
            "second": 1101
        },
        {
            "id": "21290b1c-34d7-4ba4-b975-0a515e93f42c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1040,
            "second": 8217
        },
        {
            "id": "aa5e058d-b5c5-4874-a32d-eb88a0af076c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1041,
            "second": 1040
        },
        {
            "id": "bbe411a6-9752-4e16-a19e-03bf21e92a2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1041,
            "second": 1058
        },
        {
            "id": "b14b820e-ffb3-41f4-a907-aa4baf53fbc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1041,
            "second": 1059
        },
        {
            "id": "ac9e4145-2ab0-45a7-9287-8f12784fa60e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1041,
            "second": 1061
        },
        {
            "id": "e6d83ac7-7c41-4d45-abd5-9121e3872fca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1041,
            "second": 1063
        },
        {
            "id": "2370da0a-3f0b-4c3e-8d5d-abd75fa56cf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1041,
            "second": 1066
        },
        {
            "id": "08356708-ceda-406e-8511-cb4b175445bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1041,
            "second": 1091
        },
        {
            "id": "737c3e25-ac1a-4a76-b0e3-40bcd3bb3e12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1042,
            "second": 1040
        },
        {
            "id": "1f3728b4-b03a-4803-ba93-7860292c1491",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1042,
            "second": 1044
        },
        {
            "id": "a8ca8e58-dd84-446a-ae37-f23c18078fdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1042,
            "second": 1046
        },
        {
            "id": "d94daaf1-dd0c-4586-bffa-47223d534297",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1042,
            "second": 1047
        },
        {
            "id": "d91c41fb-9ad5-421b-9830-dff7f67a168d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1042,
            "second": 1054
        },
        {
            "id": "7ecfaa9f-3f3e-4bb3-8e83-fdc8f1880b80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1042,
            "second": 1057
        },
        {
            "id": "06386277-9d43-4122-b0ff-a404a83c1df5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1042,
            "second": 1058
        },
        {
            "id": "44cd3923-a3af-4baa-ad13-5af4354d5778",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1042,
            "second": 1059
        },
        {
            "id": "9b084857-b8a7-41fd-9711-2f600e0d3aa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1042,
            "second": 1060
        },
        {
            "id": "238803ab-3303-4059-a19c-e93acf55fbca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1042,
            "second": 1061
        },
        {
            "id": "605dcd3c-3fa2-4696-8acb-b2835e62afc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1042,
            "second": 1063
        },
        {
            "id": "2410d0da-9393-4ae1-87a9-feca0ce92af7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1042,
            "second": 1066
        },
        {
            "id": "e280bb40-7ae0-4f32-8f0c-ae830924d07a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1042,
            "second": 1071
        },
        {
            "id": "b7cf217f-fb66-491e-a528-c17d5806d069",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1042,
            "second": 1090
        },
        {
            "id": "8c510c90-9d6d-4c06-b768-373c269b323e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1042,
            "second": 1095
        },
        {
            "id": "57186d57-2507-484e-b58a-a445c7efd503",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 1043,
            "second": 44
        },
        {
            "id": "d8cc6848-d784-427f-9233-5a5ffe99a7e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 1043,
            "second": 46
        },
        {
            "id": "0db34ce1-1f64-4e05-8908-1d30f7077579",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1043,
            "second": 171
        },
        {
            "id": "5293e2b6-416c-4865-a74a-4602aac8435a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1043,
            "second": 187
        },
        {
            "id": "3b9c8b03-5176-4802-8c1f-603979c00db9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1043,
            "second": 1040
        },
        {
            "id": "faa5c9b4-1437-4b5b-bcfb-a1f33373c959",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1043,
            "second": 1044
        },
        {
            "id": "0e13ab3e-c46f-483d-aead-475b2d2374d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 1047
        },
        {
            "id": "2c245343-0443-45d8-8f04-f80565ac022e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1043,
            "second": 1051
        },
        {
            "id": "f93aa445-d267-4233-bd14-4c5f7c1858ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 1052
        },
        {
            "id": "91690c64-1299-419e-b993-a6762441bb0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1043,
            "second": 1054
        },
        {
            "id": "a84a45cc-6a8d-41b9-b710-e6a89c944d91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 1057
        },
        {
            "id": "a469b57d-d042-4012-ae8c-983e22daa2bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 1071
        },
        {
            "id": "07a2bff2-faa6-492f-ba7e-67f75d1b3ed7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1043,
            "second": 1072
        },
        {
            "id": "f1a7bac4-a1ea-44ed-8912-8593fc82c129",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1043,
            "second": 1074
        },
        {
            "id": "b3e03e37-1b9c-4314-be95-702232d84b82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1043,
            "second": 1076
        },
        {
            "id": "f96e8f78-f69c-47c2-ba89-344d040fde33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1043,
            "second": 1077
        },
        {
            "id": "79de7972-2fca-4eee-8167-3027bd27c6ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1043,
            "second": 1080
        },
        {
            "id": "7034da2f-d902-435a-8b6c-3bff1434d8fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1043,
            "second": 1083
        },
        {
            "id": "da58624b-4093-4f64-8dc2-5027414c9d6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1043,
            "second": 1084
        },
        {
            "id": "b138560d-a026-463b-b141-f9878dfc3246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1043,
            "second": 1085
        },
        {
            "id": "4c5ff4e5-b669-4816-b6df-73af4e83b417",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1043,
            "second": 1086
        },
        {
            "id": "1e73c95e-8fba-4916-8e37-154de53a38be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1043,
            "second": 1088
        },
        {
            "id": "a7e2e7cc-b8ce-4a32-8886-e911d305990c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1043,
            "second": 1091
        },
        {
            "id": "0520e0dc-2cfc-47c4-8326-edd951981a20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1043,
            "second": 1099
        },
        {
            "id": "891cae91-77f3-48bb-9d0b-4cfddd7b7349",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1043,
            "second": 1100
        },
        {
            "id": "8e792933-d6ef-4004-ae33-dfddd199eea9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1043,
            "second": 1102
        },
        {
            "id": "4644ce1d-0691-4ab1-8b39-1a4416095d77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1043,
            "second": 1103
        },
        {
            "id": "0167b701-e9b6-4338-aee7-c2fac189bd83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1043,
            "second": 8212
        },
        {
            "id": "e3c0b6f8-c630-4c6e-b801-5f6e110fdfad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1044,
            "second": 1060
        },
        {
            "id": "03a723a0-8ffc-4593-980c-bd5b984a54b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1044,
            "second": 1063
        },
        {
            "id": "d4b89da4-8fd5-46b5-93e9-d656ab7d0011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 1044,
            "second": 1079
        },
        {
            "id": "a6c31dd3-08f8-4017-80f8-ea7cc3dae803",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 1044,
            "second": 1091
        },
        {
            "id": "811c4a7e-63a7-411b-9140-aa19bb2b464b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1045,
            "second": 1047
        },
        {
            "id": "d93134d6-3afd-4bf9-8bf1-4f8fc8b7b89e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1046,
            "second": 1054
        },
        {
            "id": "1421705b-bb94-40ca-a2bc-109acc514817",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 1046,
            "second": 1059
        },
        {
            "id": "ad1bcd2d-1840-4c36-a2c2-00b9acaa40d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 1046,
            "second": 1066
        },
        {
            "id": "92e995b3-93e6-48cb-99a2-f6ecb15b6184",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1047,
            "second": 1058
        },
        {
            "id": "95c53d03-6209-4100-8bc6-5edcbbf83591",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1047,
            "second": 1063
        },
        {
            "id": "00937e56-181d-4209-8498-0d308567b7cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1050,
            "second": 1060
        },
        {
            "id": "0be8cd32-c775-442c-8946-eefefd108373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1054,
            "second": 1040
        },
        {
            "id": "0e80a7a0-64b1-4741-92e5-c99ea9896438",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1054,
            "second": 1044
        },
        {
            "id": "ef97b0ee-dcc2-4c4f-8120-070438085924",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1054,
            "second": 1046
        },
        {
            "id": "ea49af52-af83-4099-b57a-811a2e7cf139",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1054,
            "second": 1059
        },
        {
            "id": "26b63b37-fd9b-4e60-bf68-cf7824c978c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1054,
            "second": 1061
        },
        {
            "id": "dd8c4f18-c4fc-4625-ad39-e31c748682cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1054,
            "second": 1063
        },
        {
            "id": "f8a3e948-2173-4e84-90ea-705ff4d4a756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1054,
            "second": 1071
        },
        {
            "id": "ad883322-780a-430f-8ba4-6c91702aa6a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1054,
            "second": 1076
        },
        {
            "id": "4b075026-c07a-4dc5-a5f8-3612460c3593",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 1056,
            "second": 44
        },
        {
            "id": "0a3b7893-d6a3-4f20-a2c9-15b752cfdf5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 1056,
            "second": 46
        },
        {
            "id": "43486271-d573-4a9e-b3bb-c3cee46ef105",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 58
        },
        {
            "id": "78e9282d-c19e-4078-8b85-fd850cdb0eb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 59
        },
        {
            "id": "fce34f94-3aa8-417b-b957-68cfb90c0449",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1056,
            "second": 187
        },
        {
            "id": "e83bd8ea-540f-44a4-a627-34784f7236cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 894
        },
        {
            "id": "ce399c5f-9706-4a8b-a8e2-181c23912ca7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1056,
            "second": 1040
        },
        {
            "id": "577ba1c4-ace5-4bc2-bad5-fb94ee963846",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1056,
            "second": 1044
        },
        {
            "id": "9c6d4569-92c8-452a-96e9-30cec2d76cbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 1047
        },
        {
            "id": "93d0647f-a323-4bfd-80bd-bd6c745a73be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1056,
            "second": 1051
        },
        {
            "id": "49dd0a31-863e-420b-8949-7703dd7b07ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 1054
        },
        {
            "id": "b00936a0-5474-4016-a9bf-09a673c625b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 1058
        },
        {
            "id": "40d652d4-905f-4e5c-9d02-fc374f436ccb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 1059
        },
        {
            "id": "62184920-f864-45e4-8acc-ed221f0876d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 1061
        },
        {
            "id": "c8bc7828-dd3d-482f-9f08-cd41c1575c2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 1071
        },
        {
            "id": "c3804bad-ff95-42c4-9f17-c8b09ba35559",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 1072
        },
        {
            "id": "03a38172-b489-4065-aa35-d1ffa8086b53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1056,
            "second": 1076
        },
        {
            "id": "9e17d585-2850-451e-b90a-32613165889f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 1077
        },
        {
            "id": "e852b48d-071c-4ffe-b343-790fbe9b22ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 1086
        },
        {
            "id": "40b8eb91-c89c-4e8e-adfc-4706b35d9e16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 1101
        },
        {
            "id": "57c95613-38bb-4854-8bb3-8b233e9cd241",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1056,
            "second": 1103
        },
        {
            "id": "8a4c6e17-2f89-40a8-87ae-8bf48733fd73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1057,
            "second": 1040
        },
        {
            "id": "2773d980-d16d-4d5a-9933-cefa998e0bd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1057,
            "second": 1044
        },
        {
            "id": "958a769e-e581-4147-ab20-112714df1706",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1057,
            "second": 1051
        },
        {
            "id": "ad6f94e3-4fb3-46b0-a670-69b93b5d9119",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1057,
            "second": 1054
        },
        {
            "id": "20488f54-3b23-44ff-b004-04139c87bd8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1057,
            "second": 1058
        },
        {
            "id": "64ddc79e-bbf9-4e8e-83a9-10e9fa5df3df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1057,
            "second": 1059
        },
        {
            "id": "7f058413-8199-4a72-9f93-efad340af3ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1057,
            "second": 1061
        },
        {
            "id": "58c26318-851b-4393-859a-eed3fcf33279",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1057,
            "second": 1063
        },
        {
            "id": "895fa1d9-f628-4ebd-9636-95d9365fdde8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1057,
            "second": 1066
        },
        {
            "id": "7723ee29-1512-4427-a511-5824ec8cf3ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 1057,
            "second": 1078
        },
        {
            "id": "dac93891-ce1c-4a0c-8397-458098125f66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 1058,
            "second": 44
        },
        {
            "id": "d043c4ab-b7eb-46b7-941d-ddeb16836967",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 1058,
            "second": 46
        },
        {
            "id": "ef820609-b952-4d85-b894-f44b4310dbc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 171
        },
        {
            "id": "2d11427e-bab9-4be7-8a13-a4da82f53f81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 187
        },
        {
            "id": "fcee0a03-e66b-4f50-8921-002f6c4d81b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1040
        },
        {
            "id": "bd15a9c6-abfc-4b51-b30d-906f7047b146",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1044
        },
        {
            "id": "e3341951-5d47-4774-9869-1480ab9db771",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1051
        },
        {
            "id": "e3ad1401-b3b4-456b-a173-ab6515eddda4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1054
        },
        {
            "id": "3c6f3bc5-3964-4649-bebb-fe24deb1fbe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1060
        },
        {
            "id": "f9b583b2-c1aa-4cc4-bf0e-b42163455724",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1071
        },
        {
            "id": "1b2fa4dd-ad7c-4d77-8c6b-3f2a2275b03b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1072
        },
        {
            "id": "4dd08f81-f4cc-422f-b9af-9158cba47af6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1074
        },
        {
            "id": "cb4ce72a-97a5-4efe-bd5b-d7dc0bc0fbe5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1058,
            "second": 1077
        },
        {
            "id": "81b9edef-4871-45f2-a1bb-6b7812252263",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1080
        },
        {
            "id": "1aac8377-e138-47fc-b60a-23ca37c73c35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1082
        },
        {
            "id": "684669e0-f6b7-475d-b78d-9813d279d31d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1083
        },
        {
            "id": "f162d6ae-4dc2-419e-83be-d855cd2f8f7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1084
        },
        {
            "id": "1b6edcf5-b0c3-4fa2-832a-170cdce7e0f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1058,
            "second": 1086
        },
        {
            "id": "150757d1-34c9-4f9f-a7c1-122198988955",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1087
        },
        {
            "id": "8d10ef16-4f7f-444b-b40d-8b07024d85a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1058,
            "second": 1088
        },
        {
            "id": "549d2d15-255b-4987-9b58-2de02625b85a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1058,
            "second": 1089
        },
        {
            "id": "28895379-564e-482e-a268-f62f92dc8580",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1058,
            "second": 1091
        },
        {
            "id": "90fbe052-3d3d-44d7-a6f9-1bd6f2a16f5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1093
        },
        {
            "id": "c4169cc0-3c63-494a-8971-8ad64d3c2cba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1097
        },
        {
            "id": "812bb76a-cd7d-4d67-88c3-1a64a43cab07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1099
        },
        {
            "id": "83b4f3a7-316f-4c6b-af63-3c7e231cfbc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1100
        },
        {
            "id": "b064c8e4-f81c-4f6e-a29d-cc7ffa3f8490",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 1102
        },
        {
            "id": "22b69705-c1ff-49c1-8053-38f4231212d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1058,
            "second": 1103
        },
        {
            "id": "3c90476a-8e87-4e14-971e-fdc859c24f6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1058,
            "second": 8212
        },
        {
            "id": "2e5d6364-c5af-40ab-94ab-4179f50de3eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 1059,
            "second": 44
        },
        {
            "id": "ce9014cf-1096-4126-a061-053b0c596e57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 1059,
            "second": 46
        },
        {
            "id": "96598419-8cdf-4a8e-bf67-2a1155f0a39a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 58
        },
        {
            "id": "cad76481-0368-4a85-b859-12b55e5288f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 59
        },
        {
            "id": "04ba9dde-dc80-41b5-b924-69f8f19fbe16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 171
        },
        {
            "id": "cd6e0ec6-8744-44c8-8877-5ebe94f5837a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 187
        },
        {
            "id": "a0a6318e-b991-4c14-ae9d-412855319d2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 894
        },
        {
            "id": "60c83ffd-4251-40f2-93b7-ad44075ccbe5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1059,
            "second": 1040
        },
        {
            "id": "7217a814-fff7-47a3-b7a2-1caf6993e313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1044
        },
        {
            "id": "baa27dce-5dbc-4bec-b7db-49de20cdc7e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1047
        },
        {
            "id": "8aa81689-ee64-4f97-b4d2-9d7ff187aa17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1051
        },
        {
            "id": "f547d36b-9931-4b84-977e-13a30b15079c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1054
        },
        {
            "id": "0a384e51-bb1f-4776-86e7-54ff11ba3fc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1060
        },
        {
            "id": "e8989c9d-b012-41cc-b86e-e47bc2089606",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1069
        },
        {
            "id": "f146f3c9-6786-43b3-87c8-945f80305490",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1071
        },
        {
            "id": "855f1f62-e45d-4e72-9ffb-caa9d3ef5a7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1073
        },
        {
            "id": "1867de93-b44e-43c7-be54-59821cb1baf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1059,
            "second": 1074
        },
        {
            "id": "d257e4ca-2845-4d57-87e1-2a8ea8b8880f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1075
        },
        {
            "id": "851560ab-ea3d-440a-bab1-a29264ad3c3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1059,
            "second": 1076
        },
        {
            "id": "074c15db-83d1-4ded-af7a-a2b6106c2a17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1059,
            "second": 1077
        },
        {
            "id": "a59dc216-2566-4bc6-a911-5d56cd9e4637",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1078
        },
        {
            "id": "e3c1bcd8-584e-47f9-8849-b847c5dd2207",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1059,
            "second": 1079
        },
        {
            "id": "db7793e0-c883-45cc-acaf-02f57fb99fb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1080
        },
        {
            "id": "ce3fdd9d-450f-4a17-a11e-09de3df5c2ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1081
        },
        {
            "id": "f7f36f1c-05ca-450e-a4e3-ce5bff8c7119",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1082
        },
        {
            "id": "00a3a6ae-97e5-4efd-abb3-5641e918686d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1059,
            "second": 1083
        },
        {
            "id": "b50865d1-4763-4878-a363-a15ac29c4480",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1084
        },
        {
            "id": "4176cef4-4470-4d8a-8ebc-d01acb7efcb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1085
        },
        {
            "id": "4aba7f6f-a81a-442d-b59d-6a751bd5a3b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1059,
            "second": 1086
        },
        {
            "id": "c2d84e4c-8d96-41dc-9550-e904abdbbc4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1087
        },
        {
            "id": "482b0488-9329-470b-9b15-9c1d76b16d87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1088
        },
        {
            "id": "f8a3f253-60e8-4ea7-ba28-002e1b2e0a97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1059,
            "second": 1089
        },
        {
            "id": "a8ac05d7-c611-4eb9-a0f1-e5c399311d75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1093
        },
        {
            "id": "b1076d7b-a59e-49a2-a395-da61741a894e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1094
        },
        {
            "id": "845fdb5d-b0f5-4fa6-b74b-b6cc44c293c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1096
        },
        {
            "id": "65f053f6-29b5-4651-aa86-c87d30eff68c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1097
        },
        {
            "id": "20c56ef4-d637-4f19-b1b6-5cf7969befcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1059,
            "second": 1102
        },
        {
            "id": "27d4108c-bd4d-438f-ad40-31ca39e41770",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1059,
            "second": 1103
        },
        {
            "id": "d1b116a1-88d9-4db3-a4fc-46fbff0be377",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1060,
            "second": 1040
        },
        {
            "id": "b87aaf75-17c3-4ef4-bf58-9cb404cad997",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1060,
            "second": 1044
        },
        {
            "id": "03094b2a-6203-466a-bb32-74f31b5028ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1060,
            "second": 1051
        },
        {
            "id": "cd9bfa07-1f1c-4993-8364-1895601b9039",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1060,
            "second": 1058
        },
        {
            "id": "453b8525-5c7d-4644-b4a6-71508c0cbab9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1060,
            "second": 1059
        },
        {
            "id": "e4880559-e440-4000-a5f3-3f06c126e684",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1060,
            "second": 1063
        },
        {
            "id": "72222cba-c429-4ecc-a35d-17e0a2c9ddc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1060,
            "second": 1071
        },
        {
            "id": "cb9312d1-5249-4923-9bc8-43ed2008d956",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1060,
            "second": 1083
        },
        {
            "id": "913f7bcd-4319-4851-9c56-a80ba3a928b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1061,
            "second": 1047
        },
        {
            "id": "fa9f2baa-fc02-4afb-99dc-4f2918e67bf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1061,
            "second": 1054
        },
        {
            "id": "6fb2ff87-03db-4f5c-a3b0-b16672e99924",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1061,
            "second": 1057
        },
        {
            "id": "50a8d9f6-971a-491a-99ca-6916abe5754f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1061,
            "second": 1060
        },
        {
            "id": "cbf99e63-5a8a-4c37-9715-cf008c70b103",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1061,
            "second": 1069
        },
        {
            "id": "937180d6-7775-460e-895c-9d849ac62f0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1061,
            "second": 1091
        },
        {
            "id": "4a917642-0130-4659-a7bd-1b09d73e4fe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1062,
            "second": 1054
        },
        {
            "id": "509b6a6d-561a-444c-b530-acdd0fddecf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 1062,
            "second": 1072
        },
        {
            "id": "3a0ee52f-1139-427a-b129-b935a9107126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 1065,
            "second": 1091
        },
        {
            "id": "35d20c65-e649-4ee8-9f97-a628023399e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1066,
            "second": 1071
        },
        {
            "id": "7f6f7f72-c9a6-4f0f-b2cf-aa1cba91cf8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1066,
            "second": 8217
        },
        {
            "id": "a8044bfa-3a77-4486-acd6-82825c3401b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1068,
            "second": 1040
        },
        {
            "id": "f59df027-59ae-433a-8b26-17463ce1912a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1068,
            "second": 1044
        },
        {
            "id": "fa5fffe5-d0d7-4137-8152-5f8ea9315a5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1068,
            "second": 1046
        },
        {
            "id": "1bc38315-d64f-4e1e-bd01-40f129b2aeea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1068,
            "second": 1051
        },
        {
            "id": "3d4f2d22-2fc3-4117-a08d-68e7efc49923",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1068,
            "second": 1052
        },
        {
            "id": "fc63eb85-1193-4c4a-b219-73bdf2a77377",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1068,
            "second": 1054
        },
        {
            "id": "91f6d677-2805-41c6-8194-b3b15b0e224b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1068,
            "second": 1057
        },
        {
            "id": "91ffd591-ef6d-46c6-a4d7-0a5ecf7bd49f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 1068,
            "second": 1058
        },
        {
            "id": "cf733d61-d568-4634-a836-60426be18f7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1068,
            "second": 1061
        },
        {
            "id": "737b24a7-ca0e-4ce4-b6ad-170f622cd1fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1068,
            "second": 1063
        },
        {
            "id": "8db70e47-8502-468c-9fc3-e057684682f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1068,
            "second": 1071
        },
        {
            "id": "7bb03035-6ce4-4957-b234-85a5aebb8941",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1068,
            "second": 8217
        },
        {
            "id": "34ace485-42a1-4c5c-9544-0665a253e0c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1069,
            "second": 1044
        },
        {
            "id": "32081924-e752-48e2-9217-5588811bac3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1069,
            "second": 1051
        },
        {
            "id": "83f9a427-60ad-4ce1-9505-971d4cf20cfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1069,
            "second": 1061
        },
        {
            "id": "0b3ca103-b68e-4941-970c-433b4ff8e6cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1069,
            "second": 1071
        },
        {
            "id": "669958d8-1fd3-43e0-8367-68cc754d92f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1069,
            "second": 1076
        },
        {
            "id": "4de1d885-65f2-479e-bb61-6cc2e6d63921",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1069,
            "second": 1083
        },
        {
            "id": "8a4bf278-c6b9-48bd-abc0-afb5825d55b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1070,
            "second": 1040
        },
        {
            "id": "897411a3-42a6-4dce-9fed-e86493b51f97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1070,
            "second": 1044
        },
        {
            "id": "c39e25a9-a544-471c-9a64-222f0d4f1d8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1070,
            "second": 1046
        },
        {
            "id": "ad0413dd-20d5-4a68-8f53-f8c937ce6351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1070,
            "second": 1051
        },
        {
            "id": "7f9213dd-ecd2-4cc1-a946-506cb24023d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1070,
            "second": 1058
        },
        {
            "id": "1df62211-4b79-486c-933b-9060b970fac5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1070,
            "second": 1061
        },
        {
            "id": "3d2f763f-4112-4b8c-9bc0-10dd431d248f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1070,
            "second": 1063
        },
        {
            "id": "2b43c40e-0d70-4862-a6d5-d82817ff04da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1070,
            "second": 1076
        },
        {
            "id": "31fb39ba-9c2d-4d8a-ae50-2d24e93fa1af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1070,
            "second": 1083
        },
        {
            "id": "dd54b734-6046-4eba-87cc-afbe7583c448",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1072,
            "second": 1090
        },
        {
            "id": "f94eb237-ddb9-48e5-984a-0611d91b5d12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1072,
            "second": 1095
        },
        {
            "id": "dcc9708e-f971-42ee-93e7-72b7e6768a21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1073,
            "second": 1072
        },
        {
            "id": "9216e0b9-5989-4f2c-ba1e-130d85420a0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1073,
            "second": 1076
        },
        {
            "id": "e94c3c5c-8d41-4295-a04b-3470cbec970c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1073,
            "second": 1079
        },
        {
            "id": "116e4054-42f2-4f70-9eb4-22b4e5ec976d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1073,
            "second": 1083
        },
        {
            "id": "1411b3a3-78c4-47ea-b5ce-9e2599432bfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1073,
            "second": 1084
        },
        {
            "id": "e66d3a63-8196-4bda-bfdf-17626de32bca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1073,
            "second": 1091
        },
        {
            "id": "c8f86837-68db-4086-825b-e9a987f4c0b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1073,
            "second": 1093
        },
        {
            "id": "4277dcb4-1482-4151-a749-fe4714f6cf3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1073,
            "second": 1095
        },
        {
            "id": "f420c2aa-9665-4ff8-abae-4cac13d48d49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1073,
            "second": 1098
        },
        {
            "id": "a5d27e0c-3c43-40f3-a21d-ded3fed10b34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1073,
            "second": 1103
        },
        {
            "id": "9d58a8a9-5002-4f93-9cfd-34fd87b8cd4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1074,
            "second": 1083
        },
        {
            "id": "46c80f23-ec6e-4b26-b2f4-91e44fc948b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1074,
            "second": 1090
        },
        {
            "id": "cb708eb4-e297-4618-bd08-d05af1e1bc20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1074,
            "second": 1091
        },
        {
            "id": "5472c6aa-52ff-40ee-bc86-3ce22c23ee5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1074,
            "second": 1095
        },
        {
            "id": "ae5e250a-6d5c-49cf-bae6-10a293d73377",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1074,
            "second": 1098
        },
        {
            "id": "89f2b828-6417-4a79-85fd-41f3d4f3971e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 1075,
            "second": 44
        },
        {
            "id": "443057ba-67ae-4906-aec1-fa31bb6a6015",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 1075,
            "second": 46
        },
        {
            "id": "fc91560c-48c9-41e5-8508-3e9aaa5388d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1075,
            "second": 1072
        },
        {
            "id": "dfd6a738-2496-4851-9b41-65bdff9d5b0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1075,
            "second": 1076
        },
        {
            "id": "b1b4460e-9ab0-47f0-a900-d7fc513cf418",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1075,
            "second": 1077
        },
        {
            "id": "89394a2d-f8e2-4bec-82cb-fcb72295acde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1075,
            "second": 1083
        },
        {
            "id": "c883d969-b065-4d83-b902-cca5b70fe3b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1075,
            "second": 1086
        },
        {
            "id": "a5b7f5ff-1c6e-4035-8ddc-90a2435181cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1075,
            "second": 1089
        },
        {
            "id": "d78478d8-36be-4541-883f-b62af85af255",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1076,
            "second": 1098
        },
        {
            "id": "bdedb9cf-8e16-4285-816f-514f1634cd45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1077,
            "second": 1076
        },
        {
            "id": "abd1aa69-541f-4268-819a-1f3696a9c13a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1077,
            "second": 1079
        },
        {
            "id": "5997f6db-6b20-45d7-bde4-14acf7167cf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1077,
            "second": 1083
        },
        {
            "id": "612b947b-0fa3-4536-9fc6-0925ac28c79c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1077,
            "second": 1090
        },
        {
            "id": "5a24a53f-1f86-4508-a170-99651a89c694",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1077,
            "second": 1093
        },
        {
            "id": "41263c12-b3e9-48cd-87df-7061e0e81b60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1077,
            "second": 1095
        },
        {
            "id": "c195d90d-a923-430f-b5f9-c263089eb2d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 1078,
            "second": 1098
        },
        {
            "id": "4295fbad-2960-4373-8c6b-bafc3a527683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1079,
            "second": 1076
        },
        {
            "id": "6430a5db-fbb9-4fec-bcdf-d4e11a0070b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1079,
            "second": 1095
        },
        {
            "id": "a1f393c4-dd8a-452b-8214-2ed4dd1eefac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1079,
            "second": 1098
        },
        {
            "id": "529c69c0-cce6-4dc9-bd59-f0f4ce2da4b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 1082,
            "second": 1072
        },
        {
            "id": "c047a1ee-1917-4f36-a5e0-8c649c6ad020",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 1082,
            "second": 1073
        },
        {
            "id": "4a6500c3-205a-4b2e-9a4b-02ae1e408f6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1083,
            "second": 1095
        },
        {
            "id": "a25371df-2f63-4e48-8850-c4d64468eb0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1086,
            "second": 1076
        },
        {
            "id": "3423e582-0468-4caf-a1b9-c9ea2218384a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1086,
            "second": 1083
        },
        {
            "id": "cbe94326-2486-4413-94b5-442dff1e345f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1086,
            "second": 1090
        },
        {
            "id": "bff409f3-8fd8-4ead-be1f-4bc92bf92aa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1086,
            "second": 1095
        },
        {
            "id": "8d4139d8-49c8-41fb-915b-f87c3f3fa971",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1088,
            "second": 1076
        },
        {
            "id": "1a512cd1-16b9-45fd-87c4-d039d06913c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1088,
            "second": 1083
        },
        {
            "id": "455b1cd0-149a-4ccc-8584-eaae5bafec0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1088,
            "second": 1090
        },
        {
            "id": "e587006c-2585-471c-86e2-adaec297c55c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1088,
            "second": 1095
        },
        {
            "id": "56bf3ec4-0ac1-4cd1-9da2-68d1594f167c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 1090,
            "second": 44
        },
        {
            "id": "e4c442e1-004d-428c-9ef6-0334f99fd855",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 1090,
            "second": 46
        },
        {
            "id": "d1d4eb15-b3e9-43f6-bdca-dfe63cb1e28b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1090,
            "second": 1076
        },
        {
            "id": "2bdcce5e-92e9-48de-a441-bf26f87636c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 1090,
            "second": 1078
        },
        {
            "id": "9f528ee5-a175-44cf-92f2-002a07cf973d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1090,
            "second": 1083
        },
        {
            "id": "3bfb2563-40b6-4672-bc8e-48a5b1524019",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 1091,
            "second": 44
        },
        {
            "id": "8efc6216-d894-4ab2-b802-53e6c77b15ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 1091,
            "second": 46
        },
        {
            "id": "2cf436db-80e5-479f-bf28-1845e6e9ca94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1091,
            "second": 1076
        },
        {
            "id": "aacd0dfa-f4e3-471e-8e35-0b5f7105e7bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1091,
            "second": 1083
        },
        {
            "id": "90e8fdae-d3df-4f70-b620-aa4addd27e9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1092,
            "second": 1076
        },
        {
            "id": "9a7fb2cf-92d5-48c7-a714-f698aa8187b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1092,
            "second": 1083
        },
        {
            "id": "1c278c4e-3e06-4a04-8782-41f50011785b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1092,
            "second": 1090
        },
        {
            "id": "4ffd9764-a6d6-432f-9dfe-66872c6018c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1092,
            "second": 1095
        },
        {
            "id": "0a75d342-4101-4fef-9282-fe89a76a541a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1093,
            "second": 1095
        },
        {
            "id": "c1f3af83-0b95-4676-93f4-bfb1809397e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1100,
            "second": 1090
        },
        {
            "id": "6d0c67af-3e8f-4869-8032-0b4ad9271c35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 1100,
            "second": 1095
        },
        {
            "id": "b21b5bf6-403c-4fbe-9786-70af79c76aad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1101,
            "second": 1076
        },
        {
            "id": "8c6bfef8-3154-49b8-a8fa-8f002cb7cfb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1101,
            "second": 1083
        },
        {
            "id": "de688df1-f5e7-48cc-a919-82bc4b781d26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1101,
            "second": 1090
        },
        {
            "id": "0afdf31c-e0e3-4308-b66a-d2ea874e1b72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1102,
            "second": 1076
        },
        {
            "id": "f18552a3-f2ba-4e15-8a87-85f96c5d04d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1102,
            "second": 1083
        },
        {
            "id": "a630b830-d00f-4606-8fc2-098986fd4a8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1102,
            "second": 1090
        },
        {
            "id": "f75128b8-6ecf-41bd-8285-376ab031150c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 1102,
            "second": 1095
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 1040,
            "y": 1106
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}