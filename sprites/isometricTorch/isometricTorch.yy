{
    "id": "6a6f2ef3-7662-450a-8c3f-6d15746d6bbb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "isometricTorch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 89,
    "bbox_left": 11,
    "bbox_right": 52,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "00dc5593-f991-4f6b-af15-20f07bd5b769",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a6f2ef3-7662-450a-8c3f-6d15746d6bbb",
            "compositeImage": {
                "id": "cd605635-0beb-4449-8025-09279104d2b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00dc5593-f991-4f6b-af15-20f07bd5b769",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e23b17fb-52be-4bfc-84f7-b0864ae64cd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00dc5593-f991-4f6b-af15-20f07bd5b769",
                    "LayerId": "a7be1915-5b58-4941-b6b0-854326c82e3d"
                },
                {
                    "id": "b66dc16d-0c0b-46fe-a116-013477ea329e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00dc5593-f991-4f6b-af15-20f07bd5b769",
                    "LayerId": "d46ada27-905c-4be8-b0ea-eece6518535e"
                },
                {
                    "id": "8519fbe8-1127-4b26-bc96-a01beb9ea2eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00dc5593-f991-4f6b-af15-20f07bd5b769",
                    "LayerId": "eac846b2-0eea-4429-8299-4d12983ae9a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "eac846b2-0eea-4429-8299-4d12983ae9a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a6f2ef3-7662-450a-8c3f-6d15746d6bbb",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "d46ada27-905c-4be8-b0ea-eece6518535e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a6f2ef3-7662-450a-8c3f-6d15746d6bbb",
            "blendMode": 0,
            "isLocked": true,
            "name": "Layer 2 (2)",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "a7be1915-5b58-4941-b6b0-854326c82e3d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a6f2ef3-7662-450a-8c3f-6d15746d6bbb",
            "blendMode": 0,
            "isLocked": true,
            "name": "default",
            "opacity": 48,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 64
}