{
    "id": "9ab9f070-92ab-4335-8920-d7f2c30f64bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "isometricTile1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d358af3c-40ab-425d-951c-36a3ed411eed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ab9f070-92ab-4335-8920-d7f2c30f64bb",
            "compositeImage": {
                "id": "8caa8c1e-5a50-4f3a-a0c2-722d1c2ecca9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d358af3c-40ab-425d-951c-36a3ed411eed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "406bae76-8f00-4be9-96b5-d7aeff35c526",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d358af3c-40ab-425d-951c-36a3ed411eed",
                    "LayerId": "0fbbce85-0fac-42fd-87a2-eee3bfc963c9"
                },
                {
                    "id": "5315c681-c67c-4f7c-812b-5ff25808c918",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d358af3c-40ab-425d-951c-36a3ed411eed",
                    "LayerId": "3fb32ad4-8d8c-4faf-9d41-b28bc19014b7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "3fb32ad4-8d8c-4faf-9d41-b28bc19014b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9ab9f070-92ab-4335-8920-d7f2c30f64bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "0fbbce85-0fac-42fd-87a2-eee3bfc963c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9ab9f070-92ab-4335-8920-d7f2c30f64bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}