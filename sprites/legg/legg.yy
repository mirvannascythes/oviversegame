{
    "id": "bdb3ccdc-be3f-4f37-a7b7-9295cf13b0ee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "legg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a3053f2-0dbb-4812-9687-2df7c9581c59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdb3ccdc-be3f-4f37-a7b7-9295cf13b0ee",
            "compositeImage": {
                "id": "0fae0e35-abd3-46a1-a7df-6680f2d0983d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a3053f2-0dbb-4812-9687-2df7c9581c59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7211d23e-2e6f-4566-b101-c9ba25ed335d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a3053f2-0dbb-4812-9687-2df7c9581c59",
                    "LayerId": "ac31ffbf-4849-49fa-be60-e0d97f3ead61"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ac31ffbf-4849-49fa-be60-e0d97f3ead61",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bdb3ccdc-be3f-4f37-a7b7-9295cf13b0ee",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1 (2)",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 0
}