{
    "id": "f3ea0d68-039f-4b14-b977-6521a0fcf443",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "valentineIcon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 15,
    "bbox_right": 83,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "16a5da3d-e8ee-45ac-9972-e09eca2afdb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3ea0d68-039f-4b14-b977-6521a0fcf443",
            "compositeImage": {
                "id": "d88de3ca-818e-412b-b4ef-f0c440a678b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16a5da3d-e8ee-45ac-9972-e09eca2afdb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be4061ad-d4a7-49e8-86af-cf19c1e13470",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16a5da3d-e8ee-45ac-9972-e09eca2afdb1",
                    "LayerId": "7b0555ef-8662-4b07-862e-0d466e3536f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "7b0555ef-8662-4b07-862e-0d466e3536f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3ea0d68-039f-4b14-b977-6521a0fcf443",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}