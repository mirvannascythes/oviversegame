{
    "id": "9c358483-31ca-460e-b5d7-bd9d309d6e6b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bitchTile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "af7d7bbc-e818-42ed-9499-be9611cdb0a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c358483-31ca-460e-b5d7-bd9d309d6e6b",
            "compositeImage": {
                "id": "b64dd5ee-0024-49af-b891-985f078a893f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af7d7bbc-e818-42ed-9499-be9611cdb0a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60960073-e0f5-45b9-b864-62575d869fce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af7d7bbc-e818-42ed-9499-be9611cdb0a5",
                    "LayerId": "d477d55c-c57f-4551-b189-23b45830d1ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d477d55c-c57f-4551-b189-23b45830d1ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c358483-31ca-460e-b5d7-bd9d309d6e6b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}