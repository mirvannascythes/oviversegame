{
    "id": "57f2007d-f4e2-4031-a66a-ece086fb2c16",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "menuButtonBar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e452ef7-100c-4144-a6e4-a29eb083ecce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57f2007d-f4e2-4031-a66a-ece086fb2c16",
            "compositeImage": {
                "id": "300500ec-afc2-4a95-b195-f90130737994",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e452ef7-100c-4144-a6e4-a29eb083ecce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08306a88-5310-4e5f-813c-fe51119be604",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e452ef7-100c-4144-a6e4-a29eb083ecce",
                    "LayerId": "e9d501c7-4937-405b-8efc-703bd7f7c923"
                },
                {
                    "id": "6f2e40dd-b4de-4b31-bb2a-a16cec84ccdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e452ef7-100c-4144-a6e4-a29eb083ecce",
                    "LayerId": "c430435a-4de1-4346-be81-b1c73e90f2ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c430435a-4de1-4346-be81-b1c73e90f2ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57f2007d-f4e2-4031-a66a-ece086fb2c16",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "e9d501c7-4937-405b-8efc-703bd7f7c923",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57f2007d-f4e2-4031-a66a-ece086fb2c16",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}