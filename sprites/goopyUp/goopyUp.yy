{
    "id": "49787eae-1de6-4cba-aeac-b14c9d308fdd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "goopyUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 319,
    "bbox_left": 0,
    "bbox_right": 176,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dfcf9a01-9ed1-4f9e-ae74-c8fcd8e5e365",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49787eae-1de6-4cba-aeac-b14c9d308fdd",
            "compositeImage": {
                "id": "a9ee4844-83b6-417a-af8f-f97892223fde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfcf9a01-9ed1-4f9e-ae74-c8fcd8e5e365",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26df6fee-55a3-45e2-9a05-b822a819877a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfcf9a01-9ed1-4f9e-ae74-c8fcd8e5e365",
                    "LayerId": "140d9dc7-9fe9-4fc3-a195-30ddb1ad7df9"
                }
            ]
        },
        {
            "id": "8d37310d-1932-4853-9794-bb924b6c7938",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49787eae-1de6-4cba-aeac-b14c9d308fdd",
            "compositeImage": {
                "id": "23b35cc1-f1f2-4979-9589-0271fdeacb91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d37310d-1932-4853-9794-bb924b6c7938",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d1ff75e-de67-412a-ab8e-5247bd0c1dd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d37310d-1932-4853-9794-bb924b6c7938",
                    "LayerId": "140d9dc7-9fe9-4fc3-a195-30ddb1ad7df9"
                }
            ]
        },
        {
            "id": "5a382ba7-38b9-4076-8b18-05f77f7d456b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49787eae-1de6-4cba-aeac-b14c9d308fdd",
            "compositeImage": {
                "id": "fffe77d7-ef6d-47a8-8714-531d740477f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a382ba7-38b9-4076-8b18-05f77f7d456b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e1479d9-fa84-484b-9b88-cff0c45ee4f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a382ba7-38b9-4076-8b18-05f77f7d456b",
                    "LayerId": "140d9dc7-9fe9-4fc3-a195-30ddb1ad7df9"
                }
            ]
        },
        {
            "id": "9f150ffb-c9ce-4745-b354-6cf8011269a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49787eae-1de6-4cba-aeac-b14c9d308fdd",
            "compositeImage": {
                "id": "41838595-cc0d-4d98-acfe-1a386351fc82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f150ffb-c9ce-4745-b354-6cf8011269a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03090f3d-292e-4bce-bd33-43407c5ac15b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f150ffb-c9ce-4745-b354-6cf8011269a4",
                    "LayerId": "140d9dc7-9fe9-4fc3-a195-30ddb1ad7df9"
                }
            ]
        },
        {
            "id": "8a1dee84-1515-4cb4-a7d2-b92821f8daeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49787eae-1de6-4cba-aeac-b14c9d308fdd",
            "compositeImage": {
                "id": "9373ba48-78f6-4f8a-af8a-63af0ec052ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a1dee84-1515-4cb4-a7d2-b92821f8daeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6cb2e67-6bfc-4824-bd44-551030bc5861",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a1dee84-1515-4cb4-a7d2-b92821f8daeb",
                    "LayerId": "140d9dc7-9fe9-4fc3-a195-30ddb1ad7df9"
                }
            ]
        },
        {
            "id": "013142fd-50df-43d3-89ca-9464173be2f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49787eae-1de6-4cba-aeac-b14c9d308fdd",
            "compositeImage": {
                "id": "c460d47f-de58-44f6-b1c5-576a2d2078ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "013142fd-50df-43d3-89ca-9464173be2f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62eaf419-0260-49ff-8f46-667301b729ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "013142fd-50df-43d3-89ca-9464173be2f3",
                    "LayerId": "140d9dc7-9fe9-4fc3-a195-30ddb1ad7df9"
                }
            ]
        },
        {
            "id": "16dafba2-100a-45f9-a6f7-b088cd006f3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49787eae-1de6-4cba-aeac-b14c9d308fdd",
            "compositeImage": {
                "id": "bf33c1a7-a8fb-46ef-bcf9-ac9a1e9cd12a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16dafba2-100a-45f9-a6f7-b088cd006f3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6da88a9d-8143-47fe-a0d1-57bdedebacfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16dafba2-100a-45f9-a6f7-b088cd006f3e",
                    "LayerId": "140d9dc7-9fe9-4fc3-a195-30ddb1ad7df9"
                }
            ]
        },
        {
            "id": "379abd20-4871-4a20-b781-f3b72aaa244f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "49787eae-1de6-4cba-aeac-b14c9d308fdd",
            "compositeImage": {
                "id": "263059d6-e050-4abc-900c-8aa24978d878",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "379abd20-4871-4a20-b781-f3b72aaa244f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "576416a9-d902-4fb2-a661-e00868d3ab05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "379abd20-4871-4a20-b781-f3b72aaa244f",
                    "LayerId": "140d9dc7-9fe9-4fc3-a195-30ddb1ad7df9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 320,
    "layers": [
        {
            "id": "140d9dc7-9fe9-4fc3-a195-30ddb1ad7df9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "49787eae-1de6-4cba-aeac-b14c9d308fdd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 184,
    "xorig": 92,
    "yorig": 319
}