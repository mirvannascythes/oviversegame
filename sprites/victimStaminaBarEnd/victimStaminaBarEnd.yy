{
    "id": "262d05c5-cdcc-4edf-ac4c-eeacc177b15a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "victimStaminaBarEnd",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 16,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d74a004-a7c6-4412-bf6f-b55f4b8971b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "262d05c5-cdcc-4edf-ac4c-eeacc177b15a",
            "compositeImage": {
                "id": "faf3b713-28c3-496e-ae6f-56fc6706fc06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d74a004-a7c6-4412-bf6f-b55f4b8971b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23965daf-3a0b-4960-83fd-a897777a30fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d74a004-a7c6-4412-bf6f-b55f4b8971b7",
                    "LayerId": "19fde645-b66f-4142-89b1-70c69d8fb48a"
                },
                {
                    "id": "6b0412e2-ec29-4969-9018-b9146789ba3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d74a004-a7c6-4412-bf6f-b55f4b8971b7",
                    "LayerId": "1f247970-0f4c-4070-893e-897b41c5ab25"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1f247970-0f4c-4070-893e-897b41c5ab25",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "262d05c5-cdcc-4edf-ac4c-eeacc177b15a",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "19fde645-b66f-4142-89b1-70c69d8fb48a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "262d05c5-cdcc-4edf-ac4c-eeacc177b15a",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1 (2)",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}