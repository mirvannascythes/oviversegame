{
    "id": "a85309dc-069b-4970-b731-03dc0e864969",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "shadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b81cbe40-26f9-42a1-9dd4-2be0aea2b4a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a85309dc-069b-4970-b731-03dc0e864969",
            "compositeImage": {
                "id": "d2a47498-4930-4451-ad6f-3e3a1a4d60b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b81cbe40-26f9-42a1-9dd4-2be0aea2b4a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9bbd6ad-4919-41fe-a34d-03bea8335b6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b81cbe40-26f9-42a1-9dd4-2be0aea2b4a3",
                    "LayerId": "16008d16-d2ed-40e2-a395-2ce3c304f6a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "16008d16-d2ed-40e2-a395-2ce3c304f6a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a85309dc-069b-4970-b731-03dc0e864969",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 55,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 16
}