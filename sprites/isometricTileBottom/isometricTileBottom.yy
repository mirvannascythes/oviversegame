{
    "id": "45a5fe03-3f23-4e59-93e0-06cb0c2d22d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "isometricTileBottom",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e116569c-a2f8-4807-b441-4e0aeee80c4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45a5fe03-3f23-4e59-93e0-06cb0c2d22d1",
            "compositeImage": {
                "id": "822ede3d-fb2e-4ed5-9370-e8d78fcd4dae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e116569c-a2f8-4807-b441-4e0aeee80c4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6eb0636-9200-43e2-a30e-80afde88b1a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e116569c-a2f8-4807-b441-4e0aeee80c4a",
                    "LayerId": "dac16439-4d25-46d8-8201-13d68bff81ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "dac16439-4d25-46d8-8201-13d68bff81ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "45a5fe03-3f23-4e59-93e0-06cb0c2d22d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}