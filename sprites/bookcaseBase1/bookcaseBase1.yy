{
    "id": "5e729dce-f7d8-49cc-93c1-10312510587b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bookcaseBase1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 16,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "79f73bea-3059-4cda-a95e-ede308c8ca61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e729dce-f7d8-49cc-93c1-10312510587b",
            "compositeImage": {
                "id": "cf44770a-f26d-4dcf-83a6-d69cd2b1cbfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79f73bea-3059-4cda-a95e-ede308c8ca61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f035d2f6-d555-486e-bd6d-ffd23ef5c0ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79f73bea-3059-4cda-a95e-ede308c8ca61",
                    "LayerId": "2c98ffbc-ebab-44f2-892b-670be3c4cd6a"
                },
                {
                    "id": "cea6e894-0799-437a-b22b-e10d952fa650",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79f73bea-3059-4cda-a95e-ede308c8ca61",
                    "LayerId": "15d36c9a-fe89-4030-a5bf-aead18582f80"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "2c98ffbc-ebab-44f2-892b-670be3c4cd6a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e729dce-f7d8-49cc-93c1-10312510587b",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "15d36c9a-fe89-4030-a5bf-aead18582f80",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e729dce-f7d8-49cc-93c1-10312510587b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}