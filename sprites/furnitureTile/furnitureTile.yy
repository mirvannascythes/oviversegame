{
    "id": "4c349405-0668-4e1d-b340-fb437e83aca4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "furnitureTile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "70f729c7-a2cb-4ee5-8a4b-9bb77479ea06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c349405-0668-4e1d-b340-fb437e83aca4",
            "compositeImage": {
                "id": "bc8b699e-28ca-4919-b916-92de0625e826",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70f729c7-a2cb-4ee5-8a4b-9bb77479ea06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6516a866-13e1-4582-a730-f079f44fe0ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70f729c7-a2cb-4ee5-8a4b-9bb77479ea06",
                    "LayerId": "700b2ae1-d6f9-461e-8085-cace8f0853cd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "700b2ae1-d6f9-461e-8085-cace8f0853cd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c349405-0668-4e1d-b340-fb437e83aca4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}