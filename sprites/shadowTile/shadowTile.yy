{
    "id": "db70222f-b3a7-4c7d-a94e-b8588a75c742",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "shadowTile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93be4e52-547f-4807-a9d8-ad47736177be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db70222f-b3a7-4c7d-a94e-b8588a75c742",
            "compositeImage": {
                "id": "f0d8503a-93da-43b8-b716-d35f4ae71d0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93be4e52-547f-4807-a9d8-ad47736177be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "306ab816-e8f9-4527-b4c7-0b6964286b85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93be4e52-547f-4807-a9d8-ad47736177be",
                    "LayerId": "c6d0f28b-6777-4e8a-9057-0c15a301cd8e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c6d0f28b-6777-4e8a-9057-0c15a301cd8e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db70222f-b3a7-4c7d-a94e-b8588a75c742",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}