{
    "id": "5db9f756-a745-4e4a-ad95-0291af4afe94",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "staminaIcon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b1d57fa1-b6ce-4df0-b7ab-2187ec4bcbc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5db9f756-a745-4e4a-ad95-0291af4afe94",
            "compositeImage": {
                "id": "5c4b53f9-2847-4ebb-955b-bec69ea56cd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1d57fa1-b6ce-4df0-b7ab-2187ec4bcbc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ba8b4db-102e-497c-8602-75dd6ca495c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1d57fa1-b6ce-4df0-b7ab-2187ec4bcbc8",
                    "LayerId": "ca570aae-211c-479c-ac90-1169cee4115d"
                },
                {
                    "id": "06e5033d-6ef8-4062-b785-948edf77c65e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1d57fa1-b6ce-4df0-b7ab-2187ec4bcbc8",
                    "LayerId": "0776ccb5-7d08-48a8-adf9-70f3351f0059"
                },
                {
                    "id": "daec2509-0c9e-4276-987b-8f7faaa95e26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1d57fa1-b6ce-4df0-b7ab-2187ec4bcbc8",
                    "LayerId": "ad1669a4-b532-428a-94dc-80f817e8c81a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ad1669a4-b532-428a-94dc-80f817e8c81a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5db9f756-a745-4e4a-ad95-0291af4afe94",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "0776ccb5-7d08-48a8-adf9-70f3351f0059",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5db9f756-a745-4e4a-ad95-0291af4afe94",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "ca570aae-211c-479c-ac90-1169cee4115d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5db9f756-a745-4e4a-ad95-0291af4afe94",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}