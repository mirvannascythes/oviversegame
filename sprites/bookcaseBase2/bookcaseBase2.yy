{
    "id": "95244c64-9f5c-45bb-932b-0d788380db61",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bookcaseBase2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 80,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a40007e9-bac3-494f-ad19-6f035871f27a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95244c64-9f5c-45bb-932b-0d788380db61",
            "compositeImage": {
                "id": "b967913d-2516-48a0-9e2d-0d7fe799fa38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a40007e9-bac3-494f-ad19-6f035871f27a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d45ca329-d5ce-4e84-addf-65f6d4ca6664",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a40007e9-bac3-494f-ad19-6f035871f27a",
                    "LayerId": "22400bb2-92cf-4899-a489-99ff678e0c4c"
                },
                {
                    "id": "904bd320-e341-43bf-b287-792fe8fc25d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a40007e9-bac3-494f-ad19-6f035871f27a",
                    "LayerId": "5128c436-3161-4243-832e-e77c7e60ad85"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "22400bb2-92cf-4899-a489-99ff678e0c4c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95244c64-9f5c-45bb-932b-0d788380db61",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "5128c436-3161-4243-832e-e77c7e60ad85",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95244c64-9f5c-45bb-932b-0d788380db61",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 80
}