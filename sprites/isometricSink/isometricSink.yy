{
    "id": "3d624ef0-7d1d-46c7-ad26-65dfd59537f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "isometricSink",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d09ebaf9-70e7-4359-a070-4aa20bc6ae5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d624ef0-7d1d-46c7-ad26-65dfd59537f7",
            "compositeImage": {
                "id": "50f6df00-60e2-4652-985d-f4a523dc60b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d09ebaf9-70e7-4359-a070-4aa20bc6ae5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "011ffbd4-d848-40aa-9807-b9616eb8a412",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d09ebaf9-70e7-4359-a070-4aa20bc6ae5d",
                    "LayerId": "69512b3f-2482-4035-becc-1750a72f85f5"
                },
                {
                    "id": "b1a0b722-836a-4dc4-bcf3-876e346cb078",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d09ebaf9-70e7-4359-a070-4aa20bc6ae5d",
                    "LayerId": "01bf95ab-d0da-47be-81b4-b4b9f7f8cb95"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "01bf95ab-d0da-47be-81b4-b4b9f7f8cb95",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3d624ef0-7d1d-46c7-ad26-65dfd59537f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "69512b3f-2482-4035-becc-1750a72f85f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3d624ef0-7d1d-46c7-ad26-65dfd59537f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1 (2)",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 32
}