{
    "id": "bf78ebee-b541-476e-ac87-7f071d3e965a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "isometricWall111",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 163,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a79944f-2f94-4c7c-8cd8-860bbbca6001",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf78ebee-b541-476e-ac87-7f071d3e965a",
            "compositeImage": {
                "id": "e63cad3c-9028-4f87-a263-7c97bbf7136f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a79944f-2f94-4c7c-8cd8-860bbbca6001",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1134f95-c677-4d8d-823e-09a03d991c06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a79944f-2f94-4c7c-8cd8-860bbbca6001",
                    "LayerId": "eeee61f8-e0ce-42a7-b955-aea8d2f68fc7"
                },
                {
                    "id": "e03d886f-4174-4bb9-9e85-6abc7b01ec20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a79944f-2f94-4c7c-8cd8-860bbbca6001",
                    "LayerId": "bc2b86cf-e203-41b0-b4a1-47d067b2bd39"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 164,
    "layers": [
        {
            "id": "eeee61f8-e0ce-42a7-b955-aea8d2f68fc7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf78ebee-b541-476e-ac87-7f071d3e965a",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "bc2b86cf-e203-41b0-b4a1-47d067b2bd39",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf78ebee-b541-476e-ac87-7f071d3e965a",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1 (2)",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 132
}