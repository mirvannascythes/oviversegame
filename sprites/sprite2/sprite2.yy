{
    "id": "649a1fc0-91f4-4e11-9612-d6d369f0c8e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "604e2b54-302b-4c72-ba37-2541ebb35de3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "649a1fc0-91f4-4e11-9612-d6d369f0c8e7",
            "compositeImage": {
                "id": "c91ee33b-fd11-422e-af0a-9469009cebcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "604e2b54-302b-4c72-ba37-2541ebb35de3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5349a194-79be-40be-8305-b575bc7ef05f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "604e2b54-302b-4c72-ba37-2541ebb35de3",
                    "LayerId": "90129991-708e-4fa1-b46a-8020ea68e713"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "90129991-708e-4fa1-b46a-8020ea68e713",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "649a1fc0-91f4-4e11-9612-d6d369f0c8e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}