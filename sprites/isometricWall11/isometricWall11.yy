{
    "id": "90ca0039-a23b-41af-9c91-8dcc6d7dbf8e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "isometricWall11",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6350d5e-4923-4231-bd1f-1f4abdcda6c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90ca0039-a23b-41af-9c91-8dcc6d7dbf8e",
            "compositeImage": {
                "id": "a33d3a31-b2ff-47e5-b58d-16897a9873b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6350d5e-4923-4231-bd1f-1f4abdcda6c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eba102b4-1df8-4b53-8658-b43f69e394cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6350d5e-4923-4231-bd1f-1f4abdcda6c6",
                    "LayerId": "d7589d48-19a3-4b60-b1df-17ba17193f1d"
                },
                {
                    "id": "553f9bbe-250c-420a-82ed-025dc80c1a4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6350d5e-4923-4231-bd1f-1f4abdcda6c6",
                    "LayerId": "9c8e0a4c-a0cc-4e81-a75b-8a7e3bf2f55c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9c8e0a4c-a0cc-4e81-a75b-8a7e3bf2f55c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90ca0039-a23b-41af-9c91-8dcc6d7dbf8e",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "d7589d48-19a3-4b60-b1df-17ba17193f1d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90ca0039-a23b-41af-9c91-8dcc6d7dbf8e",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1 (2)",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 32
}