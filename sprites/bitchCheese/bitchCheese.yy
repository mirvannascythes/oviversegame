{
    "id": "2651ea56-47d3-40f0-af7d-ac0d4e6503ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bitchCheese",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a47e0db3-8221-42e6-90de-f7ed5cc8959a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2651ea56-47d3-40f0-af7d-ac0d4e6503ad",
            "compositeImage": {
                "id": "fde79e38-e671-4a64-adfc-b14d26248e82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a47e0db3-8221-42e6-90de-f7ed5cc8959a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85b0a9b9-6866-43a6-91c5-c5fd0f4248ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a47e0db3-8221-42e6-90de-f7ed5cc8959a",
                    "LayerId": "5806f8cf-686e-4e99-928f-bd197367ea63"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5806f8cf-686e-4e99-928f-bd197367ea63",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2651ea56-47d3-40f0-af7d-ac0d4e6503ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}