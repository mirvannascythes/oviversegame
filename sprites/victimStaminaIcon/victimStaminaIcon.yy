{
    "id": "e6ec0347-35e7-47da-9eba-ef1e58acf0de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "victimStaminaIcon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc49c1c4-3efe-474b-86f4-c199880c1089",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6ec0347-35e7-47da-9eba-ef1e58acf0de",
            "compositeImage": {
                "id": "6f54e959-6e18-4ff2-bd6b-b66ace4d10e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc49c1c4-3efe-474b-86f4-c199880c1089",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56fb4047-0413-4c63-858c-8ccd25cab59e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc49c1c4-3efe-474b-86f4-c199880c1089",
                    "LayerId": "e8533a3c-9b9b-4512-8c90-821e84e166e4"
                },
                {
                    "id": "0e2eb1bd-67bf-4146-86fc-103c1e8f6220",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc49c1c4-3efe-474b-86f4-c199880c1089",
                    "LayerId": "9a853baf-71a6-4a21-9eca-3852c026b4b2"
                },
                {
                    "id": "4fae0c4e-af08-4ff1-ae6b-7e686c594ee7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc49c1c4-3efe-474b-86f4-c199880c1089",
                    "LayerId": "b001dc76-990f-46a7-ab95-e73be1048583"
                },
                {
                    "id": "de623117-9607-4cf6-9c97-63085cc66f30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc49c1c4-3efe-474b-86f4-c199880c1089",
                    "LayerId": "da3b9040-56df-4a89-a9b9-b978d07e2f3a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "da3b9040-56df-4a89-a9b9-b978d07e2f3a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6ec0347-35e7-47da-9eba-ef1e58acf0de",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "e8533a3c-9b9b-4512-8c90-821e84e166e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6ec0347-35e7-47da-9eba-ef1e58acf0de",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "9a853baf-71a6-4a21-9eca-3852c026b4b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6ec0347-35e7-47da-9eba-ef1e58acf0de",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "b001dc76-990f-46a7-ab95-e73be1048583",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6ec0347-35e7-47da-9eba-ef1e58acf0de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}