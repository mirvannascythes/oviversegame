{
    "id": "ff5442f9-a629-4922-a3d8-38898996a6e2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tileStoneSlab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3ab5b65e-0745-4560-b2ae-b7ed06b59368",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff5442f9-a629-4922-a3d8-38898996a6e2",
            "compositeImage": {
                "id": "c5351989-1d01-4b44-8f10-20bf06b6f883",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ab5b65e-0745-4560-b2ae-b7ed06b59368",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6f1b30d-0c40-4df4-aaa3-a434af60b342",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ab5b65e-0745-4560-b2ae-b7ed06b59368",
                    "LayerId": "091713aa-e094-43d7-93a6-b6306d649a74"
                },
                {
                    "id": "fdb91761-111c-4712-8975-d17d951de987",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ab5b65e-0745-4560-b2ae-b7ed06b59368",
                    "LayerId": "978f01fb-27a1-4132-be83-5ab8090333c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "978f01fb-27a1-4132-be83-5ab8090333c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff5442f9-a629-4922-a3d8-38898996a6e2",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "091713aa-e094-43d7-93a6-b6306d649a74",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff5442f9-a629-4922-a3d8-38898996a6e2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}