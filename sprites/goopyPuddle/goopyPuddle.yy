{
    "id": "dd98bd7a-a227-45bc-a435-ab15903e57bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "goopyPuddle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 319,
    "bbox_left": 16,
    "bbox_right": 228,
    "bbox_top": 59,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8038b3e-bb17-4991-9eeb-e75a15707cc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd98bd7a-a227-45bc-a435-ab15903e57bd",
            "compositeImage": {
                "id": "e671bbf7-a079-448c-a537-c9e3397bd1c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8038b3e-bb17-4991-9eeb-e75a15707cc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c704798d-2622-4550-b884-e97cafa0114f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8038b3e-bb17-4991-9eeb-e75a15707cc0",
                    "LayerId": "d32f42f6-1834-49da-ac79-4fcf749ff903"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 320,
    "layers": [
        {
            "id": "d32f42f6-1834-49da-ac79-4fcf749ff903",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd98bd7a-a227-45bc-a435-ab15903e57bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 238,
    "xorig": 119,
    "yorig": 319
}