{
    "id": "7b6644cb-07a5-4625-b323-0148c137fa2a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gasp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 14,
    "bbox_right": 17,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc0681b4-a8d5-47a9-ad8c-6e41b8ab556d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b6644cb-07a5-4625-b323-0148c137fa2a",
            "compositeImage": {
                "id": "578becbf-054b-42e0-98ee-bb00a1c40c61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc0681b4-a8d5-47a9-ad8c-6e41b8ab556d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d41eeb6-3c8a-48ef-aea4-e6ba9d5906b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc0681b4-a8d5-47a9-ad8c-6e41b8ab556d",
                    "LayerId": "5d95a643-98d8-4ffe-a180-15b34d5cdda9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5d95a643-98d8-4ffe-a180-15b34d5cdda9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b6644cb-07a5-4625-b323-0148c137fa2a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}