{
    "id": "c701599f-a1b5-4431-83c1-43b039a6ac02",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tileStoneSmallSlab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "246a4c5b-a8be-4710-b403-44b1dc96e61a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c701599f-a1b5-4431-83c1-43b039a6ac02",
            "compositeImage": {
                "id": "28239dc3-8aaa-4849-9ef0-d52b6ead3e04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "246a4c5b-a8be-4710-b403-44b1dc96e61a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3eef47f0-573c-45f4-ac02-7777d43f8a73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "246a4c5b-a8be-4710-b403-44b1dc96e61a",
                    "LayerId": "55ba6265-e4ec-4c08-b93d-65440cd57267"
                },
                {
                    "id": "95a44158-6688-4f8c-98cf-e29691f48fe1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "246a4c5b-a8be-4710-b403-44b1dc96e61a",
                    "LayerId": "fa22e8c7-1bb7-4355-992d-b8dc3001d5b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "55ba6265-e4ec-4c08-b93d-65440cd57267",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c701599f-a1b5-4431-83c1-43b039a6ac02",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "fa22e8c7-1bb7-4355-992d-b8dc3001d5b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c701599f-a1b5-4431-83c1-43b039a6ac02",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}