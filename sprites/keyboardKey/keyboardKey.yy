{
    "id": "e3fe0ce0-51dd-4a73-9e7a-00b9748e1f54",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "keyboardKey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a0c6826e-4cfb-4921-8856-378d8a8fd43b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3fe0ce0-51dd-4a73-9e7a-00b9748e1f54",
            "compositeImage": {
                "id": "77753489-613c-4536-8381-56c0db62bdf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0c6826e-4cfb-4921-8856-378d8a8fd43b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5223d01-000a-461d-adc1-2cfc2465dad1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0c6826e-4cfb-4921-8856-378d8a8fd43b",
                    "LayerId": "e8837d93-159e-4d28-94d3-728d65e25c42"
                },
                {
                    "id": "7aa56beb-10c2-4cce-958a-7ed82a774931",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0c6826e-4cfb-4921-8856-378d8a8fd43b",
                    "LayerId": "9412a086-2a8c-42b1-b569-ff4f6b3809db"
                },
                {
                    "id": "ff593526-764e-491f-9be2-9b1124bfc651",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0c6826e-4cfb-4921-8856-378d8a8fd43b",
                    "LayerId": "d60c2bad-ccc1-4044-a412-7e8594163282"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d60c2bad-ccc1-4044-a412-7e8594163282",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3fe0ce0-51dd-4a73-9e7a-00b9748e1f54",
            "blendMode": 3,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 46,
            "visible": true
        },
        {
            "id": "e8837d93-159e-4d28-94d3-728d65e25c42",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3fe0ce0-51dd-4a73-9e7a-00b9748e1f54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "9412a086-2a8c-42b1-b569-ff4f6b3809db",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3fe0ce0-51dd-4a73-9e7a-00b9748e1f54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default (2)",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}