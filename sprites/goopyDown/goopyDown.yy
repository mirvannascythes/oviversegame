{
    "id": "d2ef9bd8-6504-456e-8d25-93e0c14981c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "goopyDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 319,
    "bbox_left": 12,
    "bbox_right": 171,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf9eb7da-dda9-4a5e-b227-8fd5068560e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ef9bd8-6504-456e-8d25-93e0c14981c4",
            "compositeImage": {
                "id": "6d27a72b-a70b-4ea8-981c-2c127a9bfc3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf9eb7da-dda9-4a5e-b227-8fd5068560e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2dbbac6-be7f-4084-954f-9a306f987394",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf9eb7da-dda9-4a5e-b227-8fd5068560e0",
                    "LayerId": "8c107300-0910-4d69-ac59-2311f261ebe0"
                }
            ]
        },
        {
            "id": "9647cc30-4283-4339-8468-b8f32372b1fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ef9bd8-6504-456e-8d25-93e0c14981c4",
            "compositeImage": {
                "id": "c931209b-d47d-4a75-ab78-a20ad33d1010",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9647cc30-4283-4339-8468-b8f32372b1fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c4bc7af-4f91-40f4-b2cf-8cc85e8d994a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9647cc30-4283-4339-8468-b8f32372b1fa",
                    "LayerId": "8c107300-0910-4d69-ac59-2311f261ebe0"
                }
            ]
        },
        {
            "id": "95bb8324-da07-44a9-9f1b-13d7c317ad94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ef9bd8-6504-456e-8d25-93e0c14981c4",
            "compositeImage": {
                "id": "d4ed7b67-0dd6-4653-8232-344536a33255",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95bb8324-da07-44a9-9f1b-13d7c317ad94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f80e7873-8cd5-42a1-b78f-e7e5c445d6ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95bb8324-da07-44a9-9f1b-13d7c317ad94",
                    "LayerId": "8c107300-0910-4d69-ac59-2311f261ebe0"
                }
            ]
        },
        {
            "id": "3017ece2-37d5-4e78-a652-10057eb856b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ef9bd8-6504-456e-8d25-93e0c14981c4",
            "compositeImage": {
                "id": "16174b4b-50c0-42b0-8ed2-50086a65df8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3017ece2-37d5-4e78-a652-10057eb856b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ce944c2-0b86-4ed5-a216-9a0ae7061a92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3017ece2-37d5-4e78-a652-10057eb856b4",
                    "LayerId": "8c107300-0910-4d69-ac59-2311f261ebe0"
                }
            ]
        },
        {
            "id": "d98aaf28-9eee-4373-9ec2-38f98a300d6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ef9bd8-6504-456e-8d25-93e0c14981c4",
            "compositeImage": {
                "id": "6b8eb561-d162-4dd2-886e-c441367a19f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d98aaf28-9eee-4373-9ec2-38f98a300d6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce42ffd5-4ebb-4126-9f2b-00214ee83289",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d98aaf28-9eee-4373-9ec2-38f98a300d6e",
                    "LayerId": "8c107300-0910-4d69-ac59-2311f261ebe0"
                }
            ]
        },
        {
            "id": "9e16b7b1-3ce6-47b3-be58-7227489e7513",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ef9bd8-6504-456e-8d25-93e0c14981c4",
            "compositeImage": {
                "id": "4e0a2d6f-a7c1-481b-a706-e7fe4b8b1f92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e16b7b1-3ce6-47b3-be58-7227489e7513",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55c55748-4d4b-479e-9345-335503795525",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e16b7b1-3ce6-47b3-be58-7227489e7513",
                    "LayerId": "8c107300-0910-4d69-ac59-2311f261ebe0"
                }
            ]
        },
        {
            "id": "d1af532d-aacc-4eb6-9890-632eace0b4f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ef9bd8-6504-456e-8d25-93e0c14981c4",
            "compositeImage": {
                "id": "b761e717-9712-4cfa-9c0c-ce4d1e04d519",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1af532d-aacc-4eb6-9890-632eace0b4f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "547e7440-ef21-4b83-abc6-a52617097d91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1af532d-aacc-4eb6-9890-632eace0b4f7",
                    "LayerId": "8c107300-0910-4d69-ac59-2311f261ebe0"
                }
            ]
        },
        {
            "id": "13661fa9-a6dd-48f8-a2e9-463432abc0aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ef9bd8-6504-456e-8d25-93e0c14981c4",
            "compositeImage": {
                "id": "ca75533e-4431-43a8-bcc3-3258e0b67182",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13661fa9-a6dd-48f8-a2e9-463432abc0aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbcba617-702a-46ea-bbba-65960f858823",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13661fa9-a6dd-48f8-a2e9-463432abc0aa",
                    "LayerId": "8c107300-0910-4d69-ac59-2311f261ebe0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 320,
    "layers": [
        {
            "id": "8c107300-0910-4d69-ac59-2311f261ebe0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d2ef9bd8-6504-456e-8d25-93e0c14981c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 184,
    "xorig": 92,
    "yorig": 319
}