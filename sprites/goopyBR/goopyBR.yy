{
    "id": "e73ff043-0aae-4ad4-8ecf-9d3896f1f791",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "goopyBR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 315,
    "bbox_left": 41,
    "bbox_right": 203,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "624f582f-76ad-4c45-b03c-2185cb25f429",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e73ff043-0aae-4ad4-8ecf-9d3896f1f791",
            "compositeImage": {
                "id": "d19c1ea5-eea4-4b90-9634-1c92b4a86c07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "624f582f-76ad-4c45-b03c-2185cb25f429",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c29d7db-07f0-4cf0-8b3c-cef4236c9000",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "624f582f-76ad-4c45-b03c-2185cb25f429",
                    "LayerId": "b9dd9e61-75cd-4b6c-947b-a84cf5427bec"
                }
            ]
        },
        {
            "id": "7b996444-777e-40bc-9eb5-ab773456f172",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e73ff043-0aae-4ad4-8ecf-9d3896f1f791",
            "compositeImage": {
                "id": "6679c403-4bfb-412b-a771-87fe9c61f1b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b996444-777e-40bc-9eb5-ab773456f172",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a59491d1-3d2e-4a92-9839-3cc3569befea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b996444-777e-40bc-9eb5-ab773456f172",
                    "LayerId": "b9dd9e61-75cd-4b6c-947b-a84cf5427bec"
                }
            ]
        },
        {
            "id": "dff62397-82cb-4230-8da4-b4290a970e45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e73ff043-0aae-4ad4-8ecf-9d3896f1f791",
            "compositeImage": {
                "id": "ee4c02d9-a04d-4cdf-a282-1cdd651fc9ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dff62397-82cb-4230-8da4-b4290a970e45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42f2b2ba-ab6e-4aed-a451-16eb930c8f47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dff62397-82cb-4230-8da4-b4290a970e45",
                    "LayerId": "b9dd9e61-75cd-4b6c-947b-a84cf5427bec"
                }
            ]
        },
        {
            "id": "7a9c42a0-35e9-4d4c-b8c6-6033d8f3313e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e73ff043-0aae-4ad4-8ecf-9d3896f1f791",
            "compositeImage": {
                "id": "bbbe167f-fdda-4e0e-a1b8-7efa2fa5ad22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a9c42a0-35e9-4d4c-b8c6-6033d8f3313e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e868bb15-942b-468e-98ba-cf0a511534e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a9c42a0-35e9-4d4c-b8c6-6033d8f3313e",
                    "LayerId": "b9dd9e61-75cd-4b6c-947b-a84cf5427bec"
                }
            ]
        },
        {
            "id": "a76aadac-2e77-45d7-b7e7-956d43b2e235",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e73ff043-0aae-4ad4-8ecf-9d3896f1f791",
            "compositeImage": {
                "id": "1e204930-04d0-4e73-9fef-1ab7aa02b42f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a76aadac-2e77-45d7-b7e7-956d43b2e235",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d4d1de8-4011-4b4c-ac21-afdaa236a1e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a76aadac-2e77-45d7-b7e7-956d43b2e235",
                    "LayerId": "b9dd9e61-75cd-4b6c-947b-a84cf5427bec"
                }
            ]
        },
        {
            "id": "d4ead0c0-188d-4541-9898-b43cb06bd0d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e73ff043-0aae-4ad4-8ecf-9d3896f1f791",
            "compositeImage": {
                "id": "4e43d11d-36f2-4709-8255-7a025e726815",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4ead0c0-188d-4541-9898-b43cb06bd0d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd3a9d5c-3e1e-4ad3-a230-ef94a969ae10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4ead0c0-188d-4541-9898-b43cb06bd0d9",
                    "LayerId": "b9dd9e61-75cd-4b6c-947b-a84cf5427bec"
                }
            ]
        },
        {
            "id": "6e5159c6-b049-4879-a03d-f666aa774f3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e73ff043-0aae-4ad4-8ecf-9d3896f1f791",
            "compositeImage": {
                "id": "fe14de28-3677-4871-b9ea-30c65df68d48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e5159c6-b049-4879-a03d-f666aa774f3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "507c4bd5-7c05-4291-8ccd-2a3e42b14b91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e5159c6-b049-4879-a03d-f666aa774f3b",
                    "LayerId": "b9dd9e61-75cd-4b6c-947b-a84cf5427bec"
                }
            ]
        },
        {
            "id": "a165e4d2-f189-4cc9-a211-df0c5adf504a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e73ff043-0aae-4ad4-8ecf-9d3896f1f791",
            "compositeImage": {
                "id": "2a844130-17eb-464d-ab2d-af7867baa3fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a165e4d2-f189-4cc9-a211-df0c5adf504a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a705dc6e-0f39-43b8-ab46-85a621bf85ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a165e4d2-f189-4cc9-a211-df0c5adf504a",
                    "LayerId": "b9dd9e61-75cd-4b6c-947b-a84cf5427bec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 320,
    "layers": [
        {
            "id": "b9dd9e61-75cd-4b6c-947b-a84cf5427bec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e73ff043-0aae-4ad4-8ecf-9d3896f1f791",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 238,
    "xorig": 119,
    "yorig": 319
}