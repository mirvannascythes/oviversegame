{
    "id": "8d87b371-40da-4662-9d57-8db5d77167aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "goopyUL",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 318,
    "bbox_left": 20,
    "bbox_right": 217,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9e7d59b-57b5-46ab-81d9-432a6f281621",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d87b371-40da-4662-9d57-8db5d77167aa",
            "compositeImage": {
                "id": "d5996ac8-d129-4190-b082-a9f959b84f0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9e7d59b-57b5-46ab-81d9-432a6f281621",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f597cc5-eafb-457f-b062-35199ef0405d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9e7d59b-57b5-46ab-81d9-432a6f281621",
                    "LayerId": "62ae4347-8740-46df-a095-45eb6f7203de"
                }
            ]
        },
        {
            "id": "2b700751-499d-4577-907a-157a6420b720",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d87b371-40da-4662-9d57-8db5d77167aa",
            "compositeImage": {
                "id": "79420bb2-1a52-4c59-8eca-76aff51ed252",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b700751-499d-4577-907a-157a6420b720",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b06473a6-f918-43fc-9e40-02805ef853d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b700751-499d-4577-907a-157a6420b720",
                    "LayerId": "62ae4347-8740-46df-a095-45eb6f7203de"
                }
            ]
        },
        {
            "id": "77d586fb-0450-4b36-9009-0c6dff6ebb7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d87b371-40da-4662-9d57-8db5d77167aa",
            "compositeImage": {
                "id": "101f1ca5-8771-409b-970e-e8d8c12e4f08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77d586fb-0450-4b36-9009-0c6dff6ebb7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54d01844-edf0-453e-ad42-4336acc0cd85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77d586fb-0450-4b36-9009-0c6dff6ebb7f",
                    "LayerId": "62ae4347-8740-46df-a095-45eb6f7203de"
                }
            ]
        },
        {
            "id": "5a410d1e-d7e8-40ac-9cd6-566035099a2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d87b371-40da-4662-9d57-8db5d77167aa",
            "compositeImage": {
                "id": "a1323a7f-8674-4672-b72f-0a5e9ced086d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a410d1e-d7e8-40ac-9cd6-566035099a2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d8c14e6-5e78-44fd-87aa-33a90d5b550f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a410d1e-d7e8-40ac-9cd6-566035099a2e",
                    "LayerId": "62ae4347-8740-46df-a095-45eb6f7203de"
                }
            ]
        },
        {
            "id": "5d8d0f24-87d8-4c9b-9c49-9b9eca512f7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d87b371-40da-4662-9d57-8db5d77167aa",
            "compositeImage": {
                "id": "de769611-d585-495d-bc9b-56fa9cfac80c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d8d0f24-87d8-4c9b-9c49-9b9eca512f7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56d252f4-a90f-4c1f-9def-b77a3292bcbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d8d0f24-87d8-4c9b-9c49-9b9eca512f7e",
                    "LayerId": "62ae4347-8740-46df-a095-45eb6f7203de"
                }
            ]
        },
        {
            "id": "d6e845fa-0907-48e9-b83b-4a7a656caa14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d87b371-40da-4662-9d57-8db5d77167aa",
            "compositeImage": {
                "id": "0d331581-b14d-45c5-abf1-61c5a1e1af07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6e845fa-0907-48e9-b83b-4a7a656caa14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "901dd41c-d6a6-4abc-9f38-c78a9046b0d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6e845fa-0907-48e9-b83b-4a7a656caa14",
                    "LayerId": "62ae4347-8740-46df-a095-45eb6f7203de"
                }
            ]
        },
        {
            "id": "496f6a83-bfe3-46e2-9864-9f2d649fb38e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d87b371-40da-4662-9d57-8db5d77167aa",
            "compositeImage": {
                "id": "1def2104-2de4-4f52-afb9-422c70a37a26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "496f6a83-bfe3-46e2-9864-9f2d649fb38e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76ff8465-ff83-41bd-851c-f3ffcfb50801",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "496f6a83-bfe3-46e2-9864-9f2d649fb38e",
                    "LayerId": "62ae4347-8740-46df-a095-45eb6f7203de"
                }
            ]
        },
        {
            "id": "f5ed7c2e-e29e-415a-a244-4a879ae42fed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d87b371-40da-4662-9d57-8db5d77167aa",
            "compositeImage": {
                "id": "69b26baf-2d59-4cf1-ab29-012cea9572a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5ed7c2e-e29e-415a-a244-4a879ae42fed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fd9148e-e9f2-41d5-a6ec-89a77ee0d50b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5ed7c2e-e29e-415a-a244-4a879ae42fed",
                    "LayerId": "62ae4347-8740-46df-a095-45eb6f7203de"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 320,
    "layers": [
        {
            "id": "62ae4347-8740-46df-a095-45eb6f7203de",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d87b371-40da-4662-9d57-8db5d77167aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 238,
    "xorig": 119,
    "yorig": 319
}