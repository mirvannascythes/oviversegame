{
    "id": "fc784311-94d7-42c6-a0aa-01b65346fbab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "menuBanner",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 660,
    "bbox_left": 4,
    "bbox_right": 466,
    "bbox_top": 53,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a2f0852-410e-471b-8e65-1c4c976fd222",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc784311-94d7-42c6-a0aa-01b65346fbab",
            "compositeImage": {
                "id": "78b22547-b70c-4eec-ac5b-f08b8d90ede5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a2f0852-410e-471b-8e65-1c4c976fd222",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e736583e-4a9a-42c8-8731-2a517f0d88d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a2f0852-410e-471b-8e65-1c4c976fd222",
                    "LayerId": "951e311b-6cd5-47f6-bb55-37f6eee07999"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 666,
    "layers": [
        {
            "id": "951e311b-6cd5-47f6-bb55-37f6eee07999",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc784311-94d7-42c6-a0aa-01b65346fbab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 468,
    "xorig": 0,
    "yorig": 0
}