{
    "id": "21566656-8a1c-4513-a40b-e6b78bbd3aa2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gooDrop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 4,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c7c1fa1-fd98-4677-99a1-bf209722863e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21566656-8a1c-4513-a40b-e6b78bbd3aa2",
            "compositeImage": {
                "id": "2c7992d3-7276-4a6d-8809-90051c1c8e1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c7c1fa1-fd98-4677-99a1-bf209722863e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81a9f320-1b0e-44c1-810d-7be21138c307",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c7c1fa1-fd98-4677-99a1-bf209722863e",
                    "LayerId": "a38d4e15-cab1-4530-b5c7-8640a0936d5f"
                },
                {
                    "id": "03ef487d-ee06-4433-b28e-656f8008d70c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c7c1fa1-fd98-4677-99a1-bf209722863e",
                    "LayerId": "59ff534a-1abc-4ed4-bc1d-6033e16c76dc"
                }
            ]
        },
        {
            "id": "b687f254-ee8e-48b6-b941-4317b3ae4155",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21566656-8a1c-4513-a40b-e6b78bbd3aa2",
            "compositeImage": {
                "id": "6e5e6f21-7bd1-42fe-9550-0cea0eebb66e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b687f254-ee8e-48b6-b941-4317b3ae4155",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f34102e3-7577-4232-9014-cbd708e140cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b687f254-ee8e-48b6-b941-4317b3ae4155",
                    "LayerId": "59ff534a-1abc-4ed4-bc1d-6033e16c76dc"
                },
                {
                    "id": "4be788a4-b679-480e-be55-625e7050a623",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b687f254-ee8e-48b6-b941-4317b3ae4155",
                    "LayerId": "a38d4e15-cab1-4530-b5c7-8640a0936d5f"
                }
            ]
        },
        {
            "id": "250bdc3f-588a-4d61-929e-112b393e8783",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21566656-8a1c-4513-a40b-e6b78bbd3aa2",
            "compositeImage": {
                "id": "470df5b0-335b-4429-8fcf-8b1a2a02ea48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "250bdc3f-588a-4d61-929e-112b393e8783",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c624775f-f615-407e-93c3-275d8f9a389d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "250bdc3f-588a-4d61-929e-112b393e8783",
                    "LayerId": "59ff534a-1abc-4ed4-bc1d-6033e16c76dc"
                },
                {
                    "id": "71f8aa6e-409e-4d3f-b64f-46c7166503f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "250bdc3f-588a-4d61-929e-112b393e8783",
                    "LayerId": "a38d4e15-cab1-4530-b5c7-8640a0936d5f"
                }
            ]
        },
        {
            "id": "ea548da5-e0a7-4049-88b4-19dd40b78f5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21566656-8a1c-4513-a40b-e6b78bbd3aa2",
            "compositeImage": {
                "id": "38befb74-7252-4a7c-b625-901f58cab544",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea548da5-e0a7-4049-88b4-19dd40b78f5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b40d1135-f659-4a4f-965e-f44469e23537",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea548da5-e0a7-4049-88b4-19dd40b78f5c",
                    "LayerId": "59ff534a-1abc-4ed4-bc1d-6033e16c76dc"
                },
                {
                    "id": "7352a6d4-0e0b-49aa-99b7-1929723d8b6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea548da5-e0a7-4049-88b4-19dd40b78f5c",
                    "LayerId": "a38d4e15-cab1-4530-b5c7-8640a0936d5f"
                }
            ]
        },
        {
            "id": "239717d4-a9fa-43c8-9f2f-ab15b7f221ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21566656-8a1c-4513-a40b-e6b78bbd3aa2",
            "compositeImage": {
                "id": "20b01d2f-80b9-4ad3-a8f1-d3419a8a6414",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "239717d4-a9fa-43c8-9f2f-ab15b7f221ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c651d71-54f6-461e-8423-97c77bb98a3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "239717d4-a9fa-43c8-9f2f-ab15b7f221ba",
                    "LayerId": "59ff534a-1abc-4ed4-bc1d-6033e16c76dc"
                },
                {
                    "id": "f6414ba1-068e-42ba-84a9-9eb185459554",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "239717d4-a9fa-43c8-9f2f-ab15b7f221ba",
                    "LayerId": "a38d4e15-cab1-4530-b5c7-8640a0936d5f"
                }
            ]
        },
        {
            "id": "654059e3-0947-4191-9c3c-801aca9ad45c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21566656-8a1c-4513-a40b-e6b78bbd3aa2",
            "compositeImage": {
                "id": "fc63c52b-7a44-4a2b-b08a-b2cb871f07b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "654059e3-0947-4191-9c3c-801aca9ad45c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd5beea2-0f05-4462-a0c1-2235f1a2d7dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "654059e3-0947-4191-9c3c-801aca9ad45c",
                    "LayerId": "59ff534a-1abc-4ed4-bc1d-6033e16c76dc"
                },
                {
                    "id": "9dffee36-ca03-4b81-92f3-a3933f68a4e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "654059e3-0947-4191-9c3c-801aca9ad45c",
                    "LayerId": "a38d4e15-cab1-4530-b5c7-8640a0936d5f"
                }
            ]
        },
        {
            "id": "97f9f850-cbd6-48d0-acb2-fe571706554f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21566656-8a1c-4513-a40b-e6b78bbd3aa2",
            "compositeImage": {
                "id": "625c2822-2f4c-4364-89e9-183c6c2f4498",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97f9f850-cbd6-48d0-acb2-fe571706554f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52e9e43e-fbd2-4c0d-b545-c0ad3998e55c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97f9f850-cbd6-48d0-acb2-fe571706554f",
                    "LayerId": "59ff534a-1abc-4ed4-bc1d-6033e16c76dc"
                },
                {
                    "id": "d2b42881-7a98-4f2e-b063-95a0fb995cca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97f9f850-cbd6-48d0-acb2-fe571706554f",
                    "LayerId": "a38d4e15-cab1-4530-b5c7-8640a0936d5f"
                }
            ]
        },
        {
            "id": "d0732664-db0d-4150-8470-682e6767c2a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21566656-8a1c-4513-a40b-e6b78bbd3aa2",
            "compositeImage": {
                "id": "9a9306a4-a4f3-4089-a901-620ebfd86e95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0732664-db0d-4150-8470-682e6767c2a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66eca00c-08d7-4a01-8bf3-0feca5b575e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0732664-db0d-4150-8470-682e6767c2a3",
                    "LayerId": "59ff534a-1abc-4ed4-bc1d-6033e16c76dc"
                },
                {
                    "id": "cc4a228a-0ef2-4190-882b-e1c2fe906e19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0732664-db0d-4150-8470-682e6767c2a3",
                    "LayerId": "a38d4e15-cab1-4530-b5c7-8640a0936d5f"
                }
            ]
        },
        {
            "id": "189cd008-d41c-4f13-bee6-40f037e00242",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21566656-8a1c-4513-a40b-e6b78bbd3aa2",
            "compositeImage": {
                "id": "d8a4abf0-54b9-47ca-b158-eca04ab7aa97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "189cd008-d41c-4f13-bee6-40f037e00242",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75a6170c-241d-4b22-a5dd-0138c333550f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "189cd008-d41c-4f13-bee6-40f037e00242",
                    "LayerId": "59ff534a-1abc-4ed4-bc1d-6033e16c76dc"
                },
                {
                    "id": "8e51cbcf-5f71-4f99-a439-7e06fa56919e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "189cd008-d41c-4f13-bee6-40f037e00242",
                    "LayerId": "a38d4e15-cab1-4530-b5c7-8640a0936d5f"
                }
            ]
        },
        {
            "id": "f04283dc-2358-471e-b31a-c37741eff155",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21566656-8a1c-4513-a40b-e6b78bbd3aa2",
            "compositeImage": {
                "id": "aab96a5e-71f1-4145-9bd4-eb3d13dbfe81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f04283dc-2358-471e-b31a-c37741eff155",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "601d3b42-1162-4a2b-ac66-f5adaa2afe7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f04283dc-2358-471e-b31a-c37741eff155",
                    "LayerId": "59ff534a-1abc-4ed4-bc1d-6033e16c76dc"
                },
                {
                    "id": "0b973719-462a-4268-b838-b25ea5960d70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f04283dc-2358-471e-b31a-c37741eff155",
                    "LayerId": "a38d4e15-cab1-4530-b5c7-8640a0936d5f"
                }
            ]
        },
        {
            "id": "dbea931a-47f8-44df-b332-f091d5c3cd4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21566656-8a1c-4513-a40b-e6b78bbd3aa2",
            "compositeImage": {
                "id": "dc74a3fa-0ebb-4781-9da1-786375c6fd30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbea931a-47f8-44df-b332-f091d5c3cd4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "637c325f-5f95-4f79-8cd1-44d38e588cff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbea931a-47f8-44df-b332-f091d5c3cd4f",
                    "LayerId": "59ff534a-1abc-4ed4-bc1d-6033e16c76dc"
                },
                {
                    "id": "f7e92ae1-8e54-4615-b794-67e714cbcc05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbea931a-47f8-44df-b332-f091d5c3cd4f",
                    "LayerId": "a38d4e15-cab1-4530-b5c7-8640a0936d5f"
                }
            ]
        },
        {
            "id": "d16ef066-1cd3-4b1f-8552-7bcd304d8d77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21566656-8a1c-4513-a40b-e6b78bbd3aa2",
            "compositeImage": {
                "id": "b0c34a5a-6b85-4549-8ee2-644803ecbbbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d16ef066-1cd3-4b1f-8552-7bcd304d8d77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "350f975e-7b24-43f2-8741-e2d2aa7aacbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d16ef066-1cd3-4b1f-8552-7bcd304d8d77",
                    "LayerId": "59ff534a-1abc-4ed4-bc1d-6033e16c76dc"
                },
                {
                    "id": "d13a60ff-77ad-4e06-8c3e-9087b2a96b4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d16ef066-1cd3-4b1f-8552-7bcd304d8d77",
                    "LayerId": "a38d4e15-cab1-4530-b5c7-8640a0936d5f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "59ff534a-1abc-4ed4-bc1d-6033e16c76dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21566656-8a1c-4513-a40b-e6b78bbd3aa2",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "a38d4e15-cab1-4530-b5c7-8640a0936d5f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21566656-8a1c-4513-a40b-e6b78bbd3aa2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}