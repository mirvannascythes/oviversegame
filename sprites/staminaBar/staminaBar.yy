{
    "id": "decdeffe-75a0-4ac9-9ac2-bebe77cf1650",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "staminaBar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9e4d0cf3-965c-40a3-a9e4-ebc837e24e73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "decdeffe-75a0-4ac9-9ac2-bebe77cf1650",
            "compositeImage": {
                "id": "8ea49874-2f39-4542-8da2-84d35c6663a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e4d0cf3-965c-40a3-a9e4-ebc837e24e73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "effca458-a659-403e-884b-680084e57c33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e4d0cf3-965c-40a3-a9e4-ebc837e24e73",
                    "LayerId": "8b5a5a14-e53a-4727-bab8-71ec86496644"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8b5a5a14-e53a-4727-bab8-71ec86496644",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "decdeffe-75a0-4ac9-9ac2-bebe77cf1650",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}