{
    "id": "5cc106a7-a3c4-41dc-b4e8-ede4c1ed9a9a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "isometricWall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "396ce202-9b2d-4417-9f0d-dc2412e46c1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cc106a7-a3c4-41dc-b4e8-ede4c1ed9a9a",
            "compositeImage": {
                "id": "931d3700-a6ab-4d54-991e-86e27823e394",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "396ce202-9b2d-4417-9f0d-dc2412e46c1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3070996c-b5fd-4054-bb6d-9d2b6a04e1ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "396ce202-9b2d-4417-9f0d-dc2412e46c1a",
                    "LayerId": "9706133c-ab75-4795-908c-a2971493dcb8"
                },
                {
                    "id": "1dc41b0f-d8c1-4660-bc2d-39d9ad3e1e57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "396ce202-9b2d-4417-9f0d-dc2412e46c1a",
                    "LayerId": "bda56d1b-8ab0-4dec-9773-80e2b8463f53"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bda56d1b-8ab0-4dec-9773-80e2b8463f53",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5cc106a7-a3c4-41dc-b4e8-ede4c1ed9a9a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default (2)",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "9706133c-ab75-4795-908c-a2971493dcb8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5cc106a7-a3c4-41dc-b4e8-ede4c1ed9a9a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 32
}