{
    "id": "9cbfd756-7c4e-4130-80e9-9147c3a6c484",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tileConcrete",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b7e64c82-2d8a-478e-9e95-069c376248b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cbfd756-7c4e-4130-80e9-9147c3a6c484",
            "compositeImage": {
                "id": "ec3b2a4a-0a0f-47a6-a248-c61b479681f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7e64c82-2d8a-478e-9e95-069c376248b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8235816e-a4b4-4b5f-b150-12b177ebd038",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7e64c82-2d8a-478e-9e95-069c376248b3",
                    "LayerId": "31f7bc38-b237-468a-852f-4793eaa1fbce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "31f7bc38-b237-468a-852f-4793eaa1fbce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9cbfd756-7c4e-4130-80e9-9147c3a6c484",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}