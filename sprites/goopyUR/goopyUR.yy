{
    "id": "e4e35477-7e56-47b3-a193-8f97bbf8df20",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "goopyUR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 316,
    "bbox_left": 0,
    "bbox_right": 210,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f61427d-c3d4-4597-b795-5cb452ed260a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4e35477-7e56-47b3-a193-8f97bbf8df20",
            "compositeImage": {
                "id": "d1513513-f3bc-4bf9-af9d-2d915dac12a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f61427d-c3d4-4597-b795-5cb452ed260a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e8c7dbe-c916-4074-9427-ff5da05d1f66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f61427d-c3d4-4597-b795-5cb452ed260a",
                    "LayerId": "c6e940cf-d093-4001-a550-861f9814abb6"
                }
            ]
        },
        {
            "id": "ced67209-9e31-416e-b6e6-20222da12b99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4e35477-7e56-47b3-a193-8f97bbf8df20",
            "compositeImage": {
                "id": "5763a745-b641-4405-8ca9-dd20d7a93116",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ced67209-9e31-416e-b6e6-20222da12b99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce05c612-48c0-4e06-919c-fad40b2160d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ced67209-9e31-416e-b6e6-20222da12b99",
                    "LayerId": "c6e940cf-d093-4001-a550-861f9814abb6"
                }
            ]
        },
        {
            "id": "e42175e0-8db5-458e-8daf-8ba7d101801c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4e35477-7e56-47b3-a193-8f97bbf8df20",
            "compositeImage": {
                "id": "3a7ad29c-3d8c-418c-a18f-661a595f2e19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e42175e0-8db5-458e-8daf-8ba7d101801c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60c758a5-b774-4a2a-83fc-b4ad6255d34e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e42175e0-8db5-458e-8daf-8ba7d101801c",
                    "LayerId": "c6e940cf-d093-4001-a550-861f9814abb6"
                }
            ]
        },
        {
            "id": "a0094d22-609d-48fa-8fed-719df15e22e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4e35477-7e56-47b3-a193-8f97bbf8df20",
            "compositeImage": {
                "id": "c448138d-9c16-4873-a47b-d5ed4a94967c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0094d22-609d-48fa-8fed-719df15e22e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f74157c1-5b0f-41ea-ab5a-3a2ef1edc079",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0094d22-609d-48fa-8fed-719df15e22e1",
                    "LayerId": "c6e940cf-d093-4001-a550-861f9814abb6"
                }
            ]
        },
        {
            "id": "a5267ef7-2f46-4320-b371-7c3860a38bfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4e35477-7e56-47b3-a193-8f97bbf8df20",
            "compositeImage": {
                "id": "38ad806d-4c0f-4815-89f5-973f910f59f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5267ef7-2f46-4320-b371-7c3860a38bfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83ae1ef6-f4cc-4b7f-861d-2ef556b16df2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5267ef7-2f46-4320-b371-7c3860a38bfa",
                    "LayerId": "c6e940cf-d093-4001-a550-861f9814abb6"
                }
            ]
        },
        {
            "id": "08846ea1-6ab3-4823-a12b-60886381328c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4e35477-7e56-47b3-a193-8f97bbf8df20",
            "compositeImage": {
                "id": "808c322d-1d47-46da-aa81-9be9fed82c50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08846ea1-6ab3-4823-a12b-60886381328c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9a6929d-f784-4d3d-a9f3-340e182dedd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08846ea1-6ab3-4823-a12b-60886381328c",
                    "LayerId": "c6e940cf-d093-4001-a550-861f9814abb6"
                }
            ]
        },
        {
            "id": "7143e12c-40b3-490d-aa81-2f46b9dfb6a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4e35477-7e56-47b3-a193-8f97bbf8df20",
            "compositeImage": {
                "id": "7194d48d-d1a9-41ee-bfd8-492924f949e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7143e12c-40b3-490d-aa81-2f46b9dfb6a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57a4f128-b4a5-4869-994c-79b49175022c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7143e12c-40b3-490d-aa81-2f46b9dfb6a4",
                    "LayerId": "c6e940cf-d093-4001-a550-861f9814abb6"
                }
            ]
        },
        {
            "id": "d2135729-b83e-4d08-9b8b-a7ff7be7d690",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4e35477-7e56-47b3-a193-8f97bbf8df20",
            "compositeImage": {
                "id": "5b80abda-5605-4f39-970a-51d4f5f18425",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2135729-b83e-4d08-9b8b-a7ff7be7d690",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7bd854b-ad05-4d77-b7a8-9541d3fa308f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2135729-b83e-4d08-9b8b-a7ff7be7d690",
                    "LayerId": "c6e940cf-d093-4001-a550-861f9814abb6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 320,
    "layers": [
        {
            "id": "c6e940cf-d093-4001-a550-861f9814abb6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4e35477-7e56-47b3-a193-8f97bbf8df20",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 238,
    "xorig": 119,
    "yorig": 319
}