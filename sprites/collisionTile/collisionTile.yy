{
    "id": "1a366439-3beb-4aee-9116-b3215521d00a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "collisionTile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "918803b6-2fb4-4258-8462-2fd278eafd3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a366439-3beb-4aee-9116-b3215521d00a",
            "compositeImage": {
                "id": "b6cef3fc-08e0-4ede-9150-3120cf965900",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "918803b6-2fb4-4258-8462-2fd278eafd3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15498e82-6b46-42a4-8eac-149c8689295d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "918803b6-2fb4-4258-8462-2fd278eafd3b",
                    "LayerId": "b32531bb-7b3d-4023-87d4-172ebd272526"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b32531bb-7b3d-4023-87d4-172ebd272526",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a366439-3beb-4aee-9116-b3215521d00a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}