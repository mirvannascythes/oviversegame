{
    "id": "60048f01-6900-4afe-a9a3-933abb75108b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "isometricTrap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "57781434-a7b2-4f72-aab0-55d9049c81aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60048f01-6900-4afe-a9a3-933abb75108b",
            "compositeImage": {
                "id": "db1e208d-9bf5-46eb-bbed-28031a201eb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57781434-a7b2-4f72-aab0-55d9049c81aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f4d475b-8a74-4beb-9585-a93699453304",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57781434-a7b2-4f72-aab0-55d9049c81aa",
                    "LayerId": "845fcc1c-90d2-41f2-95cd-d3f29851e645"
                },
                {
                    "id": "5a5a2a05-7e3e-4890-b706-87c017a66647",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57781434-a7b2-4f72-aab0-55d9049c81aa",
                    "LayerId": "62dfc796-e709-4286-9ed0-e5d2e3c91d5e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "62dfc796-e709-4286-9ed0-e5d2e3c91d5e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60048f01-6900-4afe-a9a3-933abb75108b",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "845fcc1c-90d2-41f2-95cd-d3f29851e645",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60048f01-6900-4afe-a9a3-933abb75108b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}