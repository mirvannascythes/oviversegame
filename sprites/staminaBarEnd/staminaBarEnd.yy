{
    "id": "9a35e6c1-e639-4735-806e-bb9fbced4c80",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "staminaBarEnd",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "174495e7-781e-4ee9-9a83-7814ea100a89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a35e6c1-e639-4735-806e-bb9fbced4c80",
            "compositeImage": {
                "id": "2ca3870c-d7a1-465e-9b53-9ab7f132ac49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "174495e7-781e-4ee9-9a83-7814ea100a89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89210af9-b141-4ef7-bb01-88559c3ee3da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "174495e7-781e-4ee9-9a83-7814ea100a89",
                    "LayerId": "8e86985f-3531-4e7e-8815-0ccca0814f5c"
                },
                {
                    "id": "c35d45e6-04e1-4c02-9eb4-514b0946ba35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "174495e7-781e-4ee9-9a83-7814ea100a89",
                    "LayerId": "4fb74d25-b058-4611-bb8b-f2087c2823d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4fb74d25-b058-4611-bb8b-f2087c2823d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a35e6c1-e639-4735-806e-bb9fbced4c80",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "8e86985f-3531-4e7e-8815-0ccca0814f5c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a35e6c1-e639-4735-806e-bb9fbced4c80",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1 (2)",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}