{
    "id": "fe1a9cd9-f0d3-4da9-af6d-e23746e0380a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "arrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bd768f63-e399-4ecf-aad1-429ba27cd0c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe1a9cd9-f0d3-4da9-af6d-e23746e0380a",
            "compositeImage": {
                "id": "e3f23e7c-256a-46c2-9596-a3c82bb446f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd768f63-e399-4ecf-aad1-429ba27cd0c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da78a34c-7299-4755-9215-847da6dc67e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd768f63-e399-4ecf-aad1-429ba27cd0c4",
                    "LayerId": "ee785e4e-cdf8-40de-b047-c89da8483986"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ee785e4e-cdf8-40de-b047-c89da8483986",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe1a9cd9-f0d3-4da9-af6d-e23746e0380a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}