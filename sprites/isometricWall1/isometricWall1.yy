{
    "id": "1bef3178-71d7-4b96-8d8a-3c4c054b701c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "isometricWall1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23d44f58-eeb6-4ac2-89da-bb9738116ad3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bef3178-71d7-4b96-8d8a-3c4c054b701c",
            "compositeImage": {
                "id": "083a366e-b2d0-4d52-bf46-728bf7d41708",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23d44f58-eeb6-4ac2-89da-bb9738116ad3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ce968cd-b9e9-4d73-bfee-325e22baad8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23d44f58-eeb6-4ac2-89da-bb9738116ad3",
                    "LayerId": "b0bf5f11-4862-409e-a3ba-7c06307f6ff8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b0bf5f11-4862-409e-a3ba-7c06307f6ff8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1bef3178-71d7-4b96-8d8a-3c4c054b701c",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1 (2)",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 32
}