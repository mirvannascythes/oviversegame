{
    "id": "f742dcde-be08-427a-98fe-12352f7a7a47",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "victimStaminaBar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c4e9a82b-db4c-44a4-8c22-bda0e7cca6bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f742dcde-be08-427a-98fe-12352f7a7a47",
            "compositeImage": {
                "id": "eafd2c61-e9c2-4d01-aa57-94a118078fa6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4e9a82b-db4c-44a4-8c22-bda0e7cca6bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8789b0a3-a8ec-4ab6-9e0f-f62453bf8ceb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4e9a82b-db4c-44a4-8c22-bda0e7cca6bc",
                    "LayerId": "6f111147-99b3-406f-a3dd-7cbe9a332ab8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6f111147-99b3-406f-a3dd-7cbe9a332ab8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f742dcde-be08-427a-98fe-12352f7a7a47",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}