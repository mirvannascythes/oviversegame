{
    "id": "6c2a4c6d-1b10-4e75-8834-a845885f6c56",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "playerDebug",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13d85900-b791-47bc-9d19-21bbd1b0a997",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2a4c6d-1b10-4e75-8834-a845885f6c56",
            "compositeImage": {
                "id": "1eb09ee7-2f90-4f9e-98a3-c5fde1e583a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13d85900-b791-47bc-9d19-21bbd1b0a997",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5b89ed3-c4f0-4b21-ac83-b1c783448977",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13d85900-b791-47bc-9d19-21bbd1b0a997",
                    "LayerId": "91f5f1f5-f08a-4753-b2e8-8eab5d882ddd"
                },
                {
                    "id": "50f813d6-0828-4436-a38f-3010a27149de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13d85900-b791-47bc-9d19-21bbd1b0a997",
                    "LayerId": "3a80fcb4-be1f-450a-9a20-517503c89616"
                }
            ]
        },
        {
            "id": "1579433f-aadf-4917-bfa2-a85bbbaadd09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2a4c6d-1b10-4e75-8834-a845885f6c56",
            "compositeImage": {
                "id": "d57ba540-3193-4ef8-9829-6c72d85809c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1579433f-aadf-4917-bfa2-a85bbbaadd09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d8a4a5b-1148-42a3-a5fc-55f3d67334d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1579433f-aadf-4917-bfa2-a85bbbaadd09",
                    "LayerId": "3a80fcb4-be1f-450a-9a20-517503c89616"
                },
                {
                    "id": "61e673e1-38b6-42d5-a67f-175585fa2aa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1579433f-aadf-4917-bfa2-a85bbbaadd09",
                    "LayerId": "91f5f1f5-f08a-4753-b2e8-8eab5d882ddd"
                }
            ]
        },
        {
            "id": "049c4110-d6dd-4498-8384-01001be5c96e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2a4c6d-1b10-4e75-8834-a845885f6c56",
            "compositeImage": {
                "id": "c9002910-1624-43c9-a9c7-b0dacfcc6eb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "049c4110-d6dd-4498-8384-01001be5c96e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e200724b-ca7f-424f-b1ab-3b91fe4d030d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "049c4110-d6dd-4498-8384-01001be5c96e",
                    "LayerId": "3a80fcb4-be1f-450a-9a20-517503c89616"
                },
                {
                    "id": "ceabd801-c0fb-4c18-885f-beadbb4ed3aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "049c4110-d6dd-4498-8384-01001be5c96e",
                    "LayerId": "91f5f1f5-f08a-4753-b2e8-8eab5d882ddd"
                }
            ]
        },
        {
            "id": "03f8d891-dcfd-461f-ae93-5384cb342ed2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2a4c6d-1b10-4e75-8834-a845885f6c56",
            "compositeImage": {
                "id": "1a2e4bb3-4f8d-4cb7-88e4-064cdfd2f3cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03f8d891-dcfd-461f-ae93-5384cb342ed2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af441389-b714-42f3-8512-e3185bbf9b80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03f8d891-dcfd-461f-ae93-5384cb342ed2",
                    "LayerId": "3a80fcb4-be1f-450a-9a20-517503c89616"
                },
                {
                    "id": "fc64af32-61bc-4b83-bd15-8c1ca7236428",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03f8d891-dcfd-461f-ae93-5384cb342ed2",
                    "LayerId": "91f5f1f5-f08a-4753-b2e8-8eab5d882ddd"
                }
            ]
        },
        {
            "id": "ddcf0538-1a30-481a-8c54-c23577b39304",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2a4c6d-1b10-4e75-8834-a845885f6c56",
            "compositeImage": {
                "id": "f588854e-4b4e-4038-9085-295b06ad9bd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddcf0538-1a30-481a-8c54-c23577b39304",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59e65ef4-521b-4d4b-a07a-30700bd7cf04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddcf0538-1a30-481a-8c54-c23577b39304",
                    "LayerId": "3a80fcb4-be1f-450a-9a20-517503c89616"
                },
                {
                    "id": "141330e1-cf6a-4eca-90bf-2f8ffa93e645",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddcf0538-1a30-481a-8c54-c23577b39304",
                    "LayerId": "91f5f1f5-f08a-4753-b2e8-8eab5d882ddd"
                }
            ]
        },
        {
            "id": "14ce4dc3-83b7-485f-8644-8043362cf400",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2a4c6d-1b10-4e75-8834-a845885f6c56",
            "compositeImage": {
                "id": "2054d7f5-26e1-4ea7-9b57-1b6afc5b3551",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14ce4dc3-83b7-485f-8644-8043362cf400",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95b4fab5-1929-465c-b19d-488e22b0fb01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14ce4dc3-83b7-485f-8644-8043362cf400",
                    "LayerId": "3a80fcb4-be1f-450a-9a20-517503c89616"
                },
                {
                    "id": "f9e97c73-efde-4f32-9ecf-ffae53d51ea0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14ce4dc3-83b7-485f-8644-8043362cf400",
                    "LayerId": "91f5f1f5-f08a-4753-b2e8-8eab5d882ddd"
                }
            ]
        },
        {
            "id": "0c334894-a62b-4a3e-8ea0-8429b8890757",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2a4c6d-1b10-4e75-8834-a845885f6c56",
            "compositeImage": {
                "id": "dca6fe12-df09-43f7-8dd7-94a7e696be1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c334894-a62b-4a3e-8ea0-8429b8890757",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a8a992b-ae42-491c-b2c8-03d5f7fefc36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c334894-a62b-4a3e-8ea0-8429b8890757",
                    "LayerId": "3a80fcb4-be1f-450a-9a20-517503c89616"
                },
                {
                    "id": "059d2d0a-2d88-4c1e-a8d1-605358e3a55d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c334894-a62b-4a3e-8ea0-8429b8890757",
                    "LayerId": "91f5f1f5-f08a-4753-b2e8-8eab5d882ddd"
                }
            ]
        },
        {
            "id": "5c06d77b-7941-4b0b-9c5d-c65bad317e37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2a4c6d-1b10-4e75-8834-a845885f6c56",
            "compositeImage": {
                "id": "a457afc3-16be-44ce-a395-f028cd19f01f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c06d77b-7941-4b0b-9c5d-c65bad317e37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21ddb79c-2a08-44bf-afed-ef058fe4f0a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c06d77b-7941-4b0b-9c5d-c65bad317e37",
                    "LayerId": "3a80fcb4-be1f-450a-9a20-517503c89616"
                },
                {
                    "id": "9defe52c-05e6-4cdb-96df-e70dafb65976",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c06d77b-7941-4b0b-9c5d-c65bad317e37",
                    "LayerId": "91f5f1f5-f08a-4753-b2e8-8eab5d882ddd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3a80fcb4-be1f-450a-9a20-517503c89616",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c2a4c6d-1b10-4e75-8834-a845885f6c56",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "91f5f1f5-f08a-4753-b2e8-8eab5d882ddd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c2a4c6d-1b10-4e75-8834-a845885f6c56",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 55,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}