{
    "id": "f0ae9466-60a4-459d-8e19-06f265cdb06d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "keyboardSpace",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "130883ee-94a4-4ceb-942a-e38bbd6b950b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0ae9466-60a4-459d-8e19-06f265cdb06d",
            "compositeImage": {
                "id": "842d41e0-444e-4462-84a2-b8525a719536",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "130883ee-94a4-4ceb-942a-e38bbd6b950b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0e28089-0bd9-4e40-ab45-752db6ab1f88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "130883ee-94a4-4ceb-942a-e38bbd6b950b",
                    "LayerId": "b30b5910-643e-48ab-a168-3938de684a6d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b30b5910-643e-48ab-a168-3938de684a6d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f0ae9466-60a4-459d-8e19-06f265cdb06d",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1 (2) (2)",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}