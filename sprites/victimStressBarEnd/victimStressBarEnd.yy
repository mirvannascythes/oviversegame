{
    "id": "c4954b1a-a414-4a31-85aa-3872d71b662d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "victimStressBarEnd",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 16,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5634f40a-a12d-430a-b2ca-8baeeb0cf89a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4954b1a-a414-4a31-85aa-3872d71b662d",
            "compositeImage": {
                "id": "0c6e4060-c8ee-415a-91c7-9a21095b4b74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5634f40a-a12d-430a-b2ca-8baeeb0cf89a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7216ef65-ab81-464f-a4ea-34000e328005",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5634f40a-a12d-430a-b2ca-8baeeb0cf89a",
                    "LayerId": "a644ff63-5b33-4ab7-968a-d5a47ea502ca"
                },
                {
                    "id": "5fb11034-2f8e-495d-ac49-3c5a1eccfa01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5634f40a-a12d-430a-b2ca-8baeeb0cf89a",
                    "LayerId": "3d6e1890-498c-463e-bb9c-2b84b2293ca0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3d6e1890-498c-463e-bb9c-2b84b2293ca0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4954b1a-a414-4a31-85aa-3872d71b662d",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "a644ff63-5b33-4ab7-968a-d5a47ea502ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4954b1a-a414-4a31-85aa-3872d71b662d",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1 (2)",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}