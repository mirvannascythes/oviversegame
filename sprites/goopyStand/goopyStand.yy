{
    "id": "474e2e21-189e-4d01-a78c-b6b418c9c345",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "goopyStand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 319,
    "bbox_left": 16,
    "bbox_right": 228,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "09a7732f-94f0-4afe-a27f-0334bbb759b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "474e2e21-189e-4d01-a78c-b6b418c9c345",
            "compositeImage": {
                "id": "4466837e-0a5c-4d3b-82cd-00312060342a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09a7732f-94f0-4afe-a27f-0334bbb759b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0e056c7-b333-4df4-a0d6-f3123b79fa89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09a7732f-94f0-4afe-a27f-0334bbb759b2",
                    "LayerId": "3a199c8a-30e8-4c49-8acb-84db641174e8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 320,
    "layers": [
        {
            "id": "3a199c8a-30e8-4c49-8acb-84db641174e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "474e2e21-189e-4d01-a78c-b6b418c9c345",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 238,
    "xorig": 119,
    "yorig": 319
}