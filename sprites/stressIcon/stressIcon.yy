{
    "id": "82b850e3-922f-4c1f-aff7-69221ba9393e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "stressIcon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "08f87e8f-ad51-4e2c-b833-2ace4f4ac09e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82b850e3-922f-4c1f-aff7-69221ba9393e",
            "compositeImage": {
                "id": "462da174-b660-4c9c-a3eb-a8e1db2915cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08f87e8f-ad51-4e2c-b833-2ace4f4ac09e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0963e94b-5b0f-40ea-8fac-54054f09dc62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08f87e8f-ad51-4e2c-b833-2ace4f4ac09e",
                    "LayerId": "4c70220e-acc4-479f-be40-3aa3755f06d4"
                },
                {
                    "id": "bd1a271a-db4d-40a2-a6b6-256f761cabd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08f87e8f-ad51-4e2c-b833-2ace4f4ac09e",
                    "LayerId": "e233254a-47f6-4b15-94f8-5ed17d60c2c1"
                },
                {
                    "id": "d4f7cd48-5fec-4392-908f-7b1534bc65fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08f87e8f-ad51-4e2c-b833-2ace4f4ac09e",
                    "LayerId": "66db34c4-45da-44d0-b8ff-9059dcfbeb15"
                },
                {
                    "id": "72d60dd1-b136-477f-8ac6-b6fda7ec107e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08f87e8f-ad51-4e2c-b833-2ace4f4ac09e",
                    "LayerId": "2c5fa4d0-909e-4f3b-a8c5-7c6942d43f65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2c5fa4d0-909e-4f3b-a8c5-7c6942d43f65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82b850e3-922f-4c1f-aff7-69221ba9393e",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 3",
            "opacity": 100,
            "visible": false
        },
        {
            "id": "4c70220e-acc4-479f-be40-3aa3755f06d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82b850e3-922f-4c1f-aff7-69221ba9393e",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "e233254a-47f6-4b15-94f8-5ed17d60c2c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82b850e3-922f-4c1f-aff7-69221ba9393e",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "66db34c4-45da-44d0-b8ff-9059dcfbeb15",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82b850e3-922f-4c1f-aff7-69221ba9393e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}