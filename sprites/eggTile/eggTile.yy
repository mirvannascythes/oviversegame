{
    "id": "d037d4fa-d705-4318-902e-6533862ca499",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eggTile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ca2d8b3a-68cf-4e61-b18a-c59b68bd6e7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d037d4fa-d705-4318-902e-6533862ca499",
            "compositeImage": {
                "id": "4f883926-e069-4c33-99f8-b078b5df7875",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca2d8b3a-68cf-4e61-b18a-c59b68bd6e7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d6e47db-8aac-480b-bb22-50d2e92bf983",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca2d8b3a-68cf-4e61-b18a-c59b68bd6e7e",
                    "LayerId": "47ffabfe-c41e-4111-96cc-f28155f70155"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "47ffabfe-c41e-4111-96cc-f28155f70155",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d037d4fa-d705-4318-902e-6533862ca499",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}