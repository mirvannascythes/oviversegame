{
    "id": "0b5bf7ec-1f10-446c-9e2e-c7dd89a39b6a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "isometricTable",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e99b3042-a189-48d7-b3d3-284cb02d77c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b5bf7ec-1f10-446c-9e2e-c7dd89a39b6a",
            "compositeImage": {
                "id": "9999f956-10ce-40cf-b566-916316db55b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e99b3042-a189-48d7-b3d3-284cb02d77c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ddfcddb-f737-4808-bf1f-5adb4cfc9b80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e99b3042-a189-48d7-b3d3-284cb02d77c5",
                    "LayerId": "a2aefc39-657b-4cca-a3f3-f6c56264a375"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a2aefc39-657b-4cca-a3f3-f6c56264a375",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b5bf7ec-1f10-446c-9e2e-c7dd89a39b6a",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1 (2)",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 32
}