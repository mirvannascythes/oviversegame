{
    "id": "df65ae4e-3b99-4372-92b7-640280bd9005",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "goopyBL",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 312,
    "bbox_left": 38,
    "bbox_right": 196,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "af461ad1-4300-4007-bf95-9cd8d839aedf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df65ae4e-3b99-4372-92b7-640280bd9005",
            "compositeImage": {
                "id": "08e2fea7-7bec-4adc-8954-7585cc1deb3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af461ad1-4300-4007-bf95-9cd8d839aedf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd348c76-33fc-4797-ad47-7641a3d041d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af461ad1-4300-4007-bf95-9cd8d839aedf",
                    "LayerId": "98e6a7b5-865e-4fbf-930c-0d486b8e3900"
                }
            ]
        },
        {
            "id": "e7353a78-f81f-439b-8c19-28ab71c5289b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df65ae4e-3b99-4372-92b7-640280bd9005",
            "compositeImage": {
                "id": "5a93386f-2d8a-4d29-8ec8-dd6aaa5b32df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7353a78-f81f-439b-8c19-28ab71c5289b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca091415-fc4d-43eb-9b29-ffee3c83f053",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7353a78-f81f-439b-8c19-28ab71c5289b",
                    "LayerId": "98e6a7b5-865e-4fbf-930c-0d486b8e3900"
                }
            ]
        },
        {
            "id": "69c95029-458c-4ca5-a0bc-d0d1f794f330",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df65ae4e-3b99-4372-92b7-640280bd9005",
            "compositeImage": {
                "id": "21f2e9c0-9b5e-4ca3-9af6-63c4fdf86a3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69c95029-458c-4ca5-a0bc-d0d1f794f330",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e88c7cca-e8ea-4404-a65d-42e7fdc643cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69c95029-458c-4ca5-a0bc-d0d1f794f330",
                    "LayerId": "98e6a7b5-865e-4fbf-930c-0d486b8e3900"
                }
            ]
        },
        {
            "id": "5307031e-8d9d-4753-8799-1d4fb6db6e0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df65ae4e-3b99-4372-92b7-640280bd9005",
            "compositeImage": {
                "id": "4e97d850-9375-4b39-b6a9-ac09851ded84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5307031e-8d9d-4753-8799-1d4fb6db6e0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b536366-b8fd-4501-839f-ec42c9276dba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5307031e-8d9d-4753-8799-1d4fb6db6e0f",
                    "LayerId": "98e6a7b5-865e-4fbf-930c-0d486b8e3900"
                }
            ]
        },
        {
            "id": "fd7b8dc7-6211-4a3c-bbd5-d24c3668707b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df65ae4e-3b99-4372-92b7-640280bd9005",
            "compositeImage": {
                "id": "f972b6e8-bd6a-40e3-9df6-943362b8e95a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd7b8dc7-6211-4a3c-bbd5-d24c3668707b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3533ec5-343b-4209-afa9-4e559ab9ade9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd7b8dc7-6211-4a3c-bbd5-d24c3668707b",
                    "LayerId": "98e6a7b5-865e-4fbf-930c-0d486b8e3900"
                }
            ]
        },
        {
            "id": "2c00109b-60fa-4aba-b5d3-54bd06678dc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df65ae4e-3b99-4372-92b7-640280bd9005",
            "compositeImage": {
                "id": "6eec6f7f-c864-431b-a6c4-7aabd5e429cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c00109b-60fa-4aba-b5d3-54bd06678dc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1592df0a-82ae-4671-9235-eb4bca5629a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c00109b-60fa-4aba-b5d3-54bd06678dc2",
                    "LayerId": "98e6a7b5-865e-4fbf-930c-0d486b8e3900"
                }
            ]
        },
        {
            "id": "3733b565-526e-4cd5-85d9-dd1884308ae3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df65ae4e-3b99-4372-92b7-640280bd9005",
            "compositeImage": {
                "id": "aa69558b-1cb1-47c8-95a9-6070cb5e72fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3733b565-526e-4cd5-85d9-dd1884308ae3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3aeb9aca-6427-43cf-8f8c-6e2ad7392bd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3733b565-526e-4cd5-85d9-dd1884308ae3",
                    "LayerId": "98e6a7b5-865e-4fbf-930c-0d486b8e3900"
                }
            ]
        },
        {
            "id": "d01e90a2-9757-405e-94bd-7bde4a8436a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df65ae4e-3b99-4372-92b7-640280bd9005",
            "compositeImage": {
                "id": "b1a06f9a-28ba-4a57-8ba1-44bbf3ca8ca6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d01e90a2-9757-405e-94bd-7bde4a8436a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56167bc6-d7fe-405f-ad1a-df3000ed3a38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d01e90a2-9757-405e-94bd-7bde4a8436a2",
                    "LayerId": "98e6a7b5-865e-4fbf-930c-0d486b8e3900"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 320,
    "layers": [
        {
            "id": "98e6a7b5-865e-4fbf-930c-0d486b8e3900",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df65ae4e-3b99-4372-92b7-640280bd9005",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 238,
    "xorig": 119,
    "yorig": 319
}