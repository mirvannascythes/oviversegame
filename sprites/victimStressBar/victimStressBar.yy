{
    "id": "6a32226f-cbe5-49bc-8b8b-7d3706189abd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "victimStressBar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "888414ff-58d8-4265-a101-98a4d960c4d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a32226f-cbe5-49bc-8b8b-7d3706189abd",
            "compositeImage": {
                "id": "b8e6283a-09ec-439d-a37b-d84105fd4f16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "888414ff-58d8-4265-a101-98a4d960c4d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea0f24fe-777f-4e6f-aff2-0c7f9eb860b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "888414ff-58d8-4265-a101-98a4d960c4d9",
                    "LayerId": "45d576ca-938d-4493-8e2d-f5d069339629"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "45d576ca-938d-4493-8e2d-f5d069339629",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a32226f-cbe5-49bc-8b8b-7d3706189abd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}