{
    "id": "6ec09555-a527-44cf-b325-fb872f592202",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "aiDebugTap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d4879a4-30fb-472c-9e24-cad1e5a46e73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ec09555-a527-44cf-b325-fb872f592202",
            "compositeImage": {
                "id": "9b670d66-fdf7-4571-8fc9-ddc629988195",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d4879a4-30fb-472c-9e24-cad1e5a46e73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cd4396e-cbe9-4e60-9717-cf497a0fef4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d4879a4-30fb-472c-9e24-cad1e5a46e73",
                    "LayerId": "bd9ccc83-a58b-4b01-9820-36b918e7c32c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "bd9ccc83-a58b-4b01-9820-36b918e7c32c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ec09555-a527-44cf-b325-fb872f592202",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}