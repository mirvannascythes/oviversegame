{
    "id": "a04a3ff5-b30f-430a-895f-2c7b7ebf100c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "isometricTile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a11ec07-f99a-4f73-b141-b09324771640",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a04a3ff5-b30f-430a-895f-2c7b7ebf100c",
            "compositeImage": {
                "id": "887863e4-713f-494c-aa5d-6077b74918d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a11ec07-f99a-4f73-b141-b09324771640",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7ef42d5-d69e-4626-8faf-000045c33ba1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a11ec07-f99a-4f73-b141-b09324771640",
                    "LayerId": "ef59481f-3e23-455e-9faa-5c8d62af49d1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ef59481f-3e23-455e-9faa-5c8d62af49d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a04a3ff5-b30f-430a-895f-2c7b7ebf100c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}