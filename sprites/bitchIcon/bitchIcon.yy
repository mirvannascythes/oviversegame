{
    "id": "3e42f8c3-ac83-4b5c-989c-8cc8f0ec439f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bitchIcon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 33,
    "bbox_right": 123,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbe86af1-1d59-404d-8741-778495d94746",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e42f8c3-ac83-4b5c-989c-8cc8f0ec439f",
            "compositeImage": {
                "id": "5a18b043-9b13-4975-aaac-ae427f5d603a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbe86af1-1d59-404d-8741-778495d94746",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5917f39-4f3d-4c42-98ad-0f6d39e12f51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbe86af1-1d59-404d-8741-778495d94746",
                    "LayerId": "c91f6961-c58c-48d9-9888-c795667fab1c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "c91f6961-c58c-48d9-9888-c795667fab1c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e42f8c3-ac83-4b5c-989c-8cc8f0ec439f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}