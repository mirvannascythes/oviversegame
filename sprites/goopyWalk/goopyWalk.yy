{
    "id": "c23b7f40-119a-4871-ae2c-2000d095bbf7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "goopyWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 319,
    "bbox_left": 16,
    "bbox_right": 233,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f29d191-2287-4057-9280-9ec4f2a309d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c23b7f40-119a-4871-ae2c-2000d095bbf7",
            "compositeImage": {
                "id": "98faf5c7-b3ea-46f2-84c1-c36d259e2151",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f29d191-2287-4057-9280-9ec4f2a309d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe0e0afd-577b-4362-bc8c-764496cec6a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f29d191-2287-4057-9280-9ec4f2a309d6",
                    "LayerId": "1fd95fa2-135f-4e1a-be71-4a68f05c9cc3"
                }
            ]
        },
        {
            "id": "6775d2dd-d4f8-4d93-882c-d17351250e67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c23b7f40-119a-4871-ae2c-2000d095bbf7",
            "compositeImage": {
                "id": "8d86176d-eb55-47f3-b053-86b6d1c32ba9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6775d2dd-d4f8-4d93-882c-d17351250e67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a010d030-a3b2-483d-aba4-c7b0b25db061",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6775d2dd-d4f8-4d93-882c-d17351250e67",
                    "LayerId": "1fd95fa2-135f-4e1a-be71-4a68f05c9cc3"
                }
            ]
        },
        {
            "id": "99812c7d-8254-468c-8554-fe7c477a745b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c23b7f40-119a-4871-ae2c-2000d095bbf7",
            "compositeImage": {
                "id": "e1d4a26b-6daf-442d-8da0-877ffb42caee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99812c7d-8254-468c-8554-fe7c477a745b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d20fbc0b-4f91-4bcf-8565-b17ecfaf0187",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99812c7d-8254-468c-8554-fe7c477a745b",
                    "LayerId": "1fd95fa2-135f-4e1a-be71-4a68f05c9cc3"
                }
            ]
        },
        {
            "id": "384eb46f-f854-48e6-a385-36b2a87dce49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c23b7f40-119a-4871-ae2c-2000d095bbf7",
            "compositeImage": {
                "id": "202c2514-7dd4-4bef-af84-f1378d08fe34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "384eb46f-f854-48e6-a385-36b2a87dce49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fea3638-f3b7-4c1a-98d2-4637f78267bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "384eb46f-f854-48e6-a385-36b2a87dce49",
                    "LayerId": "1fd95fa2-135f-4e1a-be71-4a68f05c9cc3"
                }
            ]
        },
        {
            "id": "0d1926ab-cfd9-4389-a230-4c3a44133cfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c23b7f40-119a-4871-ae2c-2000d095bbf7",
            "compositeImage": {
                "id": "886883fe-ecb6-4b36-acba-afa8f88e7d5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d1926ab-cfd9-4389-a230-4c3a44133cfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ae8c0e8-4519-4d04-87a5-01f8ea4231f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d1926ab-cfd9-4389-a230-4c3a44133cfb",
                    "LayerId": "1fd95fa2-135f-4e1a-be71-4a68f05c9cc3"
                }
            ]
        },
        {
            "id": "4a336b1a-ff9e-4e07-be9d-c66aa07b51a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c23b7f40-119a-4871-ae2c-2000d095bbf7",
            "compositeImage": {
                "id": "593e3964-6a20-48c4-bbf2-dcbeea952839",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a336b1a-ff9e-4e07-be9d-c66aa07b51a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4071254-31d7-452c-9047-b6873b6b4f65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a336b1a-ff9e-4e07-be9d-c66aa07b51a7",
                    "LayerId": "1fd95fa2-135f-4e1a-be71-4a68f05c9cc3"
                }
            ]
        },
        {
            "id": "df6a1bed-0cb1-4605-a6f9-f44aaaaafd43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c23b7f40-119a-4871-ae2c-2000d095bbf7",
            "compositeImage": {
                "id": "7ca111ef-4b93-4681-b5a6-0f24f56eb66e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df6a1bed-0cb1-4605-a6f9-f44aaaaafd43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa1fe8a0-f50c-430b-8973-1a9e0f1de91e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df6a1bed-0cb1-4605-a6f9-f44aaaaafd43",
                    "LayerId": "1fd95fa2-135f-4e1a-be71-4a68f05c9cc3"
                }
            ]
        },
        {
            "id": "5a74e771-57d4-4aef-b822-1ffb8cf7de1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c23b7f40-119a-4871-ae2c-2000d095bbf7",
            "compositeImage": {
                "id": "dd50588b-653f-4bd7-aaa7-e86a96a88d53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a74e771-57d4-4aef-b822-1ffb8cf7de1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3718933e-b6be-4e10-8111-7368641bf97b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a74e771-57d4-4aef-b822-1ffb8cf7de1c",
                    "LayerId": "1fd95fa2-135f-4e1a-be71-4a68f05c9cc3"
                }
            ]
        },
        {
            "id": "75387d65-4c99-46f0-a7d2-af97c6197244",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c23b7f40-119a-4871-ae2c-2000d095bbf7",
            "compositeImage": {
                "id": "31479e17-f0d5-4b9d-8a1f-464e3d950ce0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75387d65-4c99-46f0-a7d2-af97c6197244",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74615d3a-8c73-4b23-8d7a-ab4e62838fcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75387d65-4c99-46f0-a7d2-af97c6197244",
                    "LayerId": "1fd95fa2-135f-4e1a-be71-4a68f05c9cc3"
                }
            ]
        },
        {
            "id": "7ac27005-839d-45e6-9dfd-a67b9274fa65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c23b7f40-119a-4871-ae2c-2000d095bbf7",
            "compositeImage": {
                "id": "289f8b28-4373-4218-bf71-500a211896eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ac27005-839d-45e6-9dfd-a67b9274fa65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29f10f7a-0719-4f6a-8434-2f1104d92240",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ac27005-839d-45e6-9dfd-a67b9274fa65",
                    "LayerId": "1fd95fa2-135f-4e1a-be71-4a68f05c9cc3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 320,
    "layers": [
        {
            "id": "1fd95fa2-135f-4e1a-be71-4a68f05c9cc3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c23b7f40-119a-4871-ae2c-2000d095bbf7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 238,
    "xorig": 119,
    "yorig": 319
}