{
    "id": "a7327687-56fe-426e-9b4e-f1ce2a574a60",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "tileObjectStoneSlab11",
    "eventList": [
        {
            "id": "0b6c82ca-5e32-4e5c-baeb-813dc92290ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a7327687-56fe-426e-9b4e-f1ce2a574a60"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8961da13-a73d-4c99-8a51-7d85160a29c1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c701599f-a1b5-4431-83c1-43b039a6ac02",
    "visible": true
}