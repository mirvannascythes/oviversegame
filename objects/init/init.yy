{
    "id": "30c23302-4ef1-448d-8521-b9cf16bfc728",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "init",
    "eventList": [
        {
            "id": "015315f6-2a45-492e-a913-2bb89fe72b72",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "30c23302-4ef1-448d-8521-b9cf16bfc728"
        },
        {
            "id": "0a17177a-d318-47ea-8096-25aa17a7a308",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "30c23302-4ef1-448d-8521-b9cf16bfc728"
        },
        {
            "id": "1bb2b9e2-eb42-4682-950d-43e14b2a08de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "30c23302-4ef1-448d-8521-b9cf16bfc728"
        },
        {
            "id": "255dd188-cdde-4982-a179-b35f8303ce12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "30c23302-4ef1-448d-8521-b9cf16bfc728"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bdb3ccdc-be3f-4f37-a7b7-9295cf13b0ee",
    "visible": true
}