if(drawTextBox = 1){
	
	draw_rectangle_color(view_wport[0]/4,view_hport[0]/4,(view_wport[0]/4)*3,(view_hport[0]/4)*3,c_white,c_white,c_white,c_white,0);
	draw_set_colour(c_blue);
	draw_set_font(f_default);
	ATEX_draw((view_wport[0]/4)+32,(view_hport[0]/4)+32, drawnText,(((view_wport[0]/4)*3)-(view_wport[0]/4))-64,fa_center);
	
} 

//Grab the window dimensions in a tidy variable
var wH = view_hport[0]
var wW = view_wport[0]
//Draw Icon and Name
draw_sprite(valentineIcon,0,x+4,((wH/5)*4)-132);
draw_set_colour(c_white);
draw_set_font(Bauhaus93);
ATEX_draw(x+144,((wH/5)*4)-32, "<outline c=@c_black>Valentine</outline>",300,fa_left);
//Draw Victim Icon and Name
draw_sprite(bitchIcon,0,wW-132,((wH/5)*4)-132);
draw_set_colour(c_white);
draw_set_font(Bauhaus93);
ATEX_draw(wW-144-300,((wH/5)*4)-32, "<outline c=@c_black>Bitch</outline>",300,fa_right);
//Draw Stamina Bar
draw_set_alpha(0.5);
draw_rectangle_color(x+48,((wH/5)*4)+16,((wW/5)*2)+48,((wH/5)*4)+48,c_black,c_black,c_black,c_black,0) //Draw dark underlying rectangle
draw_sprite_ext(staminaBarEnd,0,((wW/5)*2)+49,((wH/5)*4)+16,1,1,0,c_black,0.5); //And a end cap
draw_set_alpha(0.3);
draw_sprite_stretched_ext(staminaBar,0,x+48,((wH/5)*4)+16,stamina*((wW/5)*2),32,c_white,0.3); //Draw Expected Stamina
draw_sprite(staminaBarEnd,0,x+48+stamina*((wW/5)*2),((wH/5)*4)+16); //And a end cap
draw_set_alpha(1);
draw_sprite_stretched_ext(staminaBar,0,x+48,((wH/5)*4)+16,staminaDisp*((wW/5)*2),32,c_white,1); //Draw Stamina Move Effect
draw_sprite(staminaBarEnd,0,x+48+staminaDisp*((wW/5)*2),((wH/5)*4)+16); //And a end cap
draw_sprite(staminaIcon,0,x+16,(wH/5)*4); //Draw the Stamina Icon
//Draw Victim Stamina Bar
draw_set_alpha(0.5);
draw_rectangle_color(wW-80,((wH/5)*4)+16,wW-((wW/5)*2)-48,((wH/5)*4)+48,c_black,c_black,c_black,c_black,0) //Draw dark underlying rectangle
draw_sprite_ext(victimStaminaBarEnd,0,wW-((wW/5)*2)-63-16,((wH/5)*4)+16,1,1,0,c_black,0.5); //And a end cap
draw_set_alpha(0.3);
draw_sprite_stretched_ext(victimStaminaBar,0,wW-48-victimStamina*((wW/5)*2),((wH/5)*4)+16,victimStamina*((wW/5)*2),32,c_white,0.3); //Draw Expected Stamina
draw_sprite(victimStaminaBarEnd,0,wW-16-(victimStamina*((wW/5)*2)+64),((wH/5)*4)+16); //And a end cap
draw_set_alpha(1);
draw_sprite_stretched_ext(victimStaminaBar,0,wW-48-victimStaminaDisp*((wW/5)*2),((wH/5)*4)+16,victimStaminaDisp*((wW/5)*2),32,c_white,1); //Draw Stamina Move Effect
draw_sprite(victimStaminaBarEnd,0,wW-16-(victimStaminaDisp*((wW/5)*2)+63),((wH/5)*4)+16); //And a end cap
draw_sprite(victimStaminaIcon,0,wW-80,(wH/5)*4); //Draw the Stamina Icon
//Draw Victim Stress Bar
draw_set_alpha(0.5);
draw_rectangle_color(wW-80,((wH/5)*4)+84+8,wW-((wW/5)*2)-48,((wH/5)*4)+116+8,c_black,c_black,c_black,c_black,0) //Draw dark underlying rectangle
draw_sprite_ext(victimStaminaBarEnd,0,wW-((wW/5)*2)-63-16,((wH/5)*4)+84+8,1,1,0,c_black,0.5); //And a end cap
draw_set_alpha(0.3);
draw_sprite_stretched_ext(victimStressBar,0,wW-48-victimStress*((wW/5)*2),((wH/5)*4)+84+8,victimStress*((wW/5)*2),32,c_white,0.3); //Draw Expected Stamina
draw_sprite(victimStressBarEnd,0,wW-16-(victimStress*((wW/5)*2)+64),((wH/5)*4)+84+8); //And a end cap
draw_set_alpha(1);
draw_sprite_stretched_ext(victimStressBar,0,wW-48-victimStressDisp*((wW/5)*2),((wH/5)*4)+84+8,victimStressDisp*((wW/5)*2),32,c_white,1); //Draw Stamina Move Effect
draw_sprite(victimStressBarEnd,0,wW-16-(victimStressDisp*((wW/5)*2)+63),((wH/5)*4)+84+8); //And a end cap
draw_sprite(stressIcon,0,wW-80,((wH/5)*4)+68+8); //Draw the Stamina Icon
//Draw Keys
draw_set_colour(c_white);
if(global.key1 = 1){draw_sprite_ext(keyboardKey,0,x+32,((wH/5)*4)+84,1,1,0,c_gray,1)
	draw_set_colour(c_green);
	ATEX_draw(x+48,((wH/5)*4)+68+16+(8*global.key1), "<outline c=@c_black>Q</outline>",32,fa_center);
}else{draw_sprite_ext(keyboardKey,0,x+32,((wH/5)*4)+76,1,1,0,c_white,1)
	ATEX_draw(x+48,((wH/5)*4)+68+16+(8*global.key1), "<outline c=@c_black>Q</outline>",32,fa_center);
}//Draw a key
draw_set_colour(c_white);
if(global.key2 = 1){draw_sprite_ext(keyboardKey,0,x+128,((wH/5)*4)+84,1,1,0,c_gray,1)
	draw_set_colour(c_green);
	ATEX_draw(x+144,((wH/5)*4)+68+16+(8*global.key2), "<outline c=@c_black>E</outline>",32,fa_center);
}else{draw_sprite_ext(keyboardKey,0,x+128,((wH/5)*4)+76,1,1,0,c_white,1)
	ATEX_draw(x+144,((wH/5)*4)+68+16+(8*global.key2), "<outline c=@c_black>E</outline>",32,fa_center);
}//Draw a key
draw_set_colour(c_white);
if(global.key3 = 1){draw_sprite_ext(keyboardKey,0,x+224,((wH/5)*4)+84,1,1,0,c_gray,1)
	draw_set_colour(c_green);
	ATEX_draw(x+240,((wH/5)*4)+68+16+(8*global.key3), "<outline c=@c_black>R</outline>",32,fa_center);
}else{draw_sprite_ext(keyboardKey,0,x+224,((wH/5)*4)+76,1,1,0,c_white,1)
	ATEX_draw(x+240,((wH/5)*4)+68+16+(8*global.key3), "<outline c=@c_black>R</outline>",32,fa_center);
}
draw_set_colour(c_white);
if(global.staminaCurrent <= 0){draw_sprite_ext(keyboardSpace,0,x+320,((wH/5)*4)+84,1,1,0,c_gray,1)
	draw_set_colour(c_red);
	ATEX_draw(x+336,((wH/5)*4)+68+16+8, "<outline c=@c_black>SPACE</outline>",96,fa_center);
}else if(global.keySpace = 1){draw_sprite_ext(keyboardSpace,0,x+320,((wH/5)*4)+84,1,1,0,c_gray,1)
	draw_set_colour(c_green);
	ATEX_draw(x+336,((wH/5)*4)+68+16+(8*global.keySpace), "<outline c=@c_black>SPACE</outline>",96,fa_center);
}else{draw_sprite_ext(keyboardSpace,0,x+320,((wH/5)*4)+76,1,1,0,c_white,1)
	ATEX_draw(x+336,((wH/5)*4)+68+16+(8*global.keySpace), "<outline c=@c_black>SPACE</outline>",96,fa_center);
}
//Draw Text To Keys
draw_set_colour(c_white);
draw_set_font(Bauhaus93);


