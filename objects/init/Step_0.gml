
//Fix any over/undercap. Calculate Stamina
if(global.staminaCurrent > global.staminaMax){global.staminaCurrent = global.staminaMax}
if(global.staminaCurrent < 0){global.staminaCurrent = 0}
stamina = global.staminaCurrent/global.staminaMax;
if(stamina > staminaDisp){staminaDisp += stamLag}
if(stamina < staminaDisp){staminaDisp -= stamLag}

//Fix any over/undercap. Calculate VictimStamina
if(global.victimStaminaCurrent > global.victimStaminaMax){global.victimStaminaCurrent = global.victimStaminaMax}
if(global.victimStaminaCurrent < 0){global.victimStaminaCurrent = 0}
victimStamina = global.victimStaminaCurrent/global.victimStaminaMax;
if(victimStamina > victimStaminaDisp){victimStaminaDisp += victimStamLag}
if(victimStamina < victimStaminaDisp){victimStaminaDisp -= victimStamLag}

//Fix any over/undercap. Calculate VictimStamina
if(global.victimStressCurrent > global.victimStressMax){global.victimStressCurrent = global.victimStressMax}
if(global.victimStressCurrent < 0){global.victimStressCurrent = 0}
victimStress = global.victimStressCurrent/global.victimStressMax;
if(victimStress > victimStressDisp){victimStressDisp += victimStressLag}
if(victimStress < victimStressDisp){victimStressDisp -= victimStressLag}