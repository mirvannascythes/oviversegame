//Declare globals here!
ATEX_init() //This is a requirement of ATEX text, a 3rd party plugin for drawing text (Outline FTW)
global.paused = 0; //Pause Game

global.debug = 0; // Adds a few helpful UI things for debugging when enabled

enumInit(); //Start state machines
drawTextBox = 0; //Draw a event style textbox? drawnText controls what is shown
drawnText = "<outline c=@c_black>Ready steady, it's time too get eggy!\n\n<pic: @legg fps=3><link src=@closeTextBox?title=\" Let's egg!\"?c=@c_white?cp=@c_lime> \n\n Use the WASD Keys to move and space to puddle form! </outline>"
//Stamina
stamina = 0.5; //Actual Stamina as a % of one
staminaDisp = 0; //Displayed Stamina
stamLag = 0.005; //How fast the stamina bar updates itself 0.01-0.001 atypical;
global.staminaCurrent = 50; //Current Stamina
global.staminaMax = 100; //Max Stamina should we wish upgrades
//Victim Stamina
victimStamina = 0.5; //Actual Stamina as a % of one
victimStaminaDisp = 0; //Displayed Stamina
victimStamLag = 0.005; //How fast the stamina bar updates itself 0.01-0.001 atypical;
global.victimStaminaCurrent = 80; //Current Stamina
global.victimStaminaMax = 100; //Max Stamina should we wish upgrades
//Victim Stress
victimStress = 0.5; //Actual Stamina as a % of one
victimStressDisp = 0; //Displayed Stamina
victimStressLag = 0.005; //How fast the stamina bar updates itself 0.01-0.001 atypical;
global.victimStressCurrent = 0; //Current Stamina
global.victimStressMax = 100; //Max Stamina should we wish upgrades
//Trap Selection Arrow
instance_create_depth(x-128,y-128,(-y*2)+16,arrowObject);
global.arrowTargetX = x-128;
global.arrowTargetY = y-128;
//Draw Key Variables
global.key1 = keyboard_check(ord("Q"));
global.key2 = keyboard_check(ord("E"));
global.key3 = keyboard_check(ord("R"));
global.keySpace = keyboard_check(vk_space);
//Valentine
global.puddleForm = false;
global.playerAntiDrawDistance = 128;