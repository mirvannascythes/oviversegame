{
    "id": "499e62c0-21b2-4ec1-b7c7-a11a28126908",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "egg",
    "eventList": [
        {
            "id": "4881d48c-530e-42a1-9767-fefd5e1a10e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "499e62c0-21b2-4ec1-b7c7-a11a28126908"
        },
        {
            "id": "f5648844-2876-4f69-b33a-1c6fe3fe7cc4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "cbbc0eed-e3bf-4088-8b9e-6b9ed233fc42",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "499e62c0-21b2-4ec1-b7c7-a11a28126908"
        },
        {
            "id": "0dbfb554-4851-48a0-91a7-86492e25f8a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "499e62c0-21b2-4ec1-b7c7-a11a28126908"
        },
        {
            "id": "8f07e248-79d6-43b4-b53e-da746729b4bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "499e62c0-21b2-4ec1-b7c7-a11a28126908"
        },
        {
            "id": "557df38f-e16b-492b-ab75-0c18bcd688fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "499e62c0-21b2-4ec1-b7c7-a11a28126908"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d037d4fa-d705-4318-902e-6533862ca499",
    "visible": true
}