{
    "id": "41633a49-446e-476c-b9b6-3647cd1bfa89",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "furnitureParent",
    "eventList": [
        {
            "id": "7cba9693-9fca-40ab-ad23-0b17383251fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "41633a49-446e-476c-b9b6-3647cd1bfa89"
        },
        {
            "id": "53c795d3-1650-4ab1-98bb-c66871ac67a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "41633a49-446e-476c-b9b6-3647cd1bfa89"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "30b894f8-3598-4ecb-9e94-d82843645b1c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4c349405-0668-4e1d-b340-fb437e83aca4",
    "visible": true
}