{
    "id": "b223e149-22bc-4260-abac-0839dc9f21e7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "particleEffectGoo",
    "eventList": [
        {
            "id": "b6b2a341-6bc6-41e1-8dca-0851eb51f10d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b223e149-22bc-4260-abac-0839dc9f21e7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bdb3ccdc-be3f-4f37-a7b7-9295cf13b0ee",
    "visible": true
}