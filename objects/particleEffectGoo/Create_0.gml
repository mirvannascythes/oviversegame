//setup
part_system=part_system_create();
part_emitter=part_emitter_create(part_system);
//properties
part_type=part_type_create();
part_type_shape(part_type,pt_shape_explosion);
//part_type_color1(part_type,c_blue,);
part_type_sprite(part_type, gooDrop, 1, 1, 0);
part_type_life(part_type, 160, 180);
part_type_scale(part_type, 2, 3)
part_type_gravity(part_type,0.05,270)
//
part_emitter_stream(part_system, part_emitter, part_type, -5);
//
//Trigger
part_emitter_region(part_system, part_emitter, x-16, x+16, y-16,y+16, ps_shape_rectangle, ps_distr_linear);
//part_emitter_region(part_system, part_emitter, 64, 96, 128,180, ps_shape_rectangle, ps_distr_linear);
