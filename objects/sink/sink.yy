{
    "id": "4598de63-4bcd-4a02-91cc-a7a8745da494",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "sink",
    "eventList": [
        {
            "id": "83e3fe95-5898-4ab8-ae82-4a8a632446de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4598de63-4bcd-4a02-91cc-a7a8745da494"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "41633a49-446e-476c-b9b6-3647cd1bfa89",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3d624ef0-7d1d-46c7-ad26-65dfd59537f7",
    "visible": true
}