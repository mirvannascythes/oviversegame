{
    "id": "e50107c3-4017-49e1-b199-30fca8e2c7cf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "wallParent",
    "eventList": [
        {
            "id": "9b6f89a9-903d-4284-b23c-db2c2cf52db0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e50107c3-4017-49e1-b199-30fca8e2c7cf"
        },
        {
            "id": "3bf01700-f63f-46d6-817e-4f8fd937c23e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e50107c3-4017-49e1-b199-30fca8e2c7cf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "30b894f8-3598-4ecb-9e94-d82843645b1c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "db70222f-b3a7-4c7d-a94e-b8588a75c742",
    "visible": true
}