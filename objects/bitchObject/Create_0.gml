//Movement
xySpeed = 0;
aSpeed = 0.01; //Acceleration Speed
mSpeed = 1; //Speed
speedModifier = 1; //The speed adjustment for different states i.e. running away
node = ds_list_create();
//AI
currentState = 0;
boredomCooldown = 0;
tired = 0;
tiredMax = 100;
tireSpeed = 0.01;
soreFeet = 0;
soreFeetMax = 100;
soreFeetSpeed = 0.001;
thirsty = 0;
thirstyMax = 100;
thirstySpeed = 0.001;
fear = 0;
fearMax = 100;
fearSpeed = -0.001; //The rate which we calm down after fear

//Stats
visionRadius = 75;
visionDistance = 200;
visionDirection = 0;
detected = 0;

//Setup
path = path_add();
potentialPath = path_add();