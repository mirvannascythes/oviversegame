{
    "id": "8961da13-a73d-4c99-8a51-7d85160a29c1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "tileParent",
    "eventList": [
        {
            "id": "c6b3fae8-27cf-4891-a53a-85f176373460",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8961da13-a73d-4c99-8a51-7d85160a29c1"
        },
        {
            "id": "8f2f7d28-a943-4efb-ae3c-3250372249bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8961da13-a73d-4c99-8a51-7d85160a29c1"
        },
        {
            "id": "77da34fd-fa43-444a-859b-9a79c198dbf1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "8961da13-a73d-4c99-8a51-7d85160a29c1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "372658e3-03da-4943-9d08-f7a6166fbf35",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "tileStoneSlab",
            "varName": "topSpr",
            "varType": 5
        }
    ],
    "solid": false,
    "spriteId": "a04a3ff5-b30f-430a-895f-2c7b7ebf100c",
    "visible": true
}