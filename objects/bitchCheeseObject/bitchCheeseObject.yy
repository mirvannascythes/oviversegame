{
    "id": "2fdc7417-e48e-4680-a127-3713217f67bd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "bitchCheeseObject",
    "eventList": [
        {
            "id": "214408b3-e14f-4fa0-a94c-767ffcfb25ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c60b2c44-0389-4113-b3c9-efec6746af89",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2fdc7417-e48e-4680-a127-3713217f67bd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2651ea56-47d3-40f0-af7d-ac0d4e6503ad",
    "visible": true
}