{
    "id": "b3873157-dc83-47fe-8319-04598aac5cf7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "arrowObject",
    "eventList": [
        {
            "id": "0ce3d338-5470-4114-9209-a7d960282b20",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b3873157-dc83-47fe-8319-04598aac5cf7"
        },
        {
            "id": "7a4b4f24-c7b1-4d1b-96d6-1ffeaf5b90ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b3873157-dc83-47fe-8319-04598aac5cf7"
        },
        {
            "id": "fc21b538-9a42-463d-be22-11ed390eb95f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b3873157-dc83-47fe-8319-04598aac5cf7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d037d4fa-d705-4318-902e-6533862ca499",
    "visible": true
}