{
    "id": "bf352699-2d37-41c8-9236-fc2d562e7cb0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "sink1",
    "eventList": [
        {
            "id": "faaec990-02b4-4a9c-b6ee-4ad00ed0cefd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bf352699-2d37-41c8-9236-fc2d562e7cb0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "41633a49-446e-476c-b9b6-3647cd1bfa89",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3d624ef0-7d1d-46c7-ad26-65dfd59537f7",
    "visible": true
}