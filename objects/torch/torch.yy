{
    "id": "d96696c9-3efb-46d1-82d3-b42394a27665",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "torch",
    "eventList": [
        {
            "id": "336799a9-d68e-4be6-ae4f-d7cc868f073c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d96696c9-3efb-46d1-82d3-b42394a27665"
        },
        {
            "id": "061d030c-fa67-4811-87e8-59c08457b881",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d96696c9-3efb-46d1-82d3-b42394a27665"
        },
        {
            "id": "9f84f27a-2225-4fdd-aa9a-680ab369f098",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d96696c9-3efb-46d1-82d3-b42394a27665"
        },
        {
            "id": "01ab7c7a-b340-44cf-902b-98ecd543a7c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "d96696c9-3efb-46d1-82d3-b42394a27665"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "41633a49-446e-476c-b9b6-3647cd1bfa89",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6a6f2ef3-7662-450a-8c3f-6d15746d6bbb",
    "visible": true
}