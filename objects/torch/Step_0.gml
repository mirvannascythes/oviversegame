if(disableTorch = 0){
	
	//Trigger
	part_emitter_stream(part_system, part_emitter, part_type, -2);
	
	//part_emitter_region(part_system, part_emitter, 64, 96, 128,180, ps_shape_rectangle, ps_distr_linear);

	//The Light!
	var _list = ds_list_create();
	var _num = collision_ellipse_list(cX-lR, cY-lR, cX+lR, cY+lR, tileParent, false, true, _list, false);
	if _num > 0
	    {
			var flicker = random_range(0,10);
	    for (var i = 0; i < _num; ++i;)
	        {
				if(!collision_line(x+32,y+8,_list[| i].x+16,_list[| i].y+8,wallParent,1,1) = 1){
					var lE = (lR-point_distance(x+32,y+8,_list[| i].x+16,_list[| i].y+8))/lR; //Apply light based on distance
					with(_list[| i]){
						if(rLight < (160+flicker)*lE){rLight = (160+flicker)*lE}
						if(gLight < (125+flicker)*lE){gLight = (125+flicker)*lE}
						if(bLight < (100+flicker)*lE){bLight = (100+flicker)*lE}
						if(rLight < 50){rLight = 50}
						if(gLight < 50){gLight = 50}
						if(bLight < 50){bLight = 50}
					}
				}
	        //instance_destroy(_list[| i]);
	        }
	    }
	ds_list_destroy(_list);
}else{
	//part_emitter_destroy(part_system, part_emitter);
	part_emitter_stream(part_system, part_emitter, part_type, 0);
}