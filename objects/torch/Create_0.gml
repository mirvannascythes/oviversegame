depth = (-y*2);
rLight = 50;
bLight = 50;
gLight = 50;
//Obtain light center
cX = x+(sprite_get_width(sprite_index)/2);
cY = y+12
spriteID = isometricTorch;
//Particle-setup
part_system=part_system_create();
part_emitter=part_emitter_create(part_system);
//Particle-properties
part_type=part_type_create();
part_system_depth(part_type, depth);
part_type_shape(part_type,pt_shape_explosion);
part_type_alpha3(part_type,0.4,0.3,0.1)
part_type_color3(part_type,c_yellow,c_red,c_black);
part_type_life(part_type, 30, 45);
part_type_scale(part_type, 0.15, 0.2)
part_type_gravity(part_type,0.05,90)
//Particle-Package

part_emitter_region(part_system, part_emitter, x+31, x+32, y-58,y-57, ps_shape_rectangle, ps_distr_linear);
	
//The light!
lR = 300; //LightRadius

disableTorch = 0;