{
    "id": "a5f2c426-6868-4e23-aabb-8b724ddb1314",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "tileObjectStoneSlab1",
    "eventList": [
        {
            "id": "58408e58-c59d-4e51-b285-abda7f10fe94",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a5f2c426-6868-4e23-aabb-8b724ddb1314"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8961da13-a73d-4c99-8a51-7d85160a29c1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c701599f-a1b5-4431-83c1-43b039a6ac02",
    "visible": true
}