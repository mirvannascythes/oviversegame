if(global.paused = 1){
	exit;
}
//Place me on the second height level
depth = (-y*2)+16;
//Keyboard! We will move this to a single script

playerButtons();
xySpeed = approach(xySpeed,mSpeed,aSpeed);
if(mouse_check_button_released(mb_left)){
	eraseNodes();
	createPath(mouse_x,mouse_y);
}


//state switch
switch currentState {
    case states.normal:
        normalState();
    break;

    case states.puddle:
        puddleState();
    break;
}

//increase frame by frameSpeed
frame += frameSpeed;

//Find the nearest trap and tell the arrow object to go there
if(distance_to_object(instance_nearest(x+32,y-16,trapParent)) < 32){
	global.arrowTargetX = instance_nearest(x+32,y-16,trapParent).x;
	global.arrowTargetY = instance_nearest(x+32,y-16,trapParent).y;
}else{
	global.arrowTargetX = -128;
	global.arrowTargetY = -128;
}