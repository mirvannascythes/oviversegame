{
    "id": "cbbc0eed-e3bf-4088-8b9e-6b9ed233fc42",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "playerObject",
    "eventList": [
        {
            "id": "45fcc095-ba38-4804-8256-b72f13cad3e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cbbc0eed-e3bf-4088-8b9e-6b9ed233fc42"
        },
        {
            "id": "c3039df3-4a20-437c-9a92-c651d072fcd0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "cbbc0eed-e3bf-4088-8b9e-6b9ed233fc42"
        },
        {
            "id": "1d9f4b4e-7a55-4459-9669-6346b7b19123",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cbbc0eed-e3bf-4088-8b9e-6b9ed233fc42"
        },
        {
            "id": "8a5c12cc-167e-425e-8ce6-8c184e6be93a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "cbbc0eed-e3bf-4088-8b9e-6b9ed233fc42"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1a366439-3beb-4aee-9116-b3215521d00a",
    "visible": true
}