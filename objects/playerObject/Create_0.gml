//**Stats**
xSpeed = 0; //Current Speed
ySpeed = 0; //Current Speed
xySpeed = 0;
speedModifier = 1;
aSpeed = 0.2; //Acceleration Speed
dSpeed = 0.5; //Deceleration Speed
mSpeed = 4; //Speed
gooFormDrain = 0.1; //How much Goo form costs to hold
//**Internal Variables**
//States
currentState = 0;
lastState = 0;
//Movement
left = false;
right = false;
up = false;
down = false;
node = ds_list_create();
path = path_add();
potentialPath = path_add();
//animation
frame = 0;
frameSpeed = 0.2;
sprite = sprite_index;
lastSprite = 0;
facing = 1;
xScale = 0.5;
yScale = 0.5;
scale = 0.45;
//Targetted Trap
trapTarget = instance_nearest(x+32,y-16,trapParent);
