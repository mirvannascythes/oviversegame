{
    "id": "5ff15fc4-fe4b-4d1c-a922-9ed9339f87e9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "tileObjectStoneSlab",
    "eventList": [
        {
            "id": "a44887a9-6772-4666-81c6-0a4f6e1d3f07",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5ff15fc4-fe4b-4d1c-a922-9ed9339f87e9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8961da13-a73d-4c99-8a51-7d85160a29c1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ff5442f9-a629-4922-a3d8-38898996a6e2",
    "visible": true
}