{
    "id": "dd392642-ca3d-4f4b-948f-0aa6bbf61e12",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "menuinit",
    "eventList": [
        {
            "id": "202cfced-41f9-4b39-b124-8570028ada62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dd392642-ca3d-4f4b-948f-0aa6bbf61e12"
        },
        {
            "id": "6fa06123-0928-468a-8b95-d1b85b6de04d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "dd392642-ca3d-4f4b-948f-0aa6bbf61e12"
        },
        {
            "id": "0a22c66b-7320-4b1d-a42c-cba8fbe854bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dd392642-ca3d-4f4b-948f-0aa6bbf61e12"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bdb3ccdc-be3f-4f37-a7b7-9295cf13b0ee",
    "visible": true
}