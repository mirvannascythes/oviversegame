if(room = 0){

	part_emitter_region(part_system, part_emitter, x, x+(wH/5)+64, (wW/4)-64,(wW/4)-64, ps_shape_rectangle, ps_distr_linear);
	//part_emitter_region(part_system, part_emitter, 64, 96, 128,180, ps_shape_rectangle, ps_distr_linear);
}else{
	part_emitter_stream(part_system, part_emitter, part_type, 0);
	part_emitter_region(part_system, part_emitter, self.x, self.x+64, self.y,self.y+64, ps_shape_rectangle, ps_distr_linear);
	part_particles_clear(part_system);
}