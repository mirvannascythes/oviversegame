ATEX_init() //This is a requirement of ATEX text, a 3rd party plugin for drawing text (Outline FTW)
//Display
dispCase = 1;
global.res[0] = 1024;
global.res[1] = 768;
wH = view_hport[0]
wW = view_wport[0]

//Particle Effect
	//setup
	part_system=part_system_create();
	part_emitter=part_emitter_create(part_system);
	//properties
	part_type=part_type_create();
	part_type_shape(part_type,pt_shape_explosion);
	//part_type_color1(part_type,c_blue,);
	part_type_sprite(part_type, gooDrop, 1, 1, 0);
	part_type_life(part_type, 160, 180);
	part_type_scale(part_type, 1, 2)
	part_type_gravity(part_type,0.05,270)
	//
	part_emitter_stream(part_system, part_emitter, part_type, -5);
	//
	//Trigger
menu = 0;
global.input = "left";