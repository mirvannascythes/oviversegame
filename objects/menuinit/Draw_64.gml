ATEX_mode = 2;
if(room = 0){
	wX = x
	if(menu = 0){
		draw_sprite_stretched_ext(menuButtonBar,0,wX-64,(wH/5),(wW/3)+64,64,c_white,1);
		draw_set_colour(c_white);
		draw_set_font(Bauhaus93);
		ATEX_draw(wX,(wH/5)+6, "<outline><link src=@newGame?title=\"New Game\"?c=@c_white?cp=@c_lime></outline>",wW/3,fa_left);
		//
		draw_sprite_stretched_ext(menuButtonBar,0,wX-64,(wH/5)*2,(wW/3)+64,64,c_white,1);
		draw_set_colour(c_white);
		draw_set_font(Bauhaus93);
		ATEX_draw(wX,((wH/5)*2)+6, "<outline><link src=@openOptions?title=\"Options\"?c=@c_white?cp=@c_lime></outline>",wW/3,fa_left);
	}
	if(menu = 1){
		draw_sprite_stretched_ext(menuButtonBar,0,wX-64,(wH/5),(wW/3)+64,64,c_white,1);
		draw_set_colour(c_white);
		draw_set_font(Bauhaus93);
		if(global.input ="left"){
			ATEX_draw(wX,(wH/5)+6, "<outline>Input: <link src=@inputSelect?title=\"Left-Keyboard\"?c=@c_white?cp=@c_lime></outline>",wW/3,fa_left);
		}else if(global.input = "mouse"){
			ATEX_draw(wX,(wH/5)+6, "<outline>Input: <link src=@inputSelect?title=\"Mouse\"?c=@c_white?cp=@c_lime></outline>",wW/3,fa_left);
		}
		//
		draw_sprite_stretched_ext(menuButtonBar,0,wX-64,(wH/5)*2,(wW/3)+64,64,c_white,1);
		draw_set_colour(c_white);
		draw_set_font(Bauhaus93);
		//if(global.res[0] = 1024 && global.res[1] = 768){
			ATEX_draw(wX,(wH/5*2)+6, "<outline>Display: <link src=@dispUp?title="+string(global.res[0])+"x"+string(global.res[1])+"?c=@c_white?cp=@c_lime></outline>",wW/3,fa_left);
		//}
		//
		draw_sprite_stretched_ext(menuButtonBar,0,wX-64,(wH/5)*4,(wW/3)+64,64,c_white,1);
		draw_set_colour(c_white);
		draw_set_font(Bauhaus93);
		ATEX_draw(wX,((wH/5)*4)+6, "<outline><link src=@backMenu?title=\"Back\"?c=@c_white?cp=@c_lime></outline>",wW/3,fa_left);
	}
}
ATEX_mode = 0;
