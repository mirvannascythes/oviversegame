{
    "id": "77da8570-0e13-4142-b688-8596ee7866ac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "floorTrap",
    "eventList": [
        {
            "id": "13ecd600-d757-4c8d-be29-c3f55c3f1e6c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "77da8570-0e13-4142-b688-8596ee7866ac"
        },
        {
            "id": "c46a492d-ae00-4e3d-bd73-f8b5f6dc6966",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "77da8570-0e13-4142-b688-8596ee7866ac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "21565d97-9c8e-40be-88e0-f1c833fa393e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "60048f01-6900-4afe-a9a3-933abb75108b",
    "visible": true
}