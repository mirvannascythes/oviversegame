//Spooked State requires a target, almost certainly Valentine.
//For now let's assume it is
//spookObject = playerObject;
speedModifier = 3;
fear += 0.001;
global.victimStressCurrent += 0.00001*fear;
//show_debug_message("Aye!")

if(detectValentine() = 1 && instance_exists(bitchCheeseObject) = 0){
	global.victimStressCurrent += 10;
	//Get the direction from the scary thing
	var itsme = point_direction(playerObject.x,playerObject.y,bitchObject.x,bitchObject.y);
	//Run opposite til you can't see it to a random location
	var runDirX = lengthdir_x(visionDistance*1.5, itsme);
	var runDirY = lengthdir_x(visionDistance*1.5, itsme);
	//Try to find a place nearby as a goal, we attempt 100 times only to avoid a loop
	repeat(100){
		var xT = runDirX + irandom_range(-64, 64);
		var yT = runDirY + irandom_range(-64, 64);
		var bC = instance_create_depth(x+xT,y+yT,-y*2,bitchCheeseObject);
		with(bC){
			if(place_meeting(x,y,bitchNoGoList)){instance_destroy()}
		}
		if(instance_exists(bitchCheeseObject)){break;}
	}
	//If a valid goal is found, now we test if the path is valid, it's set to a differnt path as using a test path overides the old one.
	if(instance_exists(bitchCheeseObject)){
		createPath(bC.x,bC.y); //Create a path to the cheese
	}
	//boredomCooldown = 0;//Reset boredom
}else if(detectValentine() = 0 && instance_exists(bitchCheeseObject) = 0){
	detected = 0;
	currentState = aistates.dState;
	show_debug_message("Whew!")
}