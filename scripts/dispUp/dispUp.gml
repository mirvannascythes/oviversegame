if(dispCase < 14){dispCase += 1;}else{dispCase = 0;}
switch (dispCase){
	//4:3
	case 0: //SVGA
		global.res[0] = 800;
		global.res[1] =	600;
		break;
	case 1: //XGA
		global.res[0] = 1024;
		global.res[1] =	768;
		break;
	//21:9
	case 2:
		global.res[0] = 2560;
		global.res[1] =	1080;
		break;
	case 3:
		global.res[0] = 3440;
		global.res[1] =	1440;
		break;
	//16:9
	case 4: //WXGA
		global.res[0] = 1280;
		global.res[1] =	720;
		break;
	case 5:
		global.res[0] = 1536;
		global.res[1] =	864;
		break;
	case 6: //HD+
		global.res[0] = 1600;
		global.res[1] =	900;
		break;
	case 7: //FHD
		global.res[0] = 1920;
		global.res[1] =	1080;
		break;
	case 8: //QWXGA
		global.res[0] = 2048;
		global.res[1] =	1152;
		break;
	case 9: //QHD
		global.res[0] = 2560;
		global.res[1] =	1440;
		break;
	case 10: //4k QHD
		global.res[0] = 3840;
		global.res[1] =	2160;
		break;
	//16:10
	case 11: //WXGA
		global.res[0] = 1280;
		global.res[1] =	800;
		break;
	case 12: //WXGA+
		global.res[0] = 1440;
		global.res[1] =	900;
		break;
	case 13: //WSXGA+
		global.res[0] = 1680;
		global.res[1] =	1050;
		break;
	case 14: //WUXGA
		global.res[0] = 1920;
		global.res[1] =	1200;
		break;
}


window_set_size(global.res[0],global.res[1]);
view_wport[0] = global.res[0];
view_hport[0] = global.res[1];	
display_set_gui_maximise();
wW = global.res[0]; 
wH = global.res[1];
