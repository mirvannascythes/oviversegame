xScale = approach(xScale,1,0.03);
yScale = approach(yScale,1,0.03);

//animation control    
switch currentState {
    case states.normal:
	if(global.input = "left"){
        if(left && !down && !up || right && !down && !up){
			if(left){
				facing = -1;
			}else if(right){
				facing = 1;
			}
            sprite = goopyWalk;
        }else if(up && !left && !right){
			facing = 1;
			sprite = goopyUp;
		}else if(up && left && !right){
			facing = 1;
			sprite = goopyUL;
		}else if(up && right && !left){
			facing = 1;
			sprite = goopyUR;
		}else if(down && !left && !right){
			facing = 1;
			sprite = goopyDown;
		}else if(down && left && !right){
			facing = 1;
			sprite = goopyBL;
		}else if(down && !left && right){
			facing = 1;
			sprite = goopyBR;
		}else{
            sprite = goopyStand;
        }
	}else if(global.input = "mouse"){
		var pointMouse = round(direction/45);
		if(pointMouse = 0 || pointMouse = 8){
			facing = 1;
			sprite = goopyWalk;
		}else if(pointMouse = 1){
			facing = 1;
			sprite = goopyUR;
		}else if(pointMouse = 1){
			facing = 1;
			sprite = goopyUp;
		}else if(pointMouse = 3){
			facing = 1;
			sprite = goopyUL;
		}else if(pointMouse = 4){
			facing = -1;
			sprite = goopyWalk;
		}else if(pointMouse = 5){
			facing = 1;
			sprite = goopyBL;
		}else if(pointMouse = 6){
			facing = 1;
			sprite = goopyDown;
		}else if(pointMouse = 7){
			facing = 1;
			sprite = goopyBR;
		}
		if(xySpeed = 0){
			sprite = goopyStand;
		}
	}
    break;

    case states.puddle:
        sprite = goopyPuddle;
    break;

}

//reset frame to 0 if sprite changes
if(lastSprite != sprite){
    lastSprite = sprite;
    frame = 0;
}