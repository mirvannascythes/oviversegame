speedModifier = 1; //Move at default speed when doing this
//If there are no goals, and boredom is sufficient, set a random goal to wander too
if(instance_exists(bitchCheeseObject) = 0 && boredomCooldown >= 100){
	
	//Try to find a place nearby as a goal, we attempt 100 times only to avoid a loop
	repeat(100){
		var xT = irandom_range(-64, 64);
		var yT = irandom_range(-64, 64);
		var bC = instance_create_depth(x+xT,y+yT,-y*2,bitchCheeseObject);
		with(bC){
			if(place_meeting(x,y,bitchNoGoList)){instance_destroy()}
		}
		if(instance_exists(bitchCheeseObject)){break;}
	}
	//If a valid goal is found, now we test if the path is valid, it's set to a differnt path as using a test path overides the old one.
	if(instance_exists(bitchCheeseObject)){
		createPath(bC.x,bC.y); //Create a path to the cheese
	}
	boredomCooldown = 0;//Reset boredom
}else if(instance_exists(bitchCheeseObject) = 0){
	boredomCooldown +=1 //Increment the time before deciding to 'wander' again
	xySpeed = 0; //Reset acceleration
}

//Next in our AI state we want ways in which our AI state can be switched/interrupted i.e. fear interupts normal states
if(detectValentine() = 1){
	detected = 1;
	currentState = aistates.spooked;
	show_debug_message("Gasp!")
	eraseNodes();
}