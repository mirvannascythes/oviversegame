//Sets up player controls, these can be rebound here
//left = keyboard_check(vk_left);
//right = keyboard_check(vk_right);
//up = keyboard_check(vk_up);
//down = keyboard_check(vk_down);
//space = keyboard_check(vk_space);

left = keyboard_check(ord("A"));
right = keyboard_check(ord("D"));
up = keyboard_check(ord("W"));
down = keyboard_check(ord("S"));
if(global.input = "left"){
	space = keyboard_check(vk_space);
	global.key1 = keyboard_check(ord("Q"));
	global.key2 = keyboard_check(ord("E"));
	global.key3 = keyboard_check(ord("R"));
	global.keySpace = keyboard_check(vk_space);
}else if(global.input = "mouse"){
	space = mouse_check_button(mb_right);
	global.key1 = keyboard_check(ord("Q"));
	global.key2 = keyboard_check(ord("E"));
	global.key3 = keyboard_check(ord("R"));
	global.keySpace = mouse_check_button(mb_right);	
}

if(left && !up && !down){
	xSpeed = approach(xSpeed,-mSpeed,aSpeed);
	ySpeed = approach(ySpeed,0,dSpeed);
	direction = 180;
}else if(right && !up && !down){
	xSpeed = approach(xSpeed,mSpeed,aSpeed);
	ySpeed = approach(ySpeed,0,dSpeed);
	direction = 0;
}else if(up && !left && !right){
	xSpeed = approach(xSpeed,0,dSpeed);
	ySpeed = approach(ySpeed,-mSpeed,aSpeed);
	direction = 90;
}else if(down && !left && !right){
	xSpeed = approach(xSpeed,0,dSpeed);
	ySpeed = approach(ySpeed,mSpeed,aSpeed);
	direction = 270;
}else if(left && up){
    xSpeed = approach(xSpeed,-mSpeed,aSpeed);
	ySpeed = approach(ySpeed,-mSpeed,aSpeed);
	direction = 135;
}else if(right && up){
    xSpeed = approach(xSpeed,mSpeed,aSpeed);
	ySpeed = approach(ySpeed,-mSpeed,aSpeed);
	direction = 45;
}else if(down && right){
    xSpeed = approach(xSpeed,mSpeed,aSpeed);
	ySpeed = approach(ySpeed,mSpeed,aSpeed);
	direction = 315;
}else if(down && left){
	xSpeed = approach(xSpeed,-mSpeed,aSpeed);
	ySpeed = approach(ySpeed,mSpeed,aSpeed);
	direction = 225;
}else{ 
	xSpeed = approach(xSpeed,0,dSpeed);
	ySpeed = approach(ySpeed,0,dSpeed);
}