if(!collision_line(x+16,y+8,playerObject.x+16,playerObject.y+8,wallParent,1,1) = 1 //Can't see if behind a wall
	&& point_distance(x+16,y+8,playerObject.x+16,playerObject.y+8) <= visionDistance //Serious Myopia
	&& point_direction(x+16,y+8,playerObject.x+16,playerObject.y+8) - direction <= +(visionRadius/2)//Check up/down of direction too look for Valentine
	&& point_direction(x+16,y+8,playerObject.x+16,playerObject.y+8) - direction >= -(visionRadius/2)
	&& global.puddleForm = false
	){
		return 1;	
}else{return 0}